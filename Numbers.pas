unit Numbers;

interface

uses SysUtils, Functions, Functions2;

type

  TFPNumberKind = (fkNormal, fkSubnormal, fkInf, fkNaN, fkZero);

  TFPInputFormat = (fiBin, fiHex, fiDec);
  TFPOutputFormat = (foSingle, foDouble, foCustom);

  TFPErrorCode = (
    fperrOK,
    fperrExpRange,
    fperrMantRange,
    fperrBinDigit,
    fperrBinLength,
    fperrDecFormat,
    fperrUnderflow,
    fperrOverflow,
    fperrHexFormat,
    fperrHexLong
  );

  TFloatingPointNumber = class
    constructor Create(aIFmt: TFPInputFormat; aOFmt: TFPOutputFormat;
      anExp, anMant: Integer; Str: string);
    destructor Free;

  private
    (* Validez del n�mero *)
    FError: TFPErrorCode;

    (* Formato de representaci�n utilizado *)
    OFmt: TFPOutputFormat;
    IFmt: TFPInputFormat;
    nExp, nMant, nFmt: Integer;

    (* Campos del n�mero *)
    FSign, FExp, FMant: string;
    FBias: Integer;
    FKind: TFPNumberKind;

    (* Representaciones *)
    FValue: Double;
    FDecValue, FHexValue: string;

    function GetValidNumber: Boolean;

  public
    property ValidNumber: Boolean read GetValidNumber;
    property Error: TFPErrorCode read FError;
    property Kind: TFPNumberKind read FKind;
    property Format: TFPOutputFormat read OFmt;

    property Sign: string read FSign;
    property Exponent: string read FExp;
    property Mantissa: string read FMant;

    property ExponentBits: Integer read nExp;
    property MantissaBits: Integer read nMant;
    property Bias: Integer read FBias;

    property Value: Double read FValue;
    property DecValue: string read FDecValue;
    property HexValue: string read FHexValue;

  end;

implementation



(***  CLASE FloatingPointNumber  ***)


constructor TFloatingPointNumber.Create(aIFmt: TFPInputFormat; aOFmt: TFPOutputFormat;
  anExp, anMant: Integer; Str: string);
var
  i: Integer;
  cnverr: cnverrType;
  AllZerosExp, AllOnesExp, AllZerosMant: Boolean;
begin
  (* Formato de representaci�n *)
  FError := fperrOK;
  OFmt := aOFmt;
  if OFmt = foSingle then begin
    nExp := 8;
    nMant := 23;
  end else if aOFmt = foDouble then begin
    nExp := 11;
    nMant := 52;
  end else begin
    nExp := anExp;
    nMant := anMant;
    if (nExp < 1) or (nExp > 11) then begin
      FError := fperrExpRange;
      Exit;
    end;
    if (nMant < 1) or (nMant > 52) then begin
      FError := fperrMantRange;
      Exit;
    end;
    if (nExp = 8) and (nMant = 23) then
      OFmt := foSingle;
    if (nExp = 11) and (nMant = 52) then
      OFmt := foDouble;
  end;
  nFmt := 1 + nExp + nMant; // N� total de bits del formato
  FBias := (1 shl (nExp - 1)) - 1;

  (* Extrae las diferentes representaciones *)
  IFmt := aIFmt;
  case IFmt of

    (* Entrada binaria *)
    fiBin: begin
      (* Elimina espacios en blanco *)
      while Pos(' ', Str) > 0 do
        Delete(Str, Pos(' ', Str), 1);

      (* Comprueba que los d�gitos sean '0' y '1' y el formato *)
      for i := 1 to Length(Str) do
        if (Str[i] <> '0') and (Str[i] <> '1') then begin
          FError := fperrBinDigit;
          Exit;
        end;
      if Length(Str) <> nFmt then begin
        FError := fperrBinLength;
        Exit;
      end;

      (* Traduce a todas las representaciones *)
      (* ATENCI�N: NaN de doble precisi�n provoca una excepci�n - BUG*)
      FValue := CustomToDouble(Str, nExp, nMant);
      FSign := Str[1];
      FExp := Copy(Str, 2, nExp);
      FMant := Copy(Str, nExp + 2, nMant);
      FDecValue := FloatToStr(FValue);
      FHexValue := '0x' + BinToHex(Str);
    end;


    fiDec: begin
      (* Formato Double *)
      try
        FValue := StrToFloat(Str);
      except
        FError := fperrDecFormat;
        Exit;
      end;

      (* Convierte a formato personalizado *)
      cnverr := DoubleToCustom(FValue, nExp, nMant, Str);
      if cnverr = cnverrUnderflow then FError := fperrUnderflow
      else if cnverr = cnverrOverflow then FError := fperrOverflow;
      if cnverr <> cnverrOK then Exit;

      (* El formato personalizado provoca una p�rdida de bits; hay que volver
       * a calcular el valor Double y Decimal asociados. *)
      FValue := CustomToDouble(Str, nExp, nMant);
      FDecValue := FloatToStr(FValue);

      (* Resto de formatos *)
      FSign := Str[1];
      FExp := Copy(Str, 2, nExp);
      FMant := Copy(Str, nExp + 2, nMant);
      FHexValue := '0x' + BinToHex(Str);
    end;


    fiHex: begin
      (* Formato binario *)
      try
        Str := HexToBin(Str, nFmt);
      except
        FError := fperrHexFormat;
        Exit;
      end;
      if Length(Str) > nFmt then begin
        FError := fperrHexLong;
        Exit;
      end;

      (* Traduce a todas las representaciones *)
      (* ATENCI�N: NaN de doble precisi�n provoca una excepci�n - BUG*)
      FSign := Str[1];
      FExp := Copy(Str, 2, nExp);
      FMant := Copy(Str, nExp + 2, nMant);
      FValue := CustomToDouble(FSign + FExp + FMant, nExp, nMant);
      FDecValue := FloatToStr(FValue);
      FHexValue := '0x' + BinToHex(Str);
    end;

  end;

  (* Calcula campos necesarios para tipo de n�mero *)
  AllZerosExp := True;
  AllOnesExp := True;
  AllZerosMant := True;
  for i := 1 to nExp do begin
    if FExp[i] = '0' then AllOnesExp := False;
    if FExp[i] = '1' then AllZerosExp := False;
  end;
  for i := 1 to nMant do if FMant[i] = '1' then begin
    AllZerosMant := False;
    break;
  end;

  (* Obtiene tipo de n�mero *)
  if FValue = 0 then FKind := fkZero
  else if AllZerosExp then FKind := fkSubnormal
  else if AllOnesExp and AllZerosMant then FKind := fkInf
  else if AllOnesExp and (not AllZerosMant) then FKind := fkNaN
  else FKind := fkNormal;

  (* Retoca representaci�n decimal si es NaN o Inf *)
  if FKind = fkNaN then FDecValue := 'NaN'
  else if (FKind = fkInf) and (FSign = '0') then FDecValue := 'Inf'
  else if (FKind = fkInf) and (FSign = '1') then FDecValue := '-Inf'
end;

destructor TFloatingPointNumber.Free;
begin
end;


function TFloatingPointNumber.GetValidNumber: Boolean;
begin
  Result := FError = fperrOK;
end;



end.

