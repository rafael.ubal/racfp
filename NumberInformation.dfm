object frmNumberInfo: TfrmNumberInfo
  Left = 308
  Top = 145
  BorderStyle = bsDialog
  Caption = 'Informaci'#243'n del n'#250'mero'
  ClientHeight = 356
  ClientWidth = 503
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object grpResult: TGroupBox
    Left = 10
    Top = 11
    Width = 487
    Height = 102
    Caption = 'General'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    object lblBin: TLabel
      Left = 8
      Top = 16
      Width = 238
      Height = 13
      Caption = 'Representaci'#243'n binaria (signo-exponente-mantisa):'
    end
    object lblHex: TLabel
      Left = 8
      Top = 56
      Width = 140
      Height = 13
      Caption = 'Representaci'#243'n hexadecimal:'
    end
    object lblDec: TLabel
      Left = 168
      Top = 56
      Width = 114
      Height = 13
      Caption = 'Representaci'#243'n decimal'
    end
    object txtSign: TStaticText
      Left = 8
      Top = 32
      Width = 12
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 0
    end
    object txtExp: TStaticText
      Left = 24
      Top = 32
      Width = 83
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 1
    end
    object txtMantissa: TStaticText
      Left = 113
      Top = 32
      Width = 370
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 2
    end
    object txtHex: TStaticText
      Left = 8
      Top = 72
      Width = 137
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 3
    end
    object txtDec: TStaticText
      Left = 168
      Top = 72
      Width = 169
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Courier New'
      Font.Style = []
      ParentColor = False
      ParentFont = False
      TabOrder = 4
    end
  end
  object cbtClose: TBitBtn
    Left = 184
    Top = 312
    Width = 113
    Height = 33
    Caption = '&Cerrar'
    TabOrder = 2
    Kind = bkCancel
  end
  object grpDetails: TGroupBox
    Left = 8
    Top = 120
    Width = 489
    Height = 185
    Caption = 'Detalles'
    TabOrder = 3
    object lblPrecision: TLabel
      Left = 16
      Top = 40
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Precisi'#243'n:'
    end
    object lblKind: TLabel
      Left = 16
      Top = 64
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Tipo:'
    end
    object lblDescription: TLabel
      Left = 16
      Top = 88
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Descripci'#243'n:'
    end
    object lblStandard: TLabel
      Left = 16
      Top = 16
      Width = 65
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Est'#225'ndar:'
    end
    object txtPrecision: TStaticText
      Left = 88
      Top = 40
      Width = 385
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      ParentColor = False
      TabOrder = 0
    end
    object txtKind: TStaticText
      Left = 88
      Top = 64
      Width = 385
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      ParentColor = False
      TabOrder = 1
    end
    object txtDesc: TStaticText
      Left = 88
      Top = 88
      Width = 385
      Height = 81
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      ParentColor = False
      TabOrder = 2
    end
    object txtStandard: TStaticText
      Left = 88
      Top = 16
      Width = 385
      Height = 17
      AutoSize = False
      BevelKind = bkSoft
      Color = clMoneyGreen
      ParentColor = False
      TabOrder = 3
    end
  end
  object cbtNew: TBitBtn
    Left = 365
    Top = 77
    Width = 121
    Height = 25
    Caption = '&Nuevo n'#250'mero'
    Default = True
    TabOrder = 0
    OnClick = cbtNewClick
    Glyph.Data = {
      9A020000424D9A0200000000000036000000280000000C000000110000000100
      1800000000006402000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFB4FF6880FF00F7FFF0B4FF6880FF00F7FFF0FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFD3FFA780FF00E7FFD0D3FFA780FF00E7FFD0FFFFFFFFFFFFFF
      FFFFFFFFFFFF6868FF0000FFF0F0FF6868FF0000FFF0F080FF00D3FFA7FFFFFF
      FFFFFFFFFFFFFFFFFFFFA7A7FF0000FFD0D0FFA7A7FF0000FFD0D080FF0080FF
      0080FF0080FF00FFFFFFFFFFFFFFD0D0FF0000FFA7A7FFD0D0FF0000FFA7A780
      FF0080FF0080FF0080FF00FFFFFFFF0000FF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000F0FFE1FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF00
      00FF0000FF0000FF0000FF0000ECFFD9FFFFFFFFFFFFFFFFFFFFFFFFFF8C8CFF
      0000FFE1E1FF8C8CFF0000FFE1E180FF00DEFFBDFFFFFFFFFFFFFFFFFFFFFFFF
      FF9A9AFF0000FFD9D9FF9A9AFF0000FFD9D980FF00D3FFA7FFFFFFFFFFFFFFFF
      FFFFFFFFFFBDBDFF0000FFBDBDFFBDBDFF0000FFBDBD80FF00C6FF8CFFFFFFFF
      FFFFFFFFFFFFFFFFFFD9D9FF0000FF9A9AFFD9D9FF0000FFA7A780FF0080FF00
      80FF0080FF00FFFFFFFFFFFFFFE1E1FF0000FF8C8CFFE1E1FF0000FF8C8C80FF
      0080FF0080FF0080FF00FFFFFFFF0000FF0000FF0000FF0000FF0000FF0000FF
      0000FF0000FF0000E7FFD0FFFFFFFFFFFFFF0000FF0000FF0000FF0000FF0000
      FF0000FF0000FF0000FF0000D3FFA7FFFFFFFFFFFFFFFFFFFFFFFFFFA7A7FF00
      00FFD0D0FFA7A7FF0000FFD0D080FF00B4FF68FFFFFFFFFFFFFFFFFFFFFFFFFF
      D0D0FF0000FFA7A7FFD0D0FF0000FFA7A7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFF0F0FF0000FF6868FFF0F0FF0000FF6868FFFFFFFFFFFFFFFFFF}
  end
end
