(* Funciones que incorporan la funcionalidad necesaria para operar con
 * tama�os de exponente y mantisa variable, diferente o igual al del est�ndar
 * IEEE utilizado en la implementaci�n de los circuitos. *)
unit Functions2;

interface

uses SysUtils, Functions;

(* Extrae y pone bits de un array de bytes *)
function GetBits(src: PByteArray; lo, hi: Integer): string;
procedure SetBits(dst: PByteArray; lo: Integer; Bits: string);


(* Convierte un n�mero real a un binario con el n�mero de bits especificado
 * en mantisa y exponente, con el m�ximo limitado al formato IEEE de doble
 * precisi�n (11 bits exponente y 52 mantisa).
 * La funci�n devuelve un c�digo de error de tipo cnverrType. *)
type cnverrType = (
  cnverrOK,
  cnverrFmt,        // n� incorrecto de bits para el formato
  cnverrUnderflow,  // el n� es tan peque�o, que ser�a 0 en ese formato
  cnverrOverflow    // el n� es demasiado grande para el formato
);
function DoubleToCustom(aDouble: Double; nExp, nMantissa: Integer;
  var Res: string): cnverrType;


(* Conversi�n de un n�mero binario con formato personalizado a un real de doble
 * precisi�n. *)
function CustomToDouble(Num: string; nExp, nMant: Integer): Double;



implementation

function GetBits(src: PByteArray; lo, hi: Integer): string;
var
  i, nByte, nBit: Integer;
begin
  Result := '';
  for i := lo to hi do begin
    nByte := i div 8;
    nBit := i mod 8;
    if (src[nByte] and (1 shl nBit)) > 0 then Result := '1' + Result
    else Result := '0' + Result;
  end;
end;


procedure SetBits(dst: PByteArray; lo: Integer; Bits: string);
var
  i, Idx, nByte, nBit: Integer;
  Mask: Byte;
begin
  for i := lo to lo + Length(Bits) - 1 do begin
    Idx := Length(Bits) - i + lo;
    nByte := i div 8;
    nBit := i mod 8;
    if (Bits[Idx] = '0') then begin
      Mask := not (1 shl nBit);
      dst[nByte] := dst[nByte] and Mask;
    end else begin
      Mask := 1 shl nBit;
      dst[nByte] := dst[nByte] or Mask;
    end;
  end;
end;


function DoubleToCustom(aDouble: Double; nExp, nMantissa: Integer;
  var Res: string): cnverrType;
var
  sSign, sExp, sMantissa: string;
  iExp, Bias, i: Integer;
  buf: PByteArray;
  Denorm, MantZeros: Boolean;
begin
  Result := cnverrOK;

  (* Comprueba los rangos de bits en exponente y mantisa *)
  if (nExp < 1) or (nExp > 11) or (nMantissa < 1) or (nMantissa > 52) then begin
    Result := cnverrFmt;
    Exit;
  end;

  (* Si se trata del '0', se devuelve directamente el resultado *)
  if aDouble = 0 then begin
    res := '';
    for i := 0 to nMantissa + nExp do
      res := '0' + res;
    Exit;
  end;

  (* Calcula par�metros *)
  buf := PByteArray(@aDouble);
  sSign := GetBits(buf, 63, 63);
  sExp := GetBits(buf, 52, 62);
  sMantissa := GetBits(buf, 0, 51);
  iExp := BinToInt(sExp) - 1023;
  Bias := (1 shl (nExp - 1)) - 1;

  (* Mantisa con el bit impl�cito *)
  if iExp = -1023 then sMantissa := '0' + sMantissa
  else sMantissa := '1' + sMantissa;

  (* Normaliza la mantisa (evita posible formato denormalizado) *)
  while (sMantissa[1] = '0') do begin
    Delete(sMantissa, 1, 1);
    sMantissa := sMantissa + '0';
    Dec(iExp);
  end;

  (* Si el nuevo exponente es menor que -(Bias - 1), hay que denormalizar la
   * mantisa para adaptarla al nuevo formato. *)
  Denorm := False;
  while iExp <= -Bias do begin
    Denorm := True;
    sMantissa := '0' + sMantissa;
    inc(iExp);
  end;

  (* Comprueba desbordamiento superior *)
  if iExp > Bias then begin
    Result := cnverrOverflow;
    Exit;
  end;

  (* Crea mantisa y exponente *)
  if Denorm then begin
    sExp := '';
    for i := 1 to nExp do sExp := sExp + '0';
  end else
    sExp := IntToBin(iExp + Bias, nExp);
  sMantissa := Copy(sMantissa, 2, nMantissa);

  (* Comprueba desbordamiento inferior *)
  MantZeros := True;
  for i := 1 to nMantissa do
    if sMantissa[i] <> '0' then begin
      MantZeros := False;
      Break;
    end;
  if MantZeros and Denorm then begin
    Result := cnverrUnderflow;
    Exit;
  end;

  (* Recompone resultado *)
  res := sSign + sExp + sMantissa;
end;


function CustomToDouble(Num: string; nExp, nMant: Integer): Double;
var
  sSign, sExp, sMant: string;
  i, Bias, iExp: Integer;
  Zero, Denorm: Boolean;
begin
  (* Comprueba si es cero *)
  Zero := True;
  for i := 1 to Length(Num) do
    if Num[i] <> '0' then begin
      Zero := False;
      Break;
    end;
  if Zero then begin
    Result := 0;
    Exit;
  end;

  (* Extrae campos del n�mero *)
  sSign := Num[1];
  sExp := Copy(Num, 2, nExp);
  sMant := Copy(Num, nExp + 2, nMant);
  Bias := (1 shl (nExp - 1)) - 1;
  iExp := BinToInt(sExp) - Bias;

  (* A�ade bit impl�cito a la mantisa *)
  if iExp = -Bias then begin
    inc(iExp);
    sMant := '0' + sMant;
  end else
    sMant := '1' + sMant;

  (* Normaliza mantisa si estaba denormalizada *)
  while sMant[1] = '0' do begin
    Delete(sMant, 1, 1);
    dec(iExp);
  end;

  (* Si exponente es < -1022, hay que denormalizar para adaptar a Double *)
  Denorm := False;
  while (iExp < -1022) do begin
    Denorm := True;
    sMant := '0' + sMant;
    Inc(iExp);
  end;

  (* Crea campos del nuevo n�mero *)
  if Denorm then sExp := IntToBin(0, 11)
  else sExp := IntToBin(iExp + 1023, 11);
  while Length(sMant) < 53 do
    sMant := sMant + '0';
  Delete(sMant, 1, 1);

  (* Recompone resultado *)
  Num := sSign + sExp + sMant;
  SetBits(PByteArray(@Result), 0, Num);
end;


end.
