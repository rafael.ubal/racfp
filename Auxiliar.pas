unit Auxiliar;

interface

uses Numbers, Functions, RafMsg, SysUtils, StdCtrls, Graphics, Forms,
  ExtCtrls;

type
  TDescriptionWindow = class
    constructor Create(Title: string);
    destructor Free;

    procedure SetDescription(s: string);
    procedure SetPosition(TopPosition: Boolean);
    procedure ToggleShowing;

    procedure HideAndSave;
    procedure Restore;
  private
    Form: TForm;
    Memo: TMemo;
    SavedVisible: Boolean;
  end;

procedure ExpandOperandsInfo(var Num: array of TFloatingPointNumber;
  var S, E, M: array of string; var SInt: array of Integer);
procedure SetLabelState(var lbl: TStaticText; State: Boolean);
procedure SetStage(Form: TForm; Stage: Integer; State: Boolean;
  Heights: array of Integer);

implementation

(*** TDescriptionWindow (ventana emergente que muestra informaci�n) ***)

constructor TDescriptionWindow.Create;
const
  DESC_WND_WIDTH      = 650;
  DESC_WND_HEIGHT     = 150;
begin
  SavedVisible := True;
  Form := TForm.Create(Screen.ActiveForm);
  with Form do begin
    BorderStyle := bsToolWindow;
    FormStyle := fsStayOnTop;
    BorderIcons := [];
    Caption := Title;
    Width := DESC_WND_WIDTH;
    Height := DESC_WND_HEIGHT;
    Left := Screen.Width div 2 - Form.Width div 2 - 70;
    SetPosition(True);

    //Memo
    Memo := TMemo.Create(Form);
    with Memo do begin
      ReadOnly := True;
      Width := DESC_WND_WIDTH - 6;
      Height := DESC_WND_HEIGHT - 19;
      ScrollBars := ssVertical;
      Color := clSilver;
      Font.Name := 'Courier New';
    end;
    InsertControl(Memo);

    //La muestra
    Show;
  end;
end;

destructor TDescriptionWindow.Free;
begin
  Form.Close;
  Form.Free;
end;

procedure TDescriptionWindow.SetDescription;
begin
  if Memo.Lines.Text <> s then
    Memo.Lines.Text := s;
end;

procedure TDescriptionWindow.SetPosition;
begin
  if TopPosition then
    Form.Top := Screen.Height div 4 - Form.Height div 2 - 30
  else
    Form.Top := Screen.Height div 4 * 3 - Form.Height div 2 + 30;
end;

procedure TDescriptionWindow.ToggleShowing;
begin
  if Form.Showing then Form.Close
  else Form.Show;
end;

procedure TDescriptionWindow.HideAndSave;
begin
  SavedVisible := Form.Showing;
  if SavedVisible then
    Form.Close;
end;

procedure TDescriptionWindow.Restore;
begin
  if SavedVisible = Form.Showing then
    Exit;
  if SavedVisible then Form.Show
  else Form.Close;
end;




(*** Otras funciones ***)

(* Expande la informaci�n de un n�mero en diferentes campos para que
 * pueda ser m�s manejable *)
procedure ExpandOperandsInfo;
var
  i: Integer;
begin
  for i := 0 to 1 do begin
    M[i] := Num[i].Mantissa;
    E[i] := Num[i].Exponent;
    S[i] := Num[i].Sign;
    SInt[i] := BinToInt(S[i]);
  end;
end;

(* Resalta o devuelve el estado normal a un objeto StaticText *)
procedure SetLabelState;
begin
  if State then begin
    lbl.Color := clBlue;
    lbl.Font.Color := clWhite;
  end else begin
    lbl.Color := clBtnFace;
    lbl.Font.Color := clBlack;
  end;
end;

(* Resalta los objetos de una ventana que tengan el nombre 'lbl_X_*', donde
 * X es Stage y * es cualquier cosa. Adem�s busca un objeto con nombre
 * 'pnlMain' y le asigna su altura correspondiente. *)
procedure SetStage;
var
  i, a: Integer;
  lbl: TStaticText;
  CName: string;
  pnl: TPanel;
begin
  with Form do begin
    for i := 0 to ControlCount - 1 do if Controls[i].Name = 'pnlMain' then begin
      //Pone altura al panel
      pnl := TPanel(Controls[i]);
      if State then begin
        if Stage < 0 then
          pnl.Height := 0
        else
          pnl.Height := Heights[Stage];
      end;

      //Busca labels en el panel
      for a := 0 to pnl.ControlCount - 1 do begin
        CName := pnl.Controls[a].Name;
        if (Copy(CName, 1, 4) = 'lbl_') and (CName[5] = IntToStr(Stage)) then begin
          lbl := TStaticText(pnl.Controls[a]);
          SetLabelState(lbl, State);
        end;
      end;

    end;
  end;
end;


end.
