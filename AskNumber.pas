unit AskNumber;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, Buttons, Numbers, RafMsg, Text;

type
  TfrmAskNumber = class(TForm)
    grpOutputFmt: TGroupBox;
    rdSingle: TRadioButton;
    rdDouble: TRadioButton;
    rdCustom: TRadioButton;
    fldExp: TEdit;
    udExp: TUpDown;
    fldMantissa: TEdit;
    udMantissa: TUpDown;
    lblMantissa: TStaticText;
    lblExp: TStaticText;
    lblBits1: TStaticText;
    lblBits2: TStaticText;
    grpInputFmt: TGroupBox;
    rdBin: TRadioButton;
    rdDec: TRadioButton;
    rdHex: TRadioButton;
    fldInput: TEdit;
    lblInput: TLabel;
    cbtOK: TBitBtn;
    cbtClose: TBitBtn;
    procedure rdGroupClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAskNumber: TfrmAskNumber;

(* Activa la ventana de introducci�n de n�meros. El par�metro ActiveCustom
 * indica si se permite la opci�n de introducir n�meros con formato
 * personalizado (TRUE) o s�lo n�meros seg�n el est�ndar (FALSE). *)
function AskFPNumber(AllowCustom: Boolean): TFloatingPointNumber;

implementation

{$R *.dfm}

procedure TfrmAskNumber.rdGroupClick(Sender: TObject);
begin
  fldExp.Enabled := rdCustom.Checked;
  fldMantissa.Enabled := rdCustom.Checked;
  udExp.Enabled := rdCustom.Checked;
  udMantissa.Enabled := rdCustom.Checked;
  lblExp.Enabled := rdCustom.Checked;
  lblMantissa.Enabled := rdCustom.Checked;
  lblBits1.Enabled := rdCustom.Checked;
  lblBits2.Enabled := rdCustom.Checked;
end;


function AskFPNumber(AllowCustom: Boolean): TFloatingPointNumber;
var
  FPN: TFloatingPointNumber;
  FPIFmt: TFPInputFormat;
  FPOFmt: TFPOutputFormat;
  nExp, nMant: Integer;
label
  TryAgain;
begin

TryAgain:

  (* Muestra cuadro de di�logo *)
  Result := nil;
  frmAskNumber.rdCustom.Enabled := AllowCustom;
  if (not AllowCustom) and frmAskNumber.rdCustom.Checked then
    frmAskNumber.rdSingle.Checked := True;
  if frmAskNumber.ShowModal = mrCancel then
    Exit;

  (* Crea el n�mero a partir de datos del usuario *)
  with frmAskNumber do begin
    if rdSingle.Checked then FPOFmt := foSingle
    else if rdDouble.Checked then FPOFmt := foDouble
    else FPOFmt := foCustom;
    if rdBin.Checked then FPIFmt := fiBin
    else if rdHex.Checked then FPIFmt := fiHex
    else FPIFmt := fiDec;
    try
      nExp := StrToInt(fldExp.Text);
      nMant := StrToInt(fldMantissa.Text);
    except
      ShowError(TXT_ASKNUM_ERR_INTTOSTR[Lng]);
      Exit;
    end;
    FPN := TFloatingPointNumber.Create(FPIFmt, FPOFmt, nExp, nMant,
      fldInput.Text);
  end;

  (* Muestra mensajes de error *)
  case FPN.Error of
    fperrExpRange:  ShowError(TXT_ASKNUM_ERR_EXPRANGE[Lng]);
    fperrMantRange: ShowError(TXT_ASKNUM_ERR_MANTRANGE[Lng]);
    fperrBinDigit:  ShowError(TXT_ASKNUM_ERR_BINDIGIT[Lng]);
    fperrBinLength: ShowError(TXT_ASKNUM_ERR_BINLENGTH[Lng]);
    fperrDecFormat: ShowError(TXT_ASKNUM_ERR_DECFORMAT[Lng]);
    fperrUnderflow: ShowError(TXT_ASKNUM_ERR_UNDERFLOW[Lng]);
    fperrOverflow:  ShowError(TXT_ASKNUM_ERR_OVERFLOW[Lng]);
    fperrHexFormat: ShowError(TXT_ASKNUM_ERR_HEXFORMAT[Lng]);
    fperrHexLong:   ShowError(TXT_ASKNUM_ERR_HEXLONG[Lng]);
    else Result := FPN;
  end;

  (* Vuelve a intentarlo *)
  if Result = nil then
    goto TryAgain;
end;

procedure TfrmAskNumber.FormShow(Sender: TObject);
begin
  Caption := TXT_ASKNUM_CAPTION[Lng];
  grpOutputFmt.Caption := TXT_ASKNUM_OUTPUTFMT[Lng];
  grpInputFmt.Caption := TXT_ASKNUM_INPUTFMT[Lng];
  rdBin.Caption := TXT_ASKNUM_BIN[Lng];
  rdHex.Caption := TXT_ASKNUM_HEX[Lng];
  rdDec.Caption := TXT_ASKNUM_DEC[Lng];
  lblExp.Caption := TXT_ASKNUM_EXP[Lng];
  lblMantissa.Caption := TXT_ASKNUM_MANT[Lng];
  rdSingle.Caption := TXT_ASKNUM_SINGLE[Lng];
  rdDouble.Caption := TXT_ASKNUM_DOUBLE[Lng];
  rdCustom.Caption := TXT_ASKNUM_CUSTOM[Lng];
  lblInput.Caption := TXT_INSERT_NUMBER[Lng];
  cbtClose.Caption := TXT_BUT_CLOSE[Lng];
end;

end.

