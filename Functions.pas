unit Functions;

interface

uses SysUtils;

const
  NUM_SINGLE_EXP_LENGTH         = 8;
  NUM_SINGLE_EXP_EXCESS         = 127;
  NUM_SINGLE_EXP_RANGE_MIN      = -127;
  NUM_SINGLE_EXP_RANGE_MAX      = 127;

  NUM_DOUBLE_EXP_LENGTH         = 11;
  NUM_DOUBLE_EXP_EXCESS         = 1023;
  NUM_DOUBLE_EXP_RANGE_MIN      = -1023;
  NUM_DOUBLE_EXP_RANGE_MAX      = 1023;

type
  TByteArray = array[0..$ffff] of byte;
  PByteArray = ^TByteArray;

  PSingle = ^Single;
  PDouble = ^Double;

  (* Enumeraci�n de redondeos *)
  TRoundMethod = (rmPlusInfinite,
                  rmMinusInfinite,
                  rmZero,
                  rmNearestOdd);

(* Conversi�n entre formatos *)
function ByteToBin(aByte: Byte): string;
function ByteToHex(aByte: Byte): string;
function BinToByte(aBin: string): Byte;
function BinToInt(aBin: string): Int64;
function BinToHex(aBin: string): string;
function HexToByte(aHex: string): Byte;
function HexToBin(aHex: string; Digits: Integer): string;
function IntToBin(aInt: Int64; Digits: Integer): string;
function SingleToBin(aSingle: Single): string;
function SingleToHex(aSingle: Single): string;
function DoubleToBin(aDouble: Double): string;
function DoubleToHex(aDouble: Double): string;



(* Operaciones con n�meros binarios *)

(* Operaciones de suma y resta devuelven True si hay acarreo final. El
 * resultado tendr� los mismos bits que el operando de m�s bits. LA
 * operaci�n de comparaci�n devuelve -1, 0 � 1. *)
function AddPositive(a, b: string; var Res: string): Boolean;
function SubPositive(a, b: string; var Res: string): Boolean;
function BinCompare(a, b: string): Integer;

(* Operaci�n de desplazamiento: dcha=positivo, izq=negativo *)
function BinShift(aBin: string; NumShift: Integer; InsChar: Char): string;
function BinInv(aBin: string): string;
function BinCa2(aBin: string): string;
procedure BinZeros(var aBin: string);
procedure BinOnes(var aBin: string);


(* Operaciones con n�meros en coma flotante representados en
 * formato binario (string) *)


(* Compara los exponentes y desplaza la mantisa correspondiente al exponente
 * menor a la dcha hasta que coincidan ambos exponentes. *)
procedure CompareAndSetExponents(var Exp1, Signif1, Exp2, Signif2: string);

(* Desnormaliza un n�mero a�adi�ndole el bit impl�cito a la mantisa (�sta
 * resultar� con un bit m�s despu�s de la llamada a la funci�n. El bit a�adido
 * ser� 0 si el exponente era 0. Si no, el bit a�adido ser� 1. *)
procedure AddImplicitBit(Exp: string; var Signif: string);

(* Suma dos mantisas. Para ello les a�adir� 2 bits. El segundo para el posible
 * desbordamiento del resultado y el primero para expresar el signo. Las
 * mantisas se representar�n en Ca2 para poder operar con ellas f�cilmente.
 * Se devolver� por una parte el signo del resultado y por otra parte su
 * magnitud (�sta tendr� 1 bit m�s que las mantisas originales).
 * AddOperation es True si se pide una suma. False si es una resta. *)
procedure AddSignificands(var SignRes, SignifRes: string;
  Sign1, Signif1, Sign2, Signif2: string; IsAddOperation: Boolean);

(* Normalizaci�n. La mantisa del resultado tendr� el mismo n�mero de bits que
 * la de entrada. Se normalizar� de forma que el primer '1' quede en la posici�n
 * MSBPos de la mantisa (MSBPos = 1: primer car�cter del string).
 * La funci�n devuelve True si hay desbordamiento al normalizar. *)
function Normalize(var Exp, Signif: string; MSBPos: Integer): Boolean;

(* Redondea una mantisa seg�n el m�todo de redondeo especificado.
 * En el resultado habr�n desaparecido los 2 bits de menor peso. *)
procedure RoundSignificand(var Sign, Signif: string; RoundMethod: TRoundMethod);

(* Obtiene la operaci�n que realizar� un sumador/restador a partir de SignoA,
 * SignoB y OpUser (operaci�n pedida por el usuario 0=suma, 1=resta) *)
function GetAdderOperation(Sa, Sb, OpUser: Integer): Integer;

(* Obtiene el signo del resultado de una suma/resta a partir de SignoA, SignoB,
 * SignoRes (resultado de suma/resta de mantisas) y OpUser *)
function GetAdderResultSign(Sa, Sb, SRes, OpUser: Integer): Integer;

(* Calcula suma/resta de 2 exponentes. Si uno de �stos es 0, se convertir� a
 * 1 antes de sumar, ya que el 0 se usa para n�mero no normalizados, pero tiene
 * el efecto de 1. Overflow y Underflow devolver�n los desbordamientos *)
function AddExponents(Exp1, Exp2: string; Op: Integer; var Overflow,
  Underflow: Boolean): string;

(* Arregla un desbordamiento inferior. Esta funci�n se debe llamar en el caso
 * en que el resultado de la resta de exponentes sea <= 0. La funci�n
 * incrementar� E y desplazar� M a la derecha hasta que E valga 1. Una vez
 * hecho esto, pondr� E a 0. S�lo realizar� la acci�n si Overflow es True *)
procedure SolveMultUnderflow(Underflow: Boolean; var E, M: string);

(* Divide o multiplica mantisas. En ambos casos, el resultado tendr� el
 * doble de d�gitos que las mantisas de entrada. *)
function MultiplierSignifOp(Signif1, Signif2: string; Op: Integer): string;

(* Trunca la mantisa despu�s de la normalizaci�n. Ya sobran todos los bits
 * de menor peso guardados, excepto los 2 �ltimos para redondear despu�s.
 * La longitud quedar� con 2 + 23 + 2 bits o bien 2 + 52 + 2 bits *)
procedure TruncSignifAfterNormalize(var Signif: string);

(* Signo del resultado *)
function GetMultResultSign(Sign1, Sign2: string): string;


implementation


(***  MANEJO DE BYTES  ***)

function ByteToBin;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 7 do
    if aByte and (1 shl i) > 0 then Result := '1' + Result
    else Result := '0' + Result;
end;

function ByteToHex;
var
  i, hexDigit: Integer;
begin
  Result := '';
  for i := 0 to 1 do begin
    hexDigit := aByte and $f;
    case hexDigit of
      0..9:   Result := Chr(hexDigit + 48) + Result;
      $a..$f: Result := Chr(hexDigit + 87) + Result;
    end;
    aByte := aByte shr 4;
  end;
end;

function BinToByte;
var
  i, Index: Integer;
begin
  Result := 0;
  for i := 0 to 7 do begin
    Index := Length(aBin) - i;
    if Index < 1 then Break;

    case aBin[Index] of
      '0': begin end;
      '1': Result := Result or (1 shl i);
      else raise SysUtils.EConvertError.Create(
        'Invalid binary number: ' + aBin);
    end;
  end;
end;

function HexToByte;
var
  i, Index: Integer;
  hexDigit: Char;
begin
  Result := 0;
  for i := 0 to 1 do begin
    Index := Length(aHex) - i;
    if Index < 1 then Break;
    hexDigit := UpCase(aHex[Index]);

    case hexDigit of
      'A'..'F': Result := Result or ((Byte(hexDigit) - 55) shl (i * 4));
      '0'..'9': Result := Result or ((Byte(hexDigit) - 48) shl (i * 4));
      else raise SysUtils.EConvertError.Create(
        'Invalid hexadecimal number: ' + aHex);
    end;
  end;
end;

function BinToInt;
var
  i, Index: Integer;
  Mask: Int64;
begin
  Result := 0;
  Mask := 1;
  for i := 0 to 63 do begin
    Index := Length(aBin) - i;
    if Index < 1 then Break;

    case aBin[Index] of
      '0': begin end;
      '1': Result := Result or Mask;
      else raise SysUtils.EConvertError.Create(
        'Invalid binary number: ' + aBin);
    end;
    Mask := Mask shl 1;
  end;
end;

function IntToBin;
var
  i: Integer;
  mask: Int64;
begin
  Result := '';
  for i := 0 to Digits - 1 do begin
    mask := 1 shl i;
    if aInt and mask > 0 then Result := '1' + Result
    else Result := '0' + Result;
  end;
end;


function BinToHex(aBin: string): string;
var
  Take: Integer;
  HexChar: Byte;
begin
  Result := '';
  Take := Length(aBin) mod 4;
  if (Take = 0) and (Length(aBin) > 0) then Take := 4;
  while Length(aBin) > 0 do begin
    HexChar := BinToInt(Copy(aBin, 1, Take));
    if HexChar <= 9 then Result := Result + Chr(Ord('0') + HexChar)
    else Result := Result + Chr(Ord('a') + HexChar - 10);
    Delete(aBin, 1, Take);
    Take := 4;
  end;
end;


function HexToBin(aHex: string; Digits: Integer): string;
var
  i: Integer;
  c: Char;
begin
  Result := '';
  aHex := LowerCase(aHex);

  (* Calcula bits *)
  for i := 1 to Length(aHex) do begin
    c := aHex[i];
    if (c >= 'a') and (c <= 'f') then
      Result := Result + IntToBin(Ord(c) - Ord('a') + 10, 4)
    else if (c >= '0') and (c <= '9') then
      Result := Result + IntToBin(Ord(c) - Ord('0'), 4)
    else raise SysUtils.EConvertError.Create(
        'Invalid hexadecimal number: ' + aHex);
  end;

  (* Elimina bits a 0 delanteros y luego a�ade tantos como hagan falta *)
  while (Length(Result) > 0) and (Result[1] = '0') do
    Delete(Result, 1, 1);
  while (Length(Result) < Digits) do
    Result := '0' + Result;
end;




(***  MANEJO DE N�MEROS EN COMA FLOTANTE  ***)

function SingleToBin;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 3 do
    Result := ByteToBin(PByteArray(@aSingle)^[i]) + Result;
end;

function SingleToHex;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 3 do
    Result := ByteToHex(PByteArray(@aSingle)^[i]) + Result;
end;

function DoubleToBin;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 7 do
    Result := ByteToBin(PByteArray(@aDouble)^[i]) + Result;
end;

function DoubleToHex;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to 7 do
    Result := ByteToHex(PByteArray(@aDouble)^[i]) + Result;
end;



(***  OPERACIONES CON N�MEROS BINARIOS  ***)


(* Pone los dos n�mero con los mismos d�gitos *)
procedure SetSameLength(var a, b: string);
begin
  while Length(a) < Length(b) do
    a := '0' + a;
  while Length(b) < Length(a) do
    b := '0' + b;
end;

function AddPositive;
var
  Carry, i, si: Integer;
begin
  SetSameLength(a, b);

  (* Suma. Result contendr� el acarreo *)
  Carry := 0;
  Res := '';
  si := 0;
  Result := False;
  for i := Length(a) downto 1 do begin
    (* Suma *)
    if (a[i] = '0') and (b[i] = '0') then si := Carry
    else if (a[i] = '0') and (b[i] = '1') then si := 1 + Carry
    else if (a[i] = '1') and (b[i] = '0') then si := 1 + Carry
    else if (a[i] = '1') and (b[i] = '1') then si := 2 + Carry
    else begin  //Formato de entrada incorrecto
      Res := '';
      Exit;
    end;

    (* Calcula resultado y acarreo *)
    if si mod 2 = 0 then Res := '0' + Res
    else Res := '1' + Res;
    Carry := si div 2;
  end;

  (* Acarreo final *)
  Result := Carry > 0;
end;

function SubPositive;
var
  i: Integer;
begin
  SetSameLength(a, b);

  (* Resta. Result contendr� el pr�stamo final *)
  Result := False;
  Res := '';
  for i := Length(a) downto 1 do begin
    if (a[i] = '0') and (b[i] = '0') then begin
      if Result then Res := '1' + Res else Res := '0' + Res;
    end else if (a[i] = '1') and (b[i] = '0') then begin
      if Result then Res := '0' + Res else Res := '1' + Res;
      Result := False;
    end else if (a[i] = '0') and (b[i] = '1') then begin
      if Result then Res := '0' + Res else Res := '1' + Res;
      Result := True;
    end else if (a[i] = '1') and (b[i] = '1') then begin
      if Result then Res := '1' + Res else Res := '0' + Res;
    end else begin
      //Formato incorrecto
      Res := '';
      Exit;
    end;
  end;
end;

function BinCompare;
var
  i: Integer;
begin
  SetSameLength(a, b);

  if a = b then begin
    Result := 0;
    Exit;
  end;

  for i := 1 to Length(a) do
    if (a[i] = '0') and (b[i] = '1') then begin
      Result := 1;
      Exit;
    end else if (a[i] = '1') and (b[i] = '0') then begin
      Result := -1;
      Exit;
    end;

  //Si el formato es correcto, no se llegar� aqu�
  Result := 0;
end;

function BinShift;
var
  AbsShift, i: Integer;
begin
  Result := '';
  AbsShift := Abs(NumShift);

  (* Demasiados/demasiado pocos desplazamientos *)
  if AbsShift = 0 then begin
    Result := aBin;
    Exit;
  end else if AbsShift >= Length(aBin) then begin
    for i := 1 to Length(aBin) do
      Result := InsChar + Result;
    Exit;
  end;

  (* Desplaza *)
  for i := 1 to AbsShift do
    Result := InsChar + Result;
  if NumShift > 0 then
    Result := Result + Copy(aBin, 1, Length(aBin) - AbsShift)
  else Result := Copy(aBin, AbsShift + 1, Length(aBin) - AbsShift) + Result;
end;

function BinInv;
var
  i: Integer;
begin
  for i := 1 to Length(aBin) do
    if aBin[i] = '0' then aBin[i] := '1'
    else aBin[i] := '0';
  Result := aBin;
end;

function BinCa2;
begin
  Result := BinInv(aBin);
  if aBin = '' then Exit;
  AddPositive(Result, '1', Result);
end;

procedure BinZeros;
var
  i: Integer;
begin
  for i := 1 to Length(aBin) do
    aBin[i] := '0';
end;

procedure BinOnes;
var
  i: Integer;
begin
  for i := 1 to Length(aBin) do
    aBin[i] := '1';
end;



(***  OPERACIONES DE N�MEROS EN COMA FLOTANTE  ***)

procedure CompareAndSetExponents;
var
  Exp1Int, Exp2Int, ExpDif: Integer;
begin
  Exp1Int := BinToInt(Exp1);
  Exp2Int := BinToInt(Exp2);
  ExpDif := Abs(Exp1Int - Exp2Int);

  (* Mismo exponente, nada que hacer *)
  if Exp1Int = Exp2Int then
    Exit

  (* Exponente 1 menor *)
  else if Exp1Int < Exp2Int then begin
    Exp1 := Exp2;
    Signif1 := BinShift(Signif1, ExpDif, '0');

  (* Exponente 2 menor *)
  end else begin
    Exp2 := Exp1;
    Signif2 := BinShift(Signif2, ExpDif, '0');

  end;
end;

procedure AddImplicitBit;
begin
  if BinToInt(Exp) = 0 then Signif := '0' + Signif
  else Signif := '1' + Signif;
end;

procedure AddSignificands;
begin
  (* A�ade 2 bits para signo y evitar posible desbordamiento y representa
   * n�meros en Ca2. Si es una resta, se hace el Ca2 del 2� operando. *)
  Signif1 := '00' + Signif1;
  Signif2 := '00' + Signif2;
  if Sign1 = '1' then Signif1 := BinCa2(Signif1);
  if Sign2 = '1' then Signif2 := BinCa2(Signif2);
  if not IsAddOperation then Signif2 := BinCa2(Signif2);

  (* Realiza operaci�n *)
  AddPositive(Signif1, Signif2, SignifRes);

  (* Guarda resultados en signo y magnitud *)
  SignRes := SignifRes[1];
  if SignRes = '1' then SignifRes := BinCa2(SignifRes);
  Delete(SignifRes, 1, 1);
end;

function GetMSBCurPos(s: string): Integer;
var
  i: Integer;
begin
  Result := -1;
  for i:= 1 to Length(s) do
    if s[i] = '1' then begin
      Result := i;
      Exit;
    end;
end;

function Normalize;
var
  MSBCurPos,   //Posici�n en la que est� el primer 1
  i: Integer;
begin
  Result := False;
  MSBCurPos := GetMSBCurPos(Signif);

  (* La mantisa es 0 *)
  if MSBCurPos < 0 then begin
    for i := 1 to Length(Exp) do Exp[i] := '0';
    Exit;
  end;

  (* La mantisa ya est� normalizada *)
  if MSBCurPos = MSBPos then
    Exit;

  (* Decrementar mantisa - incrementar exponente *)
  while MSBCurPos < MSBPos do begin
    if Pos('0', Exp) = 0 then begin //Desbordamiento - exp todo a unos
      Result := True;
      Exit;
    end;
    Signif := BinShift(Signif, 1, '0');
    AddPositive(Exp, '1', Exp);
    inc(MSBCurPos);
  end;

  (* Incrementar mantisa - decrementar exponente *)
  while MSBCurPos > MSBPos do begin
    if BinToInt(Exp) = 0 then Break;
    Signif := BinShift(Signif, -1, '0');
    SubPositive(Exp, '1', Exp);
    dec(MSBCurPos);
  end;
end;

procedure RoundSignificand;
var
  l: Integer;
begin
  l := Length(Signif);
  case RoundMethod of
    (* A + Infinito *)
    rmPlusInfinite:
      if ((Signif[l] = '1') or (Signif[l - 1] = '1'))
      and (Sign = '0') then
        AddPositive(Signif, '1', Signif);

    (* A - Infinito *)
    rmMinusInfinite:
      if ((Signif[l] = '1') or (Signif[l - 1] = '1'))
      and (Sign = '1') then
        AddPositive(Signif, '1', Signif);

    (* Al par m�s pr�ximo *)
    rmNearestOdd:
      if (Signif[l - 2] = '1') then
        AddPositive(Signif, '1', Signif);

    {
    (* Redondeo *)
    rmRoundVonNeumann:
      if (Signif[l] = '1') or (Signif[l - 1] = '1') then
        Signif[l - 2] := '1'
      else Signif[l - 2] := '0';

    (* Redondeo von Neumann *)
    rmRound:
      AddPositive(Signif, '1', Signif);
    }

  end;
  Delete(Signif, l - 1, 2);
end;

function GetAdderOperation;
begin
  Result := Sa xor Sb xor OpUser;
end;

function GetAdderResultSign;
begin
  (* Funci�n obenida a partir del dise�o de una tabla aplicando la
   * simplificaci�n de Karnaugh *)
  Result := ((Sa and (not Sb) and (not SRes)) or
            (OpUser and (not Sb) and SRes) or
            ((not OpUser) and Sb and SRes) or
            (Sa and Sb and (not SRes)))
            and 1;
end;

function AddExponents;
var
  Exp1Int, Exp2Int, ResInt, Excess: Integer;
begin
  //Calcula exceso del exponente
  if Length(Exp1) = NUM_SINGLE_EXP_LENGTH then
    Excess := NUM_SINGLE_EXP_EXCESS
  else
    Excess := NUM_DOUBLE_EXP_EXCESS;

  (* Calcula enteros correspondientes a los exponentes. Si alguno es 0, lo
   * convierte en 1 *)
  Exp1Int := BinToInt(Exp1);
  Exp2Int := BinToInt(Exp2);
  if Exp1Int = 0 then Exp1Int := 1;
  if Exp2Int = 0 then Exp2Int := 1;

  //Calcula resultado del exponente seg�n la operaci�n a realizar
  if Op = 0 then
    ResInt := Exp1Int + Exp2Int - Excess
  else
    ResInt := Exp1Int - Exp2Int + Excess;
  Result := IntToBin(ResInt, Length(Exp1));

  //Calcula desbordamientos
  Underflow := ResInt < 1;
  Overflow := ResInt > Excess * 2;
end;

procedure SolveMultUnderflow;
var
  str: string;
  i: Integer;
begin
  //Si no hubo desbordamiento, no hace nada
  if not Underflow then
    Exit;

  //Disminuye mantisa
  while BinToInt(E) <> 1 do begin
    AddPositive(E, '1', str);
    E := str;
    M := BinShift(M, 1, '0');
  end;

  //Pone exponente a 0
  for i := 1 to Length(E) do
    E[i] := '0';
end;


function MultiplySignificands(Signif1, Signif2: string): string;
var
  Len, i: Integer;
  s: string;
begin
  //Pone Result todo a '0', con el doble de bits que la entrada
  Len := Length(Signif1);
  Result := '';
  for i := 1 to Len * 2 do
    Result := Result + '0';

  //Suma los productos parciales
  for i := Len downto 1 do begin
    if Signif2[i] = '1' then begin
      AddPositive(Result, Signif1, s);
      Result := s;
    end;
    Signif1 := Signif1 + '0';
  end;
end;

function DivideSignificands(Signif1, Signif2: string): string;
var
  Len, i, CompRes: Integer;
  Res, DivLen: string;
begin
  //Duplica el n�mero de bits del dividendo
  Len := Length(Signif1);
  for i := 1 to Len * 2 do
    Signif1 := Signif1 + '0';

  //Inicializa iteraciones
  Result      := '';
  DivLen      := Copy(Signif1, 1, Len - 2);

  //Comienza a dividir (Len * 2 iteraciones)
  for i := Len - 1 to Len * 3 - 1 do begin

    //Baja el bit i y lo coloca al final de DivLen. Ahora DivLen tendr� Len bits
    DivLen := DivLen + Signif1[i];

    //Si es m�s grande que el dividendo, resta y actualiza resultado
    CompRes := BinCompare(DivLen, Signif2);
    if CompRes <= 0 then begin
      SubPositive(DivLen, Signif2, Res);
      DivLen := Res;
      Result := Result + '1';
    end else
      Result := Result + '0';

    //Quita ceros del principio de DivLen
    while (Length(DivLen) > 0) and (DivLen[1] = '0') do
      Delete(DivLen, 1, 1);

  end;
end;

function MultiplierSignifOp;
begin
  if Op = 0 then
    Result := MultiplySignificands(Signif1, Signif2)
  else
    Result := DivideSignificands(Signif1, Signif2);
end;

procedure TruncSignifAfterNormalize;
begin
  Signif := Copy(Signif, 1, Length(Signif) div 2 + 3);
end;

function GetMultResultSign;
begin
  if BinToInt(Sign1) xor BinToInt(Sign2) = 0 then
    Result := '0'
  else
    Result := '1';
end;


end.
