unit HPMultiplier;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Auxiliar, RafMsg, AskEntry,
  Text, Functions, Numbers;

type
  TfrmHPMultiplier = class(TForm)
    pnlMain: TPanel;
    imgMain: TImage;
    cbtNext: TBitBtn;
    cbtEnd: TBitBtn;
    cbtInfo: TBitBtn;
    cbtDesc: TBitBtn;
    lbl_0_1: TStaticText;
    lbl_0_2: TStaticText;
    lbl_0_3: TStaticText;
    lbl_0_4: TStaticText;
    lbl_1_1: TStaticText;
    lbl_1_2: TStaticText;
    lbl_2_1: TStaticText;
    lbl_2_2: TStaticText;
    lbl_3_1: TStaticText;
    lbl_4_1: TStaticText;
    lbl_4_2: TStaticText;
    lbl_4_3: TStaticText;
    lbl_5_1: TStaticText;
    lbl_5_2: TStaticText;
    lbl_5_3: TStaticText;
    lbl_6_1: TStaticText;
    lbl_6_2: TStaticText;
    lbl_7_1: TStaticText;
    lbl_7_2: TStaticText;
    lbl_7_3: TStaticText;
    lbl_7_4: TStaticText;
    lbl_7_5: TStaticText;
    procedure cbtInfoClick(Sender: TObject);
    procedure cbtEndClick(Sender: TObject);
    procedure cbtNextClick(Sender: TObject);
    procedure cbtDescClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SetStep(State: Boolean);
    procedure lblMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHPMultiplier: TfrmHPMultiplier;

implementation
{$R *.dfm}

const
  STAGE_NUM               = 8;
  STAGE_HEIGHT            : array[0..STAGE_NUM - 1] of Integer =
                            (45, 82, 134, 186, 275, 326, 378, 464);
var
  DescWindow: TDescriptionWindow;
  Step: Integer;

  NumRes: TFloatingPointNumber;
  AE: TAskEntry;
  S, E, M: array[0..2] of string;
  SInt: Integer;
  ExpOverflow, ExpUnderflow, NormOverflow, Overflow, DivisionBy0: Boolean;


procedure TfrmHPMultiplier.cbtInfoClick(Sender: TObject);
begin
  DescWindow.HideAndSave;
  ShowInfoText(TXT_HPMULT_INFO[Lng]);
  DescWindow.Restore;
end;

procedure TfrmHPMultiplier.cbtEndClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmHPMultiplier.cbtNextClick(Sender: TObject);
begin
  SetStep(False);
  inc(Step);
  SetStep(True);
end;

procedure TfrmHPMultiplier.cbtDescClick(Sender: TObject);
begin
  DescWindow.ToggleShowing;
end;

procedure TfrmHPMultiplier.lblMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  DescWindow.SetDescription(TStaticText(Sender).Hint);
end;

procedure TfrmHPMultiplier.FormShow(Sender: TObject);
begin
  //Texto
  Caption         := TXT_HPMULT_CAPTION[Lng];
  cbtNext.Caption := TXT_HPMULT_CBT_NEXT[Lng];
  cbtEnd.Caption  := TXT_HPMULT_CBT_END[Lng];
  cbtInfo.Caption := TXT_HPMULT_CBT_INFO[Lng];
  cbtDesc.Caption := TXT_HPMULT_CBT_DESC[Lng];
  lbl_0_1.Caption := TXT_HPMULT_LBL_EXP[Lng] + ' A';
  lbl_0_2.Caption := TXT_HPMULT_LBL_EXP[Lng] + ' B';
  lbl_0_3.Caption := TXT_HPMULT_LBL_SIGNIF[Lng] + ' A';
  lbl_0_4.Caption := TXT_HPMULT_LBL_SIGNIF[Lng] + ' B';
  lbl_1_1.Caption := TXT_HPMULT_LBL_FORMAT[Lng];
  lbl_1_2.Caption := TXT_HPMULT_LBL_FORMAT[Lng];
  lbl_2_1.Caption := TXT_HPMULT_LBL_ADD_SUB[Lng];
  lbl_2_2.Caption := TXT_HPMULT_LBL_RECODE[Lng];
  lbl_3_1.Caption := TXT_HPMULT_LBL_MULT[Lng];
  lbl_4_1.Caption := TXT_HPMULT_LBL_SIGN_CORR[Lng];
  lbl_4_2.Caption := TXT_HPMULT_LBL_CPA[Lng];
  lbl_4_3.Caption := TXT_HPMULT_LBL_CPA[Lng];
  lbl_5_1.Caption := TXT_HPMULT_LBL_INC[Lng];
  lbl_5_2.Caption := TXT_HPMULT_LBL_NORMALIZE[Lng];
  lbl_5_3.Caption := TXT_HPMULT_LBL_NORMALIZE[Lng];
  lbl_6_1.Caption := TXT_HPMULT_LBL_INC[Lng];
  lbl_6_2.Caption := TXT_HPMULT_LBL_ROUND[Lng];
  lbl_7_1.Caption := TXT_HPMULT_LBL_CONST[Lng];
  lbl_7_2.Caption := TXT_HPMULT_LBL_GEN_CONST[Lng];
  lbl_7_3.Caption := TXT_HPMULT_LBL_STATUS[Lng];
  lbl_7_4.Caption := TXT_HPMULT_LBL_RES_EXP[Lng];
  lbl_7_5.Caption := TXT_HPMULT_LBL_RES_SIGNIF[Lng];

  //Inicializa simulación
  DescWindow := TDescriptionWindow.Create(TXT_ELEMENT_DESCRIPTION[Lng]);
  Step := 0;
  SetStep(True);
end;

procedure TfrmHPMultiplier.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  SetStep(False);
  DescWindow.Free;
end;

procedure TfrmHPMultiplier.SetStep;
label
  StepAction;
var
  res, str: string;
  i: Integer;
begin

  StepAction:

  (* Acciones a realizar sólo si se activa un estado *)
  if State then case Step of

    (* Pregunta entradas. Si no son correctas, vuelve a 0
     * Primera etapa *)
    1: begin
      if not AskUserEntry(AE, 1) then begin
        Step := 0;
        goto StepAction;
      end;

      ExpandOperandsInfo(AE.Num, S, E, M, SInt);
      DivisionBy0 := (AE.OpUser = 1) and (AE.Num[1].Value = 0);
      lbl_0_1.Hint := E[0];
      lbl_0_2.Hint := E[1];
      lbl_0_3.Hint := M[0];
      lbl_0_4.Hint := M[1];
    end;

    (* Formato de mantisas *)
    2: begin
      AddImplicitBit(E[0], M[0]);
      AddImplicitBit(E[1], M[1]);
      lbl_1_1.Hint := Format(TXT_HPMULT_HINT_FMT_SIGNIF[Lng], ['A', M[0]]);
      lbl_1_2.Hint := Format(TXT_HPMULT_HINT_FMT_SIGNIF[Lng], ['B', M[1]]);
    end;

    (* Sumar exponentes *)
    3: begin
      E[2] := AddExponents(E[0], E[1], AE.OpUser, ExpOverflow, ExpUnderflow);
      str := '';
      for i := 1 to Length(M[0]) do str := str + '0';
      str := str + M[0];

      lbl_2_1.Hint := Format(TXT_HPMULT_HINT_ADD_EXP[Lng], [M[0]]);
      lbl_2_2.Hint := Format(TXT_HPMULT_HINT_RECODE[Lng], [str]);
    end;

    (* Multiplicar mantisas *)
    4: begin
      M[2] := MultiplierSignifOp(M[0], M[1], AE.OpUser);
      SolveMultUnderflow(ExpUnderflow, E[2], M[2]);

      lbl_3_1.Hint := Format(TXT_HPMULT_HINT_MULT_SIGNIF[Lng], [M[0], M[1], M[2]]);
    end;

    (* CPAs *)
    5: begin
      lbl_4_1.Hint := Format(TXT_HPMULT_HINT_CARRY_ASSIM[Lng], [M[2]]);
      lbl_4_2.Hint := lbl_4_1.Hint;
      lbl_4_3.Hint := lbl_4_1.Hint;
    end;

    (* Normalización *)
    6: begin
      E[0] := E[2];
      M[0] := M[2];
      NormOverflow := Normalize(E[2], M[2], 2);
      TruncSignifAfterNormalize(M[2]);

      lbl_5_1.Hint := Format(TXT_HPMULT_HINT_NORM_EXP[Lng], [E[0], E[2]]);
      lbl_5_2.Hint := Format(TXT_HPMULT_HINT_NORM_SIGNIF[Lng], [M[0], M[2]]);
      lbl_5_3.Hint := lbl_5_2.Hint;
    end;

    (* Redondeo *)
    7: begin
      str := M[2];
      RoundSignificand(S[2], M[2], AE.RoundMethod);
      if M[2, 1] = '1' then begin
        AddPositive(E[2], '1', res);
        Overflow := Pos('0', res) = 0;
        lbl_6_1.Hint := Format(TXT_HPMULT_HINT_ROUNDEXP_YES[Lng], [E[2], res]);
        E[2] := res;
        M[2] := BinShift(M[2], 1, '0');
      end else begin
        lbl_6_1.Hint := Format(TXT_HPMULT_HINT_ROUNDEXP_NO[Lng], [E[2]]);
      end;
      lbl_6_2.Hint := Format(TXT_HPMULT_HINT_ROUND_SIGNIF[Lng],
        [TXT_ROUND[Lng, Integer(AE.RoundMethod)], str, M[2]]);
    end;

    (* Fin *)
    8: begin
      Delete(M[2], 1, 2);
      S[2] := GetMultResultSign(S[0], S[1]);

      //Caso general
      NumRes := TFloatingPointNumber.Create(
        fiBin, AE.num[0].Format, 0, 0, S[2] + E[2] + M[2]);
      lbl_7_3.Hint := Format(TXT_HPMULT_HINT_SUCCESS[Lng], [AE.Num[0].Sign,
        AE.Num[0].Exponent, AE.Num[0].Mantissa, AE.Num[0].DecValue,
        TXT_MULTIPLICATION_SYMBOL[AE.OpUser], AE.Num[1].Sign, AE.Num[1].Exponent,
        AE.Num[1].Mantissa, AE.Num[1].DecValue, S[2], E[2], M[2],
        NumRes.DecValue]);
      NumRes.Free;

      //Casos particulares
      if Overflow then begin
        BinZeros(M[2]);
        BinOnes(E[2]);
        lbl_7_3.Hint := TXT_OVERFLOW[Lng];
      end else if DivisionBy0 then begin
        BinZeros(M[2]);
        BinOnes(E[2]);
        lbl_7_3.Hint := TXT_DIVISION_BY_ZERO[Lng];
      end;

      lbl_7_1.Hint := Format(TXT_HPMULT_HINT_CONST[Lng], [E[2]]);
      lbl_7_2.Hint := Format(TXT_HPMULT_HINT_CONST_GEN[Lng], [M[2]]);
      lbl_7_4.Hint := E[2];
      lbl_7_5.Hint := M[2];
    end;

  end;

  (* Acciones a realizar al activar y desactivar *)
  SetStage(Self, Step - 1, State, STAGE_HEIGHT);
  DescWindow.SetPosition(Step > 4);
  cbtNext.Enabled := Step < STAGE_NUM;

end;

end.
