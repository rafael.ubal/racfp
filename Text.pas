unit Text;

interface

const
  LANGUAGE_NUM                  = 2;

type
  TLangStr = array[0..LANGUAGE_NUM - 1] of string;

var
  Lng: Integer;

const

  (* Acerca de... *)

  TXT_ABOUT_CAPTION: TLangStr = (
    'Acerca de...', 'About...');
  TXT_ABOUT_TITLE: TLangStr = (
    'Proyecto Final de Carrera', 'Final-course project');
  TXT_ABOUT_SUBTITLE: TLangStr = (
    'Simulador de circuitos en coma flotante',
    'Floating point circuits simulator');
  TXT_ABOUT_ITEMS: TLangStr = (
    'Autor:'#13'Director:'#13'Fecha:'#13'Centro:'#13#13'Universidad:'#13#13 +
    'Ciudad:'#13'Pa�s:',
    'Author:'#13'Supervisor:'#13'Date:'#13'Center:'#13#13'University:'#13#13 +
    'City:'#13'Country:');
  TXT_ABOUT_DESC: TLangStr = (
    'Rafael Ubal Tena'#13'Julio Sahuquillo Borr�s'#13'Marzo 2004'#13 +
    'Escuela T�cnica Superior de Inform�tica Aplicada'#13 +
    'Universidad Polit�cnica de Valencia'#13'Valencia'#13'Espa�a',

    'Rafael Ubal Tena'#13'Julio Sahuquillo Borr�s'#13'March 2004'#13 +
    'Escuela T�cnica Superior de Inform�tica Aplicada'#13 +
    'Universidad Polit�cnica de Valencia'#13'Valencia'#13'Spain');


  (*** Palabras concretas ***)

  TXT_INSERT_NUMBER: TLangStr = (
    'Insertar n�mero:', 'Insert number:');
  TXT_OVERFLOW: TLangStr = (
    'Desbordamiento', 'Overflow');
  TXT_ELEMENT_DESCRIPTION: TLangStr = (
    'Descripci�n del elemento', 'Element description');
  TXT_AFFIRMATION: TLangStr = (
    's�', 'does');
  TXT_NEGATION: TLangStr = (
    'no', 'does not');
  TXT_YES: TLangStr = (
    'S�', 'Yes');
  TXT_NO: TLangStr = (
    'No', 'No');
  TXT_DIVISION_BY_ZERO: TLangStr = (
    'Divisi�n por 0', 'Division by 0');
  TXT_POSITIVE: TLangStr = (
    'positivo', 'positive');
  TXT_NEGATIVE: TLangStr = (
    'negativo', 'negative');
  TXT_MANTISSA: TLangStr = ('Mantisa', 'Mantissa');
  TXT_EXPONENT: TLangStr = ('Exponente', 'Exponent');
  TXT_SIGN: TLangStr = ('Signo', 'Sign');

  TXT_ADDITION_NAME: array[0..LANGUAGE_NUM - 1, 0..1] of string = ((
    'sumar', 'addition'), (
    'restar', 'subtraction'));
  TXT_MULTIPLICATION_NAME: array[0..LANGUAGE_NUM - 1, 0..1] of string = ((
    'multiplicar', 'multiplication'), (
    'divisi�n', 'division'));
  TXT_ADDITION_SYMBOL: array[0..1] of string = (
    '+', '-');
  TXT_MULTIPLICATION_SYMBOL: array[0..1] of string = (
    '*', '/');

  TXT_BUT_OK: TLangStr = (
    '&Aceptar', '&OK');
  TXT_BUT_CANCEL: TLangStr = (
    '&Cancelar', '&Cancel');
  TXT_BUT_CLOSE: TLangStr = (
    '&Cerrar', '&Close');
  TXT_BUT_YES: TLangStr = (
    '&S�', '&Yes');
  TXT_BUT_NO: TLangStr = (
    '&No', '&No');
  TXT_BUT_HELP: TLangStr = (
    'A&yuda', '&Help');
  TXT_BUT_END: TLangStr = (
    '&Finalizar', '&End');
  TXT_BUT_NEXT: TLangStr = (
    '&Siguiente', '&Next');


  (*** Ventana de idioma ***)

  TXT_LANGUAGE: TLangStr = (
    'Idioma', 'Language');
  TXT_LANGUAGE_SELECT: TLangStr = (
    'Seleccionar idioma', 'Select language');


  (*** Ventana principal ***)

  TXT_MAIN_CAPTION: TLangStr = (
    'RAC-FP', 'RAC-FP');
  TXT_MAIN_CONVERSION: TLangStr = (
    'Conversi�n', 'Conversion');
  TXT_MAIN_TABLES: TLangStr = (
    'Tablas', 'Tables');
  TXT_MAIN_ABOUT: TLangStr = (
    'Acerca de...', 'About...');
  TXT_MAIN_LANGUAGE: TLangStr = (
    'Idioma...', 'Language...');
  TXT_MAIN_EXIT: TLangStr = (
    'Salir', 'Exit');
  TXT_MAIN_ALGORITHMS: TLangStr = (
    'Algoritmos', 'Algorithms');
  TXT_MAIN_CIRCUITS: TLangStr = (
    'Circuitos HP', 'HP Circuits');
  TXT_MAIN_MULTDIV: TLangStr = (
    'Mult./Div.', 'Mult./Div.');
  TXT_MAIN_ADDSUB: TLangStr = (
    'Suma/resta', 'Add./Subtr.');


  (*** PreguntaNumero ***)

  TXT_ASKNUM_ERR_INTTOSTR: TLangStr = (
    'No se ha introducido un n�mero de bits v�lido'#13 +
    'para la mantisa o el exponente.',
    'The value input for the exponent or mantissa bits'#13 +
    'is not a valid number');
  TXT_ASKNUM_ERR_EXPRANGE: TLangStr = (
    'El n�mero de bits del exponente debe estar entre 1 y 11.',
    'The number of bits for the exponent must lie between 1 and 11.');
  TXT_ASKNUM_ERR_MANTRANGE: TLangStr = (
    'El n�mero de bits de la mantisa debe estar entre 1 y 52.',
    'The number of bits for the mantissa must lie between 1 and 52.');
  TXT_ASKNUM_ERR_BINDIGIT: TLangStr = (
    'Los d�gitos de un n�mero binario s�lo pueden ser "0" � "1".'#13 +
    'Tambi�n se pueden incluir espacios en blanco.',
    'The binary number digits can only be "0" or "1".'#13 +
    'Blank spaces can also be included.');
  TXT_ASKNUM_ERR_BINLENGTH: TLangStr = (
    'El n�mero de bits introducido no coincide con el formato especificado.',
    'The input number of bits does not match the specified output format.');
  TXT_ASKNUM_ERR_DECFORMAT: TLangStr = (
    'El n�mero decimal introducido no es v�lido.'#13 +
    'Compruebe que el separador decimal concuerda con la configuraci�n '#13 +
    'regional de su sistema operativo (pruebe con "." o ",").',
    'The input decimal number is not valid.'#13 +
    'Verify that the decimal separator matches with the regional '#13 +
    'configuration of your operating system. Check with "." and ",".');
  TXT_ASKNUM_ERR_UNDERFLOW: TLangStr = (
    'El n�mero es demasiado peque�o para el formato escogido.'#13 +
    'El tama�o del exponente o la precisi�n de la mantisa lo truncar�an '#13 +
    'haciendo su representaci�n equivalente a la del "0".',
    'This number is too small for the chosen format.'#13 +
    'The exponent size or the mantissa precision would truncate it '#13 +
    'and make its representation equivalent to "0".');
  TXT_ASKNUM_ERR_OVERFLOW: TLangStr = (
    'El n�mero provoca un desbordamiento superior para el formato escogido.',
    'This number causes an overflow in the chosen representation format.');
  TXT_ASKNUM_ERR_HEXFORMAT: TLangStr = (
    'El n�mero hexadecimal introducido contiene caracteres ilegales.',
    'The input hexadecimal number contains illegar characters.');
  TXT_ASKNUM_ERR_HEXLONG: TLangStr = (
    'La representaci�n hexadecimal introducida contiene m�s bits que el'#13 +
    'formato de representaci�n especificado.',
    'The input hexadecimal representation contains more bits than the'#13 +
    'specified representation format.');

  TXT_ASKNUM_CAPTION: TLangStr = ('Introduzca el n�mero', 'Enter the number');
  TXT_ASKNUM_OUTPUTFMT: TLangStr = ('Formato de salida:', 'Output format:');
  TXT_ASKNUM_INPUTFMT: TLangStr = ('Formato de entrada:', 'Input format:');
  TXT_ASKNUM_BIN: TLangStr = ('Binario', 'Binary');
  TXT_ASKNUM_DEC: TLangStr = ('Decimal', 'Decimal');
  TXT_ASKNUM_HEX: TLangStr = ('Hexadecimal', 'Hexadecimal');
  TXT_ASKNUM_EXP: TLangStr = ('Exponente:', 'Exponent:');
  TXT_ASKNUM_MANT: TLangStr = ('Mantisa:', 'Mantissa:');
  TXT_ASKNUM_SINGLE: TLangStr = (
    'IEEE de simple precisi�n (32 bits)',
    'IEEE single precision (32-bit)');
  TXT_ASKNUM_DOUBLE: TLangStr = (
    'IEEE de doble precisi�n (64 bits)',
    'IEEE double precision (64-bit)');
  TXT_ASKNUM_CUSTOM: TLangStr = ('Formato personalizado', 'Custom format');


  (*** Informaci�n del n�mero ***)

  TXT_NUMINFO_NEW: TLangStr = ('&Nuevo n�mero', '&New number');
  TXT_NUMINFO_SINGLE: TLangStr = (
    'IEEE de simple precisi�n',
    'IEEE single precision');
  TXT_NUMINFO_DOUBLE: TLangStr = (
    'IEEE de doble precisi�n',
    'IEEE double precision');
  TXT_NUMINFO_NO: TLangStr = ('No', 'No');
  TXT_NUMINFO_PRECISION: TLangStr = (
    'Signo - 1 bit,  Exponente - %d bit(s),  Mantisa - %d bit(s).',
    'Sign - 1 bit,  Exponent - %d bit(s),  Mantissa - %d bit(s).');
  TXT_NUMINFO_EXP: TLangStr = (
    'Exp = %d, internamente representado en exceso %d.'#13,
    'Exp = %d, internally represented with bias %d.'#13);
  TXT_NUMINFO_SUBNORMAL: TLangStr = (
    'Todos los bits del exponente son 0, luego el n�mero representado no ' +
    'est� normalizado, es decir, el bit impl�cito para la mantisa ser� "0," ' +
    'en lugar de "1,".',
    'All bits in the exponent are 0, so the represented number is not ' +
    'normalized, that is, the implicit bit for the mantissa is "0," ' +
    'instead of "1,".');
  TXT_NUMINFO_ZERO: TLangStr = (
    'Todos los bits del exponente y de la mantisa son 0, luego se representa ' +
    'el valor 0 (no normalizado).',
    'All bits in the exponent and mantissa are 0, so the represented number ' +
    'is 0 (not normalized).');
  TXT_NUMINFO_NORMAL: TLangStr = (
    'El exponente est� entre -%d y %d, luego el n�mero representado est� ' +
    'normalizado, con el bit impl�cito en la mantisa "1,".',
    'The exponent lies between -%d and %d, so the represented number is ' +
    'normalized with the implicid bit "1," in the mantissa.');
  TXT_NUMINFO_INF: TLangStr = (
    'Todos los bits del exponente son 1 y todos los de la mantisa son 0, ' +
    'luego se representa el valor infinito.',
    'All bits in the exponent are 1 and all bits in the mantissa are 0, ' +
    'so infinite is represented.');
  TXT_NUMINFO_NAN: TLangStr = (
    'Todos los bits del exponente son 1 y la mantisa NO es igual a 00...0, ' +
    'por lo que se est� representando el valor especial NaN (not a number), ' +
    'utilizado para resultados de operaciones inv�lidas.',
    'All bits in the exponent are 1 and the significant is NOT 00...0, so the ' +
    'represented number is the special value NaN (NotANumber), which is used ' +
    'for results of invalid operations.');
  TXT_NUMINFO_WRD_NORMALIZED: TLangStr = (
    'Normalizado', 'Normalized');
  TXT_NUMINFO_WRD_NOT_NORMALIZED: TLangStr = (
    'No normalizado', 'Not normalized');
  TXT_NUMINFO_WRD_NAN: TLangStr = (
    'NaN', 'NaN');
  TXT_NUMINFO_WRD_INFINITE: TLangStr = (
    'Infinito', 'Inifinite');
  TXT_NUMINFO_WRD_ZERO: TLangStr = (
    'Cero', 'Zero');
  TXT_NUMINFO_CAPTION: TLangStr = (
    'Informaci�n del n�mero', 'Information about the number');
  TXT_NUMINFO_GRP_RESULT: TLangStr = (
    'General', 'General');
  TXT_NUMINFO_LBL_HEXADECIMAL: TLangStr = (
    'Representaci�n hexadecimal:', 'Hexadecimal representation:');
  TXT_NUMINFO_LBL_BINARY: TLangStr = (
    'Representaci�n binaria (signo-exponente-mantisa):',
    'Binary representation (sign-exponent-mantissa):');
  TXT_NUMINFO_LBL_DECIMAL: TLangStr = (
    'Representaci�n decimal:', 'Decimal representation:');
  TXT_NUMINFO_GRP_DETAILS: TLangStr = (
    'Detalles:', 'Details:');
  TXT_NUMINFO_LBL_PRECISION: TLangStr = (
    'Precisi�n:', 'Precision:');
  TXT_NUMINFO_LBL_TYPE: TLangStr = (
    'Tipo:', 'Type:');
  TXT_NUMINFO_LBL_DESCRIPTION: TLangStr = (
    'Descripci�n:', 'Description:');
  TXT_NUMINFO_LBL_STANDARD: TLangStr = (
    'Est�ndar:', 'Standard:');


  (*** Tablas ***)
  
  TXT_TBLS_CBT_MORE: TLangStr = (
    'M�s...', 'More...');
  TXT_TBLS_LBL_ADDER_OPERATION: TLangStr = (
    'Operaci�n efectiva suma/resta', 'Effective operation add/sub');
  TXT_TBLS_LBL_ADDER_RESULT_SIGN: TLangStr = (
    'Signo del resultado suma/resta', 'Add/sub result sign');
  TXT_TBLS_LBL_MULT_RESULT_SIGN: TLangStr = (
    'Signo del resultado mult/div', 'Mult/div result sign');
  TXT_TBLS_CAPTION: TLangStr = (
    'Tablas', 'Tables');

  TXT_TBLS_ADDER_OPERATION: TLangStr = (
    '  Esta tabla representa la operaci�n que realizar� el sumador/restador ' +
    'de mantisas en un circuito de suma/resta en coma flotante. ' +
    'La operaci�n efectiva del subcircuito depende de tres factores, cada uno ' +
    'de ellos representados en una de las tres primeras columnas de la tabla. '#13 +
    '  Los dos primeros factores son los signos de los operandos introducidos ' +
    'por el usuario. El tercer factor es la operaci�n que haya especificado ' +
    'el usuario en un principio.'#13 +
    '  Para calcular la funci�n que devuelve los valores de la �ltima columna, ' +
    'podr�amos haber utilizado funciones de maxit�rminos o minit�rminos y ' +
    'simplificaci�n por tablas de Karnaugh, pero se puede observar que una ' +
    'forma sencilla de calcular la 4� columa es "SRes = Sa XOR Sb XOR OpUser".',

    '  This table represents the operation that the mantissas adder/subtracter ' +
    'will perform inside the floating point addition/subtraction circuit. ' +
    'The effective operation of this subcircuit depends on three factors, ' +
    'each one represented in one of the three first columns of the table. '#13 +
    '  The two first factors are the signs of the by the user introduced ' +
    'operands. The third factor is the operation the user specified in the ' +
    'beginning.'#13 +
    '  In order to obtain the function that returns the values of the last ' +
    'column, we could have used the Karnaugh method. But with boolean ' +
    'arithmetic we can notice that a simple way of doing this is by means of ' +
    'the formula "SRes = Sa XOR Sb XOR OpUser".');

  TXT_TBLS_MULT_RESULT_SIGN: TLangStr = (
    '  El signo del resultado de una multiplicaci�n o divisi�n es positivo ' +
    'si los dos operandos tienen el mismo signo, y negativo en caso contrario. ' +
    'Por lo tanto, la forma de calcularlo ser� tan sencilla como "SRes = Sa XOR Sb".',

    '  The result''s sign of a multiplication or division is positive if both ' +
    'operands have the same sign, and negative otherwise. So the way to ' +
    'calculate this sign is simply "SRes = Sa XOR Sb".');

  TXT_TBLS_ADDER_RESULT_SIGN: TLangStr = (
    '  Esta tabla es consultada por el circuito sumador/restador en coma ' +
    'flotante en el momento de colocar el signo al resultado final de la operaci�n. ' +
    'Este signo depender� de 4 factores: los signos de los operandos, la ' +
    'operaci�n introducida por el usuario y el signo obtenido por el sumador/' +
    'restador de mantisas.'#13 +
    '  La tabla puede ser rellenada a mano comprobando cuidadosamente el ' +
    'comportamiento que debe tener el circuito en cada caso y, una vez hecho esto, ' +
    'puede calcularse la funci�n que devuelve el signo del resultado a partir ' +
    'de tablas de Karnaugh y simplificaci�n.'#13 +
    '  La funci�n resultante ser�a: "SResFinal = Sa��Sb��SRes + OpUser��Sb�' +
    'SRes + �OpUser�Sb�SRes + Sa�Sb��SRes".',

    '  This table is consulted by the floating point adder/subtracter circuit ' +
    'when establishing the result''s sign at the end of the operation. ' +
    'This sign depends on four factors: the operands signs, the operation ' +
    'introduced by the user and the by the mantissas adder/subtracter ' +
    'obtained result''s sign.'#13 +
    '  The table can be filled out by hand by carefully checking the ' +
    'behaviour that should have the circuit in each case. After this, the ' +
    'function that returns the values of the table can be calculated by ' +
    'means of the Karnaugh method.'#13 +
    ' The resulting function is: "SFinalRes = Sa��Sb��SRes + OpUser��Sb�' +
    'SRes + �OpUser�Sb�SRes + Sa�Sb��SRes".');


  (*** Redondeo ***)

  TXT_ROUND: array[0..LANGUAGE_NUM - 1, 0..3] of string = ((
    'En este caso se aplicar� el redondeo a + infinito. ' +
    'Esta t�cnica consiste en sumar 1 al bit menos significativo retenido de ' +
    'los n�meros positivos si alg�n bit de guarda difiere de 0, y truncar ' +
    'en el caso de los n�meros negativos.',

    'En este caso se aplicar� el redondeo a - infinito. ' +
    'Esta t�cnica consiste en sumar 1 al bit menos significativo retenido de ' +
    'los n�meros negativos si alg�n bit de guarda difiere de 0, y truncar ' +
    'en el caso de los n�meros positivos.',

    'En este caso se aplicar� redondeo a 0. ' +
    'Esta t�cnica consiste en truncar la mantisa, eliminando los bits de ' +
    'guarda, de forma que el n�mero resultante se acerque a 0 sea positivo ' +
    'o negativo.',

    'En este caso se aplicar� redondeo al par m�s pr�ximo. ' +
    'Esta t�cnica consiste en sumar 1 al �ltimo bit de guarda s�lo en caso de ' +
    'que el �ltimo bit retenido sea 1. Si este �ltimo es 0, se trunca.'), (

    'In this case, we apply rounding to + infinite. ' +
    'This technique consists of adding 1 to the least significant retained ' +
    'bit of the positive numbers if some guard bit differs from 0, and ' +
    'truncating in case of the negative numbers.',

    'In this case, we apply rounding to - infinite. ' +
    'This technique consists of adding 1 to the least significant retained ' +
    'bit of the negative numbers if some guard bit differs from 0, and ' +
    'truncating in the case of the positive numbers.',

    'In this case, we apply rounding to 0. ' +
    'This technique consists of truncating the mantissa, deleting the ' +
    'guard bits, so that the resulting numbers is nearer to 0 either for ' +
    'the positive numbers or for the negative ones.',

    'In this case we apply rounding toward the nearest even. ' +
    'This technique consists of adding 1 to the last guard bit only if the ' +
    'last retained bit is 1, and then truncating.'
    ));


  (*** Pregunta entrada ***)

  TXT_ASKENTRY_OP_NAME: array[0..LANGUAGE_NUM - 1, 0..1, 0..1] of string = (
    (('Suma', 'Resta'), ('Multiplicaci�n', 'Divisi�n')),
    (('Addition', 'Subtraction'), ('Multiplication', 'Division')));
  TXT_ASKENTRY_INSERT_NUMBER: TLangStr = (
    'Introduce un n�mero', 'Insert a number');
  TXT_ASKENTRY_ERR_INPUT: TLangStr = (
    'La entrada "%s" no es un n�mero decimal de simple precisi�n v�lido.',
    'The input "%s" is not a valid single precision decimal number.');
  TXT_ASKENTRY_ERR_PRECISION: TLangStr = (
    'La precisi�n de los dos n�meros de entrada debe coincidir.',
    'The precision of both numbers must be the same.');
  TXT_ASKENTRY_ERR_NAN_OR_INF: TLangStr = (
    'No se puede operar con Infinito o NaN',
    'The operands cannot be Infinite or NotANumber');
  TXT_ASKENTRY_RES_ADJUST: TLangStr = (
    'Ajuste del resultado', 'Result adjustment');
  TXT_ASKENTRY_ROUND: array[0..LANGUAGE_NUM - 1, 0..3] of string = ((
    'A + infinito', 'A - infinito', 'A 0', 'Al par m�s pr�ximo'), (
    'Toward + infinite', 'Toward - infinite', 'Toward 0', 'Toward the nearest even'));
  TXT_ASKENTRY_ACTION: TLangStr = (
    'Acci�n a realizar', 'Action to carry out');
  TXT_ASKENTRY_CAPTION: TLangStr = (
    'Entrada de la operaci�n...', 'Operation input...');
  TXT_ASKENTRY_INPUT: TLangStr = (
    'Valores de entrada', 'Input values');


  (*** Algoritmo sumador ***)

  TXT_ALGADD_CAPTION: TLangStr = (
    'Algoritmo gen�rico de suma/resta en coma flotante',
    'Generic floating point addition/subtraction algorithm');
  TXT_ALGADD_LBL_INIT: TLangStr = (
    'Inicio', 'Start');
  TXT_ALGADD_LBL_STEP1: TLangStr = (
    '1. Comparar exponentes de los n�meros. Desplazar el n�mero menor a la ' +
    'derecha hasta que su exponente coincida con el mayor.',
    '1. Compare both exponents. Shift the smallest number to the right until ' +
    'its exponent matches up with the greatest one.');
  TXT_ALGADD_LBL_STEP2: TLangStr = (
    '2. Sumar/restar las mantisas.',
    '2. Add/Subtract mantissas.');
  TXT_ALGADD_LBL_STEP3: TLangStr = (
    '3. Normalizar la suma, ya sea desplazando a la derecha e incrementando el ' +
    'exponente o a la izquierda y decrement�ndolo.',
    '3. Normalize the addition, either by right shifting and increasing the ' +
    'exponent or by left shifting and decreasing it.');
  TXT_ALGADD_LBL_OVERFLOW: TLangStr = (
    '�Desbordamiento superior?', 'Overflow?');
  TXT_ALGADD_LBL_EXCEPTION: TLangStr = (
    'Excepci�n', 'Exception');
  TXT_ALGADD_LBL_STEP4: TLangStr = (
    '4. Redondear la mantisa al n�mero de bits apropiado.',
    '4. Round the mantissa to the appropriate number of bits.');
  TXT_ALGADD_LBL_NORMALIZED: TLangStr = (
    '�Todav�a est� normalizado?', 'Yet normalized?');
  TXT_ALGADD_LBL_END: TLangStr = (
    'Fin', 'End');
  TXT_ALGADD_STEP: array[0..LANGUAGE_NUM - 1, 0..11] of string = ((
    'Elige 2 n�meros, la acci�n a realizar y el m�todo de redondeo. ' +
    'Para m�s opciones acerca de la representaci�n de los n�meros a sumar/restar, pulsa los botones "..." que se encuentran a la derecha de �stos.' +
    'Por defecto se asumir� que la entrada se proporciona en formato decimal y que los n�meros se representar�n en IEEE de precisi�n simple (32-bit).',

    'Los n�meros con los que se operar� repesentados en Signo-Exponente-Mantisa son:'#13 +
    '%s  %s  %s  (=%s)'#13'%s  %s  %s  (=%s)',

    'En primer lugar, se comprobar� que los operandos son v�lidos, es decir, ni INF ni NaN.'#13 +
    'Antes de comenzar las operaciones, se a�adir� el bit impl�cito a la mantisa (1 si estaba normalizada y 0 si no lo estaba). ' +
    'Adem�s se le a�adir�n 2 bits al final a "0" que servir�n para no perder precisi�n a la hora de deplazar. ' +
    'Estos bits ser�n luego tenidos en cuenta por el algoritmo de redondeo.'#13 +
    'M1 = %s'#13'M2 = %s',

    'A continuaci�n se comparar�n los exponentes. ' +
    'La mantisa correspondiente al exponente menor se desplazar� tantas posiciones a la derecha como unidades diferencien a los exponentes. ' +
    'Tras esta operaci�n, los exponentes ser�n iguales y ya se podr� operar con las mantisas. ' +
    'Quedar� como sigue (Exponente-Mantisa):'#13 +
    'E1M1 = %s  %s'#13'E2M2 = %s  %s',

    'La operaci�n efectiva que realizar� el sumador/restador de mantisas depende del signo de los operandos y de la operaci�n seleccionada por el usuario. ' +
    'Esta operaci�n efectiva puede consultarse en una tabla o calcularse mediante la siguiente f�rmula:'#13 +
    'Op(0=Suma/1=Resta) = Sa XOR Sb XOR OpUsuario'#13 +
    'En este caso: %d XOR %d XOR %d = %d, luego la acci�n efectiva ser� la de %s.',

    'Se opera con las mantisas. Para ello a�adiremos 1 bit a "0" m�s a la izquierda, ' +
    'que servir� para evitar que la operaci�n provoque un desbordamiento. ' +
    'La operaci�n quedar�a as�: '#13 +
    '       %s'#13 +
    ' %s     %s'#13 +
    '_______________________________________________________________'#13 +
    '=   %s %s  (representado en Signo Y Magnitud)',

    'Ahora se normalizar� el resultado. Hay que recordar que hemos a�adido un bit para capturar los posibles desbordamientos, ' +
    'luego el primer bit significativo (bit a "1") debe quedar en segundo lugar empezando por la izq. ' +
    'El exponente se deber� modificar seg�n el n�mero de desplazamientos realizados en la mantisa.'#13 +
    'Antes (Exp-M):  %s %s'#13' Normalizando:  %s %s',

    'La operaci�n de normalizaci�n puede provocar un desbordamiento, ya que es posible que haya que incrementar el exponente. ' +
    'En este caso, %s se dar� esa situaci�n.',

    'Ha ocurrido un desbordamiento en el exponente. ' +
    'La suma de los dos n�meros especificados no es representable en IEEE.',

    'La �ltima operaci�n es ajustar el resultado obtenido. %s'#13 +
    '%s'#13'%s',

    'El redondeo puede haber provocado una desnormalizaci�n de la mantisa, lo que deber�a llevarnos a un paso anterior del algoritmo. ' +
    'En este caso, %s se da esa situaci�n.',

    'Por �ltimo, eliminaremos lo que ser� el bit impl�cito en la mantisa. ' +
    'El signo del resultado lo obtendremos a partir de los signos de los operandos, la operaci�n a realizar y el signo del resultado de la operaci�n con las mantisas. '#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)'

  ), (
    'Choose two numbers, the action to carry out and the rounding method. ' +
    'For more options about numbers'' representation, push the "..." buttons. ' +
    'By default an input in decimal format and a single precision number ' +
    '(32-bit) are assumed.',

    'The operands represented in Sign-Exponent-Mantissa are:'#13 +
    '%s  %s  %s  (=%s)'#13'%s  %s  %s  (=%s)',

    'First it''s checked that the operands are valid, that is neither INF nor ' +
    'NaN. Before beginning with the operation, the implicit bit is added to ' +
    'the mantissa (1 if it was normalized and 0 otherwise). Apart of ' +
    'this, two additional bits "0" are added at the end to avoid a loss of ' +
    'precission when shifting. These bits are then necessary in the rounding ' +
    'stage.'#13'M1 = %s'#13'M2 = %s',

    'Next both exponents are compared. The mantissa that belongs to the ' +
    'lowest exponent is right shifted and this exponent increased until ' +
    'both exponets match. After this they will be equal and we will be ' +
    'able to operate with the mantissas. They will look like this:'#13 +
    'E1M1 = %s  %s'#13'E2M2 = %s  %s',

    'The effective operation that the adder/subtracter of mantissas ' +
    'will carry out depends on the sign of the operands and the operation ' +
    'selected by the user. This effective operation can be looked up in a ' +
    'table or calculated by means of this formula:'#13 +
    'Op(0=Add/1=Sub) = Sa XOR Sb XOR OpUser'#13 +
    'In this case: %d XOR %d XOR %d = %d, so the effective action will be %s.',

    'The adder/subtracter operates with the mantissas. A "0"-bit is added ' +
    'in the left side of them in order to avoid a possible overflow: '#13 +
    '       %s'#13 +
    ' %s     %s'#13 +
    '_______________________________________________________________'#13 +
    '=   %s %s  (represented in Sign and Magnitude)',

    'The result is normalized. We must remember the added bit in the left, ' +
    'so the first significant bit ("1") must be in the second left position. ' +
    'The exponent must be also modified depending on the number of positions ' +
    'the mantissa was shifted.'#13 +
    'Before (Exp-M):  %s %s'#13'   Normalizing:  %s %s',

    'The normalization can cause an overflow due to the possibility of ' +
    'having to increase the exponent. This situation %s occur in this case.',

    'An overflow in the exponent has occured. The addition of the ' +
    'specified numbers is not representable in IEEE.',

    'The last operation is the adjustment of the obtained result. %s'#13 +
    '%s'#13'%s',

    'Rounding can have caused a denormalization of the mantissa, which ' +
    'should bring the algorithm a step backward. This situation %s occur ' +
    'in this case.',

    'Finally, we remove the implicit bit of the mantissa. The result''s ' +
    'sign is obtained from the operands'' signs, the operation to carry out ' +
    'and the sign of the mantissas operation.'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)'));


  (*** Sumador HP ***)

  TXT_HPADD_LBL_EXP: TLangStr = (
    'Exponente', 'Exponent');
  TXT_HPADD_LBL_SIGNIF: TLangStr = (
    'Mantisa', 'Mantissa');
  TXT_HPADD_LBL_FORMAT: TLangStr = (
    'Formato', 'Format');
  TXT_HPADD_LBL_EXPCOMP: TLangStr = (
    'Comparador de exponentes', 'Exponent comparer');
  TXT_HPADD_LBL_MUX: TLangStr = (
    'MUX', 'MUX');
  TXT_HPADD_LBL_SWAP: TLangStr = (
    'Intercambio', 'Swap');
  TXT_HPADD_LBL_ALIGN_SHIFT: TLangStr = (
    'Registro de desplazamiento alineador', 'Alignment shifter');
  TXT_HPADD_LBL_ADD_SUB: TLangStr = (
    'Sumador/restador', 'Adder/subtracter');
  TXT_HPADD_LBL_INC: TLangStr = (
    'Incremento', 'Increment');
  TXT_HPADD_LBL_RIGHT_SHIFT: TLangStr = (
    'Registro de despl. a derechas', 'Right shifter');
  TXT_HPADD_LBL_PRI_ENC: TLangStr = (
    'Codificador de prioridad', 'Priority encoder');
  TXT_HPADD_LBL_DEC: TLangStr = (
    'Decremento', 'Decrement');
  TXT_HPADD_LBL_LEFT_SHIFT: TLangStr = (
    'Registro de despl. a izquierdas', 'Left shifter');
  TXT_HPADD_LBL_ROUND_INC: TLangStr = (
    'Redondeo (incremento)', 'Round (increment)');
  TXT_HPADD_LBL_CONST: TLangStr = (
    'Constante', 'Constant');
  TXT_HPADD_LBL_GEN_CONST: TLangStr = (
    'Generador de constante', 'Generate constant');
  TXT_HPADD_LBL_STATUS: TLangStr = (
    'Estado', 'Status');
  TXT_HPADD_LBL_EXP_RES: TLangStr = (
    'Exponente resultante', 'Resulting exponent');
  TXT_HPADD_LBL_SIGNIF_RES: TLangStr = (
    'Mantisa resultante', 'Resulting mantissa');
  TXT_HPADD_CBT_NEXT: TLangStr = (
    'Sig', 'Next');
  TXT_HPADD_CBT_END: TLangStr = (
    'Fin', 'End');
  TXT_HPADD_CBT_INFO: TLangStr = (
    'Info', 'Info');
  TXT_HPADD_CBT_DESC: TLangStr = (
    'Desc', 'Desc');
  TXT_HPADD_CAPTION: TLangStr = (
    'Circuito Sumador/Restador Hewlett Packard',
    'Hewlett Packard Adder/Subtracter Circuit');

  TXT_HPADD_HINT_FORMAT_SIGNIF: TLangStr = (
    'Se le da formato a la mantisa %s a�adi�ndole el bit impl�cito, de forma ' +
    'que pueda ser tratada correctamente en etapas futuras.'#13'%s',

    'The mantissa %s is formated by adding the implicit bit, so that it can ' +
    'be handled correctly in future stages.'#13'%s');

  TXT_HPADD_HINT_EXPCOMP: TLangStr = (
    'El comparador de exponentes busca el exponente mayor y le pasa su ' +
    'decisi�n al multiplexor de esta etapa. En este caso, el mayor exponente ' +
    'es el %s (%s). '#13 +
    'La forma en que se comparan los exponentes es calculando su diferencia y ' +
    'comprobando el signo de �sta, pero adem�s, se conservar� el valor de esta ' +
    'diferencia para pas�rselo en la siguiente etapa al desplazador de mantisa.',

    'The exponent comparer searches the greatest exponent and sends it ' +
    'to the MUX in this stage. In this case, the greatest exponent is the ' +
    'exponent %s (%s). '#13 +
    'The exponents are compared by calculating their difference and checking' +
    'its sign. Apart of this, the result of this difference is saved and sent to the ' +
    'mantissa shifter in the next stage.');

  TXT_HPADD_HINT_MUX: TLangStr = (
    'El multiplexor recibe informaci�n desde el comparador. �ste le indice que ' +
    'el mayor exponente es el %s, por lo tanto, el multiplexor se encargar� ' +
    'de pasar �nicamente este exponente a las siguientes etapas del circuito. ' +
    'Este exponente ser� el exponente del resultado:'#13'%s',

    'The comparer tells the MUX that the greatest exponent is %s, so the MUX ' +
    'will send only this exponent to the next circuit stages. This exponent ' +
    'will be the result''s exponent:'#13'%s');

  TXT_HPADD_HINT_SWAP_A: TLangStr = (
    'El intercambiador A (tambi�n es un MUX) debe hacer pasar a la siguiente ' +
    'etapa la mantisa correspondiente al n�mero con MENOR exponente, ya ' +
    'que en las siguientes etapas ser� esta mantisa la que deber� desplazarse. ' +
    'La mantisa a desplazar es:'#13'%s',

    'The swapper A (also a MUX) must send to the next stage the ' +
    'mantissa which belongs to the number with SMALLEST exponent. In the ' +
    'next stages this mantissa will be the one that will have to be ' +
    'shifted:'#13'%s');

  TXT_HPADD_HINT_SWAP_B: TLangStr = (
    'El intercambiador B (tambi�n es un MUX) deja pasar a las siguientes ' +
    'etapas la mantisa correspondiente al n�mero con MAYOR exponente, ya ' +
    'que esta mantisa no ser� modificada:'#13'%s',

    'The swapper B (also a MUX) sends to the next stages the mantissa ' +
    'which belongs to the number with GREATEST exponent. This mantissa ' +
    'won''t have to be shifted:'#13'%s');

  TXT_HPADD_HINT_ALIGN_SHIFT: TLangStr = (
    'Este desplazador recibe del comparador de exponentes de la etapa anterior ' +
    'la diferencia de ambos exponentes. Este valor ser� el n�mero de ' +
    'desplazamientos a derechas que se le deber� aplicar a la mantisa sobre ' +
    'la que act�a. Y como se ha especificado en la etapa anterior, la mantisa ' +
    'que llega al desplazador es la mantisa correspondiente al n�mero con ' +
    'menor exponente.'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'This shifter gets the exponents'' difference from the exponent comparer ' +
    'in the previous stage. This value corresponds to the positions that the ' +
    'mantissa must be right shifted. As specified in last stage, the ' +
    'mantissa that arrives to the shifter is the one that belongs to the ' +
    'number with smallest exponent.'#13 +
    'Before: %s'#13'After:  %s');
    
  TXT_HPADD_HINT_ADD_SUB: TLangStr = (
    'El sumador/restador opera con las mantisas, bien sea sum�ndolas o rest�ndolas. ' +
    'En este caso, la operaci�n que realizar� ser� la siguiente:'#13 +
    '       %s'#13 +
    ' %s     %s'#13 +
    '_______________________________________________________________'#13 +
    '=   %s %s  (representado en Signo Y Magnitud)',

    'The adder/subtracter adds or subtracts the mantissa. In this case ' +
    'the performed operation is this:'#13 +
    '       %s'#13 +
    ' %s     %s'#13 +
    '_______________________________________________________________'#13 +
    '=   %s %s  (represented in Sign and Magnitude)');

  TXT_HPADD_HINT_EXP_INC_OV: TLangStr = (
    'El sumador/restador de la etapa anterior notifica que hay desbordamiento ' +
    'en su salida. El exponente del resultado debe incrementarse en 1 para que ' +
    'el resultado quede normalizado.'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'The adder/subtracter in the previous stage notifies an overflow in its ' +
    'output. The result''s exponent must be increased in 1, so that the ' +
    'result is normalized.'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_RIGHT_SHIFT_OV: TLangStr = (
    'El sumador/restador de la etapa anterior notifica que hay desbordamiento ' +
    'en su salida. La mantisa del resultado debe ser desplazada una posici�n ' +
    'a la derecha para que el resultado quede normalizado.'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'The adder/subtracter in the previous stage notifies an overflow in its ' +
    'output. The resulting mantissa must be right shifted one position, ' +
    'so that the result is normalized.'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_EXP_INC: TLangStr = (
    'El sumador/restador ha proporcionado un resultado que no provoca ' +
    'desbordamiento, por lo tanto, esta etapa no realizar� ninguna acci�n.'#13'%s',

    'The adder/subtracter has supplied a result which does not cause overflow, ' +
    'so this stage does not carry out any action.'#13'%s');

  TXT_HPADD_HINT_PRIO_ENC: TLangStr = (
    'Este elemento del circuito calcula la posici�n del primer bit ' +
    'significativo de la mantisa resultante, lo cual ser� necesario para ' +
    'posteriormente normalizar. Esta posici�n es pasada a la siguiente etapa ' +
    'en forma de n�mero binario de 6 bits:'#13'%s',

    'This element of the circuit calculates the position of the first ' +
    'significant bit in the resulting mantissa, which is necessary in ' +
    'order to normalize later. This value is sent to the next stage as ' +
    'a binary 6-bit number:'#13'%s');

  TXT_HPADD_HINT_EXP_DEC: TLangStr = (
    'En esta etapa, se decrementa el exponente tantas unidades como le ' +
    'especifique el valor obtenido de la etapa anterior. ' +
    'Con este decremento y el desplazamiento de la mantisa realizado en ' +
    'paralelo, el resultado quedar� normalizado. ' +
    'El exponente quedar� as�:'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'In this stage the exponent is decreased by the value obtained in the ' +
    'previous stage. With this decrement and the shifted mantissa, the ' +
    'result will be normalized. The exponent will look like this:'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_LEFT_SHIFT: TLangStr = (
    'Este elemento desplaza la mantisa hacia la izquierda hasta que su primer ' +
    'bit significativo quede en 2� posici�n. Adem�s le quitaremos un bit en ' +
    'la parte izquierda, ya que deja de sernos necesario (se a�adi� antes ' +
    'para capturar el desbordamiento en la suma. ' +
    'La mantisa quedar� as�:'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'This element left-shifts the mantissa until its first significant bit ' +
    'lies in the second position. Apart of this, the first bit is removed, ' +
    'because it isn''t necessary anymore (it was added before in order to ' +
    'capture an overflow in the addition. The mantissa will look like this:'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_EXP_INC2_YES: TLangStr = (
    'Se incrementa en una unidad el exponente, ya que el resultado del ' +
    'redondeo ha provocado un desbordamiento en la mantisa.'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'The exponent is increased by 1, because rounding has caused an overflow ' +
    'in the mantissa.'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_EXP_INC2_NO: TLangStr = (
    'No se realiza ninguna acci�n, ya que el redondeo se ha llevado a cabo ' +
    'sin desbordamiento.'#13'%s',

    'Any action is carried out, because there was no overflow when rounding.' +
    #13'%s');

  TXT_HPADD_HINT_ROUND_INC: TLangStr = (
    'Este elemento ajusta el resultado. %s' +
    'La mantisa quedar� as�:'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'This element adjusts the result. %s ' +
    'The mantissa will look like this:'#13 +
    'Before: %s'#13'After:  %s');

  TXT_HPADD_HINT_CONST: TLangStr = (
    'Este elemento genera una constante para el exponente del resultado en ' +
    'el caso en que �ste sea un n�mero no v�lido. ' +
    'Si se debe devolver Inf o NaN, se generar� una serie de 1s.'#13'%s',

    'This element generates a constant for the result''s exponent in case ' +
    'that it is a non-valid number. If Inf or NaN should be returned, a set of ' +
    '1s is generated.'#13'%s');

  TXT_HPADD_HINT_GEN_CONST: TLangStr = (
    'Este elemento genera constantes de error para la mantisa del resultado. ' +
    'Si se debe devolver Inf, generar�a una secuencia de 0s. ' +
    'Si debe devolver NaN, generar�a una secuencia de n�meros determinada, ' +
    'donde apareciara alg�n 1.'#13'%s',

    'This element generates error constants for the resulting mantissa. ' +
    'If it should return Inf, it generates a sequence of 0s. If the result ' +
    'must be NaN, this element generates a certain bit sequence where some ' +
    '1 occurs.'#13'%s');

  TXT_HPADD_HINT_SUCCESS: TLangStr = (
    '�xito.'#13'Resumen:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)',

    'Success'#13'Summary:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)');

  TXT_HPADD_INFO: TLangStr = (
    '  Esta ventana simula el circuito sumador/restador HP de n�meros en ' +
    'coma flotante IEEE. Este circuito est� integrado en un �nico chip ' +
    'VLSI segmentado, y cada una de sus funciones est� representada ' +
    'independientemente con una zona amarilla o azul en el esquema.'#13 +
    '  Para avanzar en la simulaci�n, pulsa el bot�n "Sig >>" y para ver el ' +
    'contenido de un elemento del circuito, desplaza el rat�n sobre �l. ' +
    'En la ventana emergente, se presenta una descripci�n de la etapa y su ' +
    'estado. Para ocultar/mostrar la ventana de descrici�n, pulsa el bot�n "Desc".',

    '  This window simulates the HP IEEE Floating Point Adder/subtracter. ' +
    'This circuit is integrated in a single segmented VLSI chip, ' +
    'and each of its functions is represented independently with a yellow or blue zone in the ' +
    'block diagram.'#13 +
    '  In order to advance the simulation, push the "Next >>" button. In ' +
    'order to see the contents of an element, move the mouse pointer ' +
    'over it. In the emergent window, a description of the current stage ' +
    'and its status is shown. In order to show/hide the description window, ' +
    'push the "Desc" button.');


  (*** Algoritmo multiplicador ***)

  TXT_ALGMULT_CAPTION: TLangStr = (
    'Algoritmo gen�rico del multiplicador/divisor en coma flotante',
    'Generic floating point multiplication/division algorithm');
  TXT_ALGMULT_LBL_INIT: TLangStr = (
    'Inicio', 'Start');
  TXT_ALGMULT_LBL_STEP1: TLangStr = (
    '1. Sumar los exponentes de ambos n�meros, restando el exceso para ' +
    'obtener el nuevo exponente',
    '1. Add both exponents and subtract the bias to obtain the new ' +
    'exponent');
  TXT_ALGMULT_LBL_STEP2: TLangStr = (
    '2. Multiplicar/dividir las mantisas',
    '2. Multiply/divide the mantissas');
  TXT_ALGMULT_LBL_STEP3: TLangStr = (
    '3. Normalizar el producto, si es preciso, desplazando a la derecha ' +
    'e incrementando el exponente',
    '3. Normalize the product if necessary, by right-shifting the mantissa ' +
    'and increasing the exponent');
  TXT_ALGMULT_LBL_OVERFLOW: TLangStr = (
    '�Desbordamiento superior o interior?',
    'Overflow or underflow?');
  TXT_ALGMULT_LBL_EXCEPTION: TLangStr = (
    'Excepci�n', 'Exception');
  TXT_ALGMULT_LBL_STEP4: TLangStr = (
    '4. Redondear la mantisa al n�mero de bits apropiado',
    '4. Round the mantissa to the appropriate bit number');
  TXT_ALGMULT_LBL_NORMALIZED: TLangStr = (
    '�Todav�a est� normalizado?',
    'Yet normalized?');
  TXT_ALGMULT_LBL_SIGN: TLangStr = (
    '5. Poner el signo del producto como positivo si los signos de los ' +
    'operandos originales eran iguales',
    '5. Set the product sign');
  TXT_ALGMULT_LBL_END: TLangStr = (
    'Fin', 'End');
  TXT_ALGMULT_STEP: array[0..LANGUAGE_NUM - 1, 0..10] of string = ((
    'Elige 2 n�meros, la acci�n a realizar y el m�todo de redondeo. ' +
    'Para m�s opciones acerca de la representaci�n de los n�meros a ' +
    'multiplicar/dividir, pulsa los botones "..." que se encuentran a la ' +
    'derecha de �stos. Por defecto se asumir� que la entrada se proporciona ' +
    'en formato decimal y que los n�meros se representar�n en IEEE de ' +
    'precisi�n simple (32-bit).',

    'Los n�meros con los que se operar� repesentados en Signo-Exponente-' +
    'Mantisa son:'#13 +
    '%s  %s  %s  (=%s)'#13'%s  %s  %s  (=%s)',

    'Se suman los exponentes si se eligi� una multiplicaci�n o se restan ' +
    'en caso contrario. Adem�s se desnormalizan las mantisas. Quedar�n as�:'#13 +
    '%s  %s  %s'#13'%s  %s  %s'#13 +
    'Operaci�n con exponentes: %s',

    'Multiplica/divide las mantisas.'#13 +
    '%s'#13'%s'#13 +
    '_______________________________________________________________'#13'%s',

    'Ahora se normalizar� el resultado.' +
    'El primer bit significativo (bit a "1") debe quedar en segundo lugar ' +
    'empezando por la izq. El exponente se deber� modificar seg�n el n�mero ' +
    'de desplazamientos realizados en la mantisa.'#13 +
    'Antes (Exp-M):  %s %s'#13' Normalizando:  %s %s',

    'Se comprueba si ha habido desbordamiento en la normalizaci�n o bien ' +
    'en la suma de exponentes. En este caso %s ocurrir�.',

    'Ha ocurrido una excepci�n de desbordamiento. El resultado de la ' +
    'operaci�n seleccionada no es representable en IEEE con la precisi�n ' +
    'especificada.',

    'La �ltima operaci�n es ajustar el resultado obtenido. %s'#13 +
    '%s'#13'%s',

    'El redondeo puede haber provocado una desnormalizaci�n de la mantisa, ' +
    'lo que deber�a llevarnos tres pasos hacia atr�s en el algoritmo. ' +
    'En este caso, %s se da esa situaci�n.',

    'Se calcula el signo del resultado. ' +
    'En el caso de la multiplicaci�n/divisi�n, el resultado ser� positivo ' +
    'si ambos operandos tienen el mismo signo, y negativo en caso contrario. ' +
    'La funci�n que lo calcula es Sr = Sa XOR Sb'#13 +
    '%s XOR %s = %s',

    'Por �ltimo, eliminaremos lo que ser� el bit impl�cito en la mantisa. ' +
    'El resumen de la operaci�n ser�a el siguiente:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)'
  ) , (
    'Choose two numbers, the action to carry out and the rounding method. ' +
    'For more options about numbers'' representation, push the "..." buttons. ' +
    'By default an input in decimal format and a single precision number ' +
    '(32-bit) are assumed.',

    'The numbers which the circuit will operate with represented in Sign-' +
    'Exponent-Mantissa are:'#13 +
    '%s  %s  %s  (=%s)'#13'%s  %s  %s  (=%s)',

    'The exponents are added if a multiplication was chosen or subtracted ' +
    'otherwise. Besides, the mantissas are denormalized. They will look ' +
    'like this:'#13 +
    '%s  %s  %s'#13'%s  %s  %s'#13 +
    'Exponent operation: %s',

    'Multiply/divide the mantissas'#13 +
    '%s'#13'%s'#13 +
    '_______________________________________________________________'#13'%s',

    'The result will be now normalized. The first significant bit (bit "1") must ' +
    'be in the second left-position. The exponent must be modified depending ' +
    'on the number of positions the mantissa was shifted.'#13 +
    'Before (Exp-M):  %s %s'#13'   Normalizing:  %s %s',

    'An overflow during the normalization or during the exponents'' operation ' +
    'is checked. In this case, this situation %s occur.',

    'There was an overflow exception. The operation result is not representable ' +
    'in IEEE with the specified precision.',

    'The last step is to adjust the obtained result. %s'#13 +
    '%s'#13'%s',

    'The result adjustment can have caused a mantissa denormalization, ' +
    'which should bring the algorithm three steps backward. In this case, ' +
    'this situation %s happen.',

    'The result''s sign is calculated. In the case of the multiplication/' +
    'division, this sign will be positive if both operands have the same ' +
    'sign, and negative otherwise. The function for the result''s sign is ' +
    'Sr = Sa XOR Sb'#13 +
    '%s XOR %s = %s',

    'Finally, the mantissa implicit bit is removed. The operation ' +
    'summary is:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)'
  ));


  (*** Multiplicador HP ***)

  TXT_HPMULT_CAPTION: TLangStr = (
    'Circuito Multiplicador/Divisor Hewlett Packard',
    'Hewlett Packard Multiplier/Divider Circuit');
  TXT_HPMULT_CBT_NEXT: TLangStr = (
    'Sig', 'Next');
  TXT_HPMULT_CBT_END: TLangStr = (
    'Fin', 'End');
  TXT_HPMULT_CBT_INFO: TLangStr = (
    'Info', 'Info');
  TXT_HPMULT_CBT_DESC: TLangStr = (
    'Desc', 'Desc');
  TXT_HPMULT_LBL_EXP: TLangStr = (
    'Exponente', 'Exponent');
  TXT_HPMULT_LBL_SIGNIF: TLangStr = (
    'Mantisa', 'Mantissa');
  TXT_HPMULT_LBL_FORMAT: TLangStr = (
    'Formato', 'Format');
  TXT_HPMULT_LBL_ADD_SUB: TLangStr = (
    'Sumador/restador', 'Adder/subtracter');
  TXT_HPMULT_LBL_RECODE: TLangStr = (
    'Recodificar', 'Recode');
  TXT_HPMULT_LBL_MULT: TLangStr = (
    'Multiplicador de 64 bits CSA', 'CSA 64-bit Multiplier');
  TXT_HPMULT_LBL_SIGN_CORR: TLangStr = (
    'Correcci�n de signo', 'Sign correction');
  TXT_HPMULT_LBL_CPA: TLangStr = (
    'CPA', 'CPA');
  TXT_HPMULT_LBL_INC: TLangStr = (
    'Incremento', 'Increment');
  TXT_HPMULT_LBL_NORMALIZE: TLangStr = (
    'Normalizar', 'Normalize');
  TXT_HPMULT_LBL_ROUND: TLangStr = (
    'Redondeo (64 bits)', 'Round (64-bit)');
  TXT_HPMULT_LBL_CONST: TLangStr = (
    'Constante', 'Constant');
  TXT_HPMULT_LBL_GEN_CONST: TLangStr = (
    'Generar constante', 'Generate constant');
  TXT_HPMULT_LBL_STATUS: TLangStr = (
    'Estado', 'Status');
  TXT_HPMULT_LBL_RES_EXP: TLangStr = (
    'Exponente resultante', 'Resulting exponent');
  TXT_HPMULT_LBL_RES_SIGNIF: TLangStr = (
    'Mantisa resultante', 'Resulting mantissa');

  TXT_HPMULT_HINT_FMT_SIGNIF: TLangStr = (
    'Se le da formato a la mantisa %s, a�adi�ndole el bit impl�cito.'#13'%s',

    'The mantissa %s is formated by adding the implicit bit.'#13'%s');

  TXT_HPMULT_HINT_ADD_EXP: TLangStr = (
    'Se suman los exponentes en el caso en que se haya seleccionado la operaci�n ' +
    'de multiplicaci�n y se restan en caso contrario.'#13'%s',

    'Both exponents are added in case that a multiplication was chosen. They ' +
    'are subtracted otherwise.'#13'%s');

  TXT_HPMULT_HINT_RECODE: TLangStr = (
    'Se recodifica la mantisa, a�adiendo tantos ceros a su izquierda como sea ' +
    'necesario, de forma que el multiplicador pueda calcular directamente los ' +
    'productos intermedios.'#13'%s',

    'The mantissa is recoded by adding as many bits as necessary, so the ' +
    'mantissas multiplier can calculate directly the intermediate products.' +
    #13'%s');

  TXT_HPMULT_HINT_MULT_SIGNIF: TLangStr = (
    'Este multiplicador/divisor opera con las mantisas bien multiplic�ndolas o ' +
    'dividi�ndolas seg�n la operaci�n seleccionada. Proporciona como salida un ' +
    'vector de bits de suma y otro vector de bits de acarreo.'#13 +
    '%s'#13'%s'#13 +
    '_______________________________________________________________'#13'%s',

    'This multiplier/divider operates with both mantissas either by ' +
    'multiplying or dividing them, depending on the by the user selected ' +
    'operation. In its output, this element provides an addition-bit-array ' +
    'and a carry-bit-array.'#13 +
    '%s'#13'%s'#13 +
    '_______________________________________________________________'#13'%s');

  TXT_HPMULT_HINT_CARRY_ASSIM: TLangStr = (
    'En esta etapa, se asimila el resultado proporcionado por el CSA, sumando ' +
    'el vector de suma con el de acarreo, desplazando este �ltimo convenientemente. ' +
    'A partir de ahora, ya se tiene el producto de las mantisas en un formato ' +
    'correcto.'#13'%s',

    'In this stage, the result provided by the CSA is assimilated by adding ' +
    'the addition-bit-array and the carry-bit-array. The carry-bit-array ' +
    'must be left-shifted by one position before being added. From now on, ' +
    'we have got the mantissas product in the right format.'#13'%s');

  TXT_HPMULT_HINT_NORM_EXP: TLangStr = (
    'Se modifica el exponente seg�n los desplazamientos realizados en la mantisa ' +
    'despu�s de ser normalizada. Un desplazamiento a derecha de �sta, supondr� ' +
    'un incremento del exponente y al rev�s.'#13 +
    'Antes:    %s'#13'Despu�s:  %s',

    'The exponent is modified depending on the number of positions the ' +
    'mantissa was shifted when being normalized. A right-shifting for ' +
    'the mantissa will imply an increment for the exponent.'#13 +
    'Before:  %s'#13'After:   %s');

  TXT_HPMULT_HINT_NORM_SIGNIF: TLangStr = (
    'Se normaliza la mantisa de forma que el bit m�s significativo quede en ' +
    'segunda posici�n comenzando por la izquierda. Adem�s, se truncar� al ' +
    'n�mero de bits apropiado.'#13 +
    'Antes:    %s'#13'Despu�s:  %s',

    'The mantissa is normalized so that the most significant bit lies in ' +
    'the second left position. It is also truncated to the right number of ' +
    'bits.'#13 +
    'Before:  %s'#13'After:   %s');

  TXT_HPMULT_HINT_ROUNDEXP_YES: TLangStr = (
    'Se incrementa en una unidad el exponente, ya que el resultado del redondeo ' +
    'ha provocado un desbordamiento en la mantisa.'#13 +
    'Antes:   %s'#13'Despu�s: %s',

    'The exponent is increased in one, because the rounding element has ' +
    'caused an overflow in the mantissa.'#13 +
    'Before:  %s'#13'After:   %s');

  TXT_HPMULT_HINT_ROUNDEXP_NO: TLangStr = (
    'No se realiza ninguna acci�n, ya que el redondeo se ha llevado a cabo sin ' +
    'desbordamiento.'#13'%s',

    'No action is carried out, because the rounding element has performed ' +
    'its job without causing an overflow.'#13'%s');

  TXT_HPMULT_HINT_ROUND_SIGNIF: TLangStr = (
    'Este elemento ajusta el resultado. %s' +
    'La mantisa quedar� as�:'#13'Antes:   %s'#13'Despu�s: %s',

    'This element adjusts the result. %s The mantissa will look like this:'#13 +
    'Before:  %s'#13'After:   %s');

  TXT_HPMULT_HINT_CONST: TLangStr = (
    'Este elemento genera una constante para el exponente del resultado en el ' +
    'caso en que �ste sea un n�mero no v�lido. Si se debe devolver Inf o NaN, ' +
    'se generar� una serie de 1s.'#13'%s',

    'This element generates a constant for the result''s exponent in case that ' +
    'it is not a valid number. If it should return Inf or NaN, it would ' +
    'generate a sequence of 1s.'#13'%s');

  TXT_HPMULT_HINT_CONST_GEN: TLangStr = (
    'Este elemento genera constantes de error para la mantisa del resultado. ' +
    'Si se debe devolver Inf, generar�a una secuencia de 0s. Si debe devolver ' +
    'NaN, generar�a una secuencia de n�meros determinada, donde apareciara ' +
    'alg�n 1.'#13'%s',

    'This element generates erro constants for the resulting mantissa. ' +
    'If it should return Inf, it would return a sequence of 0s. If it should ' +
    'return NaN, it would generate a certain sequence of bits, where some 1 ' +
    'would occur.'#13'%s');

  TXT_HPMULT_HINT_SUCCESS: TLangStr = (
    '�xito.'#13'Resumen:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)',

    'Success.'#13'Summary:'#13 +
    '  %s %s %s  (=%s)'#13 +
    '%s %s %s %s  (=%s)'#13 +
    '_______________________________________________________________'#13 +
    '  %s %s %s  (=%s)');

  TXT_HPMULT_INFO: TLangStr = (
    '  Esta ventana simula el circuito multiplicador HP de n�meros en coma flotante ' +
    'IEEE. Este circuito est� integrado en un �nico chip con tecnolog�a CMOS ' +
    'segmentado, y cada una de sus funciones est� representada independientemente con una zona ' +
    'amarilla o verde en el esquema.'#13 +
    '  Para avanzar en la simulaci�n, pulsa el bot�n "Sig >>" y para ver el ' +
    'contenido de un elemento del circuito, desplaza el rat�n sobre �l. ' +
    'En la ventana emergente, se presenta una descripci�n de la etapa y su estado. ' +
    'Para ocultar/mostrar la ventana de descrici�n, pulsa el bot�n "Desc".',

    '  This window simulates the HP Floating Point IEEE Multiplier Circuit. ' +
    'This circuit is integrated in a single segmented CMOS chip and each of its ' +
    'functions is represented independently in a yellow or green zone in the ' +
    'block diagram.'#13 +
    '  In order to advance in the simulation, push the "Next >>" button, and ' +
    'move the mouse pointer over a certain element on the diagram in order ' +
    'to see its contents. In the emergent window, a description of the current ' +
    'stage and its status is presented. In order to show/hide this description ' +
    'window, push the "Desc" button.');


implementation

begin
  Lng := 1;
end.
