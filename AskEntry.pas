unit AskEntry;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Numbers, Functions, Buttons,
  RafMsg, Text, AskNumber;

type
  TfrmAskEntry = class(TForm)
    chkAction: TRadioGroup;
    grpInput: TGroupBox;
    fldNum1: TEdit;
    fldNum2: TEdit;
    cbtChoose1: TButton;
    cbtChoose2: TButton;
    chkRound: TRadioGroup;
    cbtOk: TBitBtn;
    cbtCancel: TBitBtn;
    procedure cbtChooseClick(Sender: TObject);
    procedure cbtOkClick(Sender: TObject);
  end;

  (* Tipo que empaqueta toda la informaci�n devuelta
   * por la funci�n AskEntry. *)
  TAskEntry = record
    Num           : array[0..1] of TFloatingPointNumber;
    RoundMethod   : TRoundMethod;
    OpUser        : Integer;
  end;

var
  frmAskEntry: TfrmAskEntry;

(* Funci�n que abre la ventana para introducir una entrada de una operaci�n.
 * Devuelve True si se presion� en Aceptar, false si se cancel�.
 * En OpKind se pondr� si se necesita Suma/Resta (0), o mult/div (1). *)
function AskUserEntry(var aEntryStruct: TAskEntry; OpKind: Integer): Boolean;

implementation
{$R *.dfm}

const
  TXT_MORE = '(...)';

var
  EntryStruct: TAskEntry;


(*** Funci�n p�blica ***)

function AskUserEntry;
var
  chkItemIndex, i: Integer;
begin
  with frmAskEntry do begin

    //Textos
    Caption                     := TXT_ASKENTRY_CAPTION[Lng];
    cbtOK.Caption               := TXT_BUT_OK[Lng];
    cbtCancel.Caption           := TXT_BUT_CANCEL[Lng];
    chkRound.Caption            := TXT_ASKENTRY_RES_ADJUST[Lng];
    for i := 0 to 3 do
      chkRound.Items.Strings[i] := TXT_ASKENTRY_ROUND[Lng, i];
    chkAction.Caption           := TXT_ASKENTRY_ACTION[Lng];
    grpInput.Caption            := TXT_ASKENTRY_INPUT[Lng];

    //Acci�n seg�n par�metro
    chkAction.Items.Clear;
    chkItemIndex := chkAction.ItemIndex;
    if chkItemIndex < 0 then
      chkItemIndex := 0;
    for i := 0 to 1 do
      chkAction.Items.Add(TXT_ASKENTRY_OP_NAME[Lng, OpKind, i]);
    chkAction.ItemIndex := chkItemIndex;

    //Inicializa campos
    if fldNum1.Text = TXT_MORE then
      fldNum1.Text := '';
    if fldNum2.Text = TXT_MORE then
      fldNum2.Text := '';

    //Inicializa EntryStruct
    EntryStruct.Num[0] := nil;
    EntryStruct.Num[1] := nil;

    //Muestra ventana
    Result := ShowModal = mrOK;

    //Guarda resultados
    aEntryStruct := EntryStruct;

  end;
end;



(*** Funciones de la ventana ***)

(* Pregunta por un n�mero y lo asigna si es v�lido, poniendo en el campo
 * asociado el texto "..." *)
procedure AskFloatingPointNumberInput(var Num: TFloatingPointNumber;
  var fldNum: TEdit);
var
  AskNum: TFloatingPointNumber;
begin
  AskNum := AskFPNumber(False);
  if AskNum <> nil then begin
    //Se comprueba que no sea NaN ni Inf
    if AskNum.Kind in [fkNaN, fkInf] then begin
      ShowError(TXT_ASKENTRY_ERR_NAN_OR_INF[Lng]);
      AskNum.Free;
      Exit;
    end;

    //Se establece el nuevo n�mero
    if Num <> nil then Num.Free;
    Num := AskNum;
    fldNum.Text := TXT_MORE;
  end;
end;

procedure TfrmAskEntry.cbtChooseClick(Sender: TObject);
begin
  //Qu� n�mero hay que elegir?
  if Sender = cbtChoose1 then
    AskFloatingPointNumberInput(EntryStruct.Num[0], fldNum1)
  else
    AskFloatingPointNumberInput(EntryStruct.Num[1], fldNum2)
end;

function CreateFloatingPointNumber(var Num: TFloatingPointNumber;
  var fld: TEdit): Boolean;
begin
  Result := True;

  (* Si el n�mero ha sido creado ya presionando el bot�n '...' y adem�s
   * no se ha modificado el texto del campo "(...)", no habr� que crear
   * ning�n n�mero. *)
  if (Num <> nil) and (fld.Text = TXT_MORE) then
    Exit;

  (* Si no, se crea el n�mero a partir del texto del campo como un n�mero
   * en decimal con simple precisi�n. *)
  if Num <> nil then
    Num.Free;
    Num := TFloatingPointNumber.Create(fiDec, foSingle, 0, 0, fld.Text);

  (* Si no es v�lida la entrada, se muestra un error *)
  if not Num.ValidNumber then begin
    ShowError(TXT_ASKNUM_ERR_DECFORMAT[Lng]);
    Num.Free;
    Num := nil;
    Result := False;
    Exit;
  end;
end;

procedure TfrmAskEntry.cbtOkClick(Sender: TObject);
begin
  (* Intenta crear los n�meros. Si no lo consigue, fuera. *)
  if (not CreateFloatingPointNumber(EntryStruct.Num[0], fldNum1)) or
  (not CreateFloatingPointNumber(EntryStruct.Num[1], fldNum2)) then
    Exit;

  (* Si son de precisi�n diferente, error *)
  if EntryStruct.Num[0].Format <> EntryStruct.Num[1].Format then begin
    ShowError(TXT_ASKENTRY_ERR_PRECISION[Lng]);
    Exit;
  end;

  (* Establece m�todo de redondeo y operaci�n *)
  EntryStruct.RoundMethod := TRoundMethod(chkRound.ItemIndex);
  EntryStruct.OpUser := chkAction.ItemIndex;

  (* Una vez llegado aqu�, todo correcto. Sale con �xito. *)
  ModalResult := mrOK;
end;

end.
