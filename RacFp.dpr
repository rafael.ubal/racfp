program RacFp;

uses
  Forms,
  Main in 'Main.pas' {frmMain},
  Functions in 'Functions.pas',
  Numbers in 'Numbers.pas',
  NumberInformation in 'NumberInformation.pas' {frmNumberInfo},
  AdderAlgorithm in 'AdderAlgorithm.pas',
  MultiplierAlgorithm in 'MultiplierAlgorithm.pas' {frmMultiplierAlgorithm},
  Auxiliar in 'Auxiliar.pas',
  Tables in 'Tables.pas' {frmTables},
  HPAdder in 'HPAdder.pas' {frmHPAdder},
  AskEntry in 'AskEntry.pas' {frmAskEntry},
  HPMultiplier in 'HPMultiplier.pas' {frmHPMultiplier},
  Text in 'Text.pas',
  Language in 'Language.pas' {frmLanguage},
  About in 'About.pas' {frmAbout},
  Functions2 in 'Functions2.pas',
  AskNumber in 'AskNumber.pas' {frmAskNumber};

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'Simulador coma flotante';
  Application.CreateForm(TfrmMain, frmMain);
  Application.CreateForm(TfrmNumberInfo, frmNumberInfo);
  Application.CreateForm(TfrmAdderAlgorithm, frmAdderAlgorithm);
  Application.CreateForm(TfrmMultiplierAlgorithm, frmMultiplierAlgorithm);
  Application.CreateForm(TfrmTables, frmTables);
  Application.CreateForm(TfrmHPAdder, frmHPAdder);
  Application.CreateForm(TfrmAskEntry, frmAskEntry);
  Application.CreateForm(TfrmHPMultiplier, frmHPMultiplier);
  Application.CreateForm(TfrmLanguage, frmLanguage);
  Application.CreateForm(TfrmAbout, frmAbout);
  Application.CreateForm(TfrmAskNumber, frmAskNumber);
  Application.Run;
end.
