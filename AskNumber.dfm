object frmAskNumber: TfrmAskNumber
  Left = 281
  Top = 240
  HorzScrollBar.Visible = False
  VertScrollBar.Visible = False
  BorderStyle = bsDialog
  Caption = 'Introduzca el n'#250'mero'
  ClientHeight = 194
  ClientWidth = 519
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lblInput: TLabel
    Left = 256
    Top = 16
    Width = 40
    Height = 13
    Caption = 'N'#250'mero:'
    FocusControl = fldInput
  end
  object grpOutputFmt: TGroupBox
    Left = 16
    Top = 16
    Width = 233
    Height = 129
    Caption = 'Formato de salida'
    TabOrder = 0
    object rdSingle: TRadioButton
      Left = 8
      Top = 16
      Width = 193
      Height = 17
      Caption = 'IEEE de simple precisi'#243'n (32 bits)'
      Checked = True
      TabOrder = 0
      TabStop = True
      OnClick = rdGroupClick
    end
    object rdDouble: TRadioButton
      Left = 8
      Top = 32
      Width = 185
      Height = 17
      Caption = 'IEEE de doble precisi'#243'n (64 bits)'
      TabOrder = 1
      OnClick = rdGroupClick
    end
    object rdCustom: TRadioButton
      Left = 8
      Top = 48
      Width = 161
      Height = 17
      Caption = 'Formato personalizado'
      TabOrder = 2
      OnClick = rdGroupClick
    end
    object fldExp: TEdit
      Left = 96
      Top = 72
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 3
      Text = '8'
    end
    object udExp: TUpDown
      Left = 129
      Top = 72
      Width = 15
      Height = 21
      Associate = fldExp
      Enabled = False
      Min = 1
      Max = 11
      Position = 8
      TabOrder = 4
      Wrap = False
    end
    object fldMantissa: TEdit
      Left = 96
      Top = 96
      Width = 33
      Height = 21
      Enabled = False
      TabOrder = 5
      Text = '23'
    end
    object udMantissa: TUpDown
      Left = 129
      Top = 96
      Width = 15
      Height = 21
      Associate = fldMantissa
      Enabled = False
      Min = 1
      Max = 52
      Position = 23
      TabOrder = 6
      Wrap = False
    end
    object lblMantissa: TStaticText
      Left = 24
      Top = 99
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Mantisa:'
      Enabled = False
      TabOrder = 7
    end
    object lblExp: TStaticText
      Left = 24
      Top = 75
      Width = 65
      Height = 17
      Alignment = taRightJustify
      AutoSize = False
      Caption = 'Exponente:'
      Enabled = False
      TabOrder = 8
    end
    object lblBits1: TStaticText
      Left = 148
      Top = 75
      Width = 41
      Height = 17
      AutoSize = False
      Caption = 'bits'
      Enabled = False
      TabOrder = 9
    end
    object lblBits2: TStaticText
      Left = 148
      Top = 99
      Width = 41
      Height = 17
      AutoSize = False
      Caption = 'bits'
      Enabled = False
      TabOrder = 10
    end
  end
  object grpInputFmt: TGroupBox
    Left = 256
    Top = 72
    Width = 249
    Height = 73
    Caption = 'Formato de entrada'
    TabOrder = 1
    object rdBin: TRadioButton
      Left = 8
      Top = 16
      Width = 121
      Height = 17
      Caption = 'Binario'
      TabOrder = 0
    end
    object rdDec: TRadioButton
      Left = 8
      Top = 32
      Width = 129
      Height = 17
      Caption = 'Decimal'
      Checked = True
      TabOrder = 1
      TabStop = True
    end
    object rdHex: TRadioButton
      Left = 8
      Top = 48
      Width = 121
      Height = 17
      Caption = 'Hexadecimal'
      TabOrder = 2
    end
  end
  object fldInput: TEdit
    Left = 256
    Top = 32
    Width = 177
    Height = 21
    TabOrder = 2
  end
  object cbtOK: TBitBtn
    Left = 440
    Top = 31
    Width = 49
    Height = 25
    Caption = 'OK'
    Default = True
    ModalResult = 1
    TabOrder = 3
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object cbtClose: TBitBtn
    Left = 208
    Top = 152
    Width = 121
    Height = 33
    Cancel = True
    Caption = '&Cancelar'
    ModalResult = 2
    TabOrder = 4
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      333333333333333333333333000033338833333333333333333F333333333333
      0000333911833333983333333388F333333F3333000033391118333911833333
      38F38F333F88F33300003339111183911118333338F338F3F8338F3300003333
      911118111118333338F3338F833338F3000033333911111111833333338F3338
      3333F8330000333333911111183333333338F333333F83330000333333311111
      8333333333338F3333383333000033333339111183333333333338F333833333
      00003333339111118333333333333833338F3333000033333911181118333333
      33338333338F333300003333911183911183333333383338F338F33300003333
      9118333911183333338F33838F338F33000033333913333391113333338FF833
      38F338F300003333333333333919333333388333338FFF830000333333333333
      3333333333333333333888330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
end
