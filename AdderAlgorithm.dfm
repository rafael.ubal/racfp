object frmAdderAlgorithm: TfrmAdderAlgorithm
  Left = 153
  Top = 41
  BorderStyle = bsDialog
  Caption = 'Algoritmo gen'#233'rico del sumador/restador en coma flotante'
  ClientHeight = 494
  ClientWidth = 599
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Shape1: TShape
    Left = 163
    Top = 16
    Width = 3
    Height = 345
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Shape2: TShape
    Left = 328
    Top = 208
    Width = 3
    Height = 25
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Shape3: TShape
    Left = 200
    Top = 208
    Width = 129
    Height = 3
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Shape4: TShape
    Left = 8
    Top = 152
    Width = 100
    Height = 3
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Shape5: TShape
    Left = 8
    Top = 152
    Width = 3
    Height = 153
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Shape6: TShape
    Left = 8
    Top = 304
    Width = 129
    Height = 3
    Brush.Color = clRed
    Pen.Color = clRed
  end
  object Image1: TImage
    Left = 5
    Top = 340
    Width = 32
    Height = 32
    Picture.Data = {
      07544269746D617076020000424D760200000000000076000000280000002000
      000020000000010004000000000000020000C40E0000C40E0000100000000000
      000000000000000080000080000000808000800000008000800080800000C0C0
      C000808080000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFF
      FF00000000000000000000088000000000000000000000000000008880000000
      00000000000000000000000880000000000000000000000000000F0880000000
      00000000000000000000FF08800000000000000000000000080FFF0880000000
      0000000000000088880FFF08888800000000000000008880007FFF0888888800
      0000000000080007FFFFFF70008888800000000000007FFFFFFFFFFFF7008888
      00000000007FFFFFFFFFFFFFFFF70888800000000FFFFFFFFFFFFFFFFFFFF088
      88000000FFFFFFFFFFFFFFFFFFFFFF088880008FFFFFFFFCCCCCCCCCFFFFFFF0
      8880087FFFFFFFFFFCCCCCFFFFFFFFF7088808FFFFFFFFFFFCCCCCFFFFFFFFFF
      088887FFFFFFFFFFFCCCCCFFFFFFFFFF70888FFFFFFFFFFFFCCCCCFFFFFFFFFF
      F0888FFFFFFFFFFFFCCCCCFFFFFFFFFFF0888FFFFFFFFFFFFCCCCCFFFFFFFFFF
      F0888FFFFFFFFFFFFCCCCCFFFFFFFFFFF0888FFFFFFFFFFCCCCCCCFFFFFFFFFF
      F08087FFFFFFFFFFFFFFFFFFFFFFFFFF708008FFFFFFFFFFFFFFFFFFFFFFFFFF
      0800087FFFFFFFFF7CCCC7FFFFFFFFF70000008FFFFFFFFFCCCCCCFFFFFFFFF0
      00000008FFFFFFFFCCCCCCFFFFFFFF00000000008FFFFFFF7CCCC7FFFFFFF000
      00000000087FFFFFFFFFFFFFFFF780000000000000887FFFFFFFFFFFF7880000
      0000000000008887FFFFFF788800000000000000000000088888888000000000
      0000}
    Transparent = True
  end
  object lblInit: TStaticText
    Left = 96
    Top = 16
    Width = 137
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = 'Inicio'
    ParentBiDiMode = False
    TabOrder = 0
  end
  object lblStep1: TStaticText
    Left = 40
    Top = 48
    Width = 249
    Height = 49
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = 
      '1. Comparar exponentes de los dos n'#250'meros. Desplazar el n'#250'mero m' +
      'enor a la derecha hasta que su exponente coincida con el mayor'
    ParentBiDiMode = False
    TabOrder = 1
  end
  object lblStep2: TStaticText
    Left = 40
    Top = 104
    Width = 249
    Height = 25
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = '2. Sumar/restar las mantisas'
    ParentBiDiMode = False
    TabOrder = 2
  end
  object lblStep3: TStaticText
    Left = 40
    Top = 136
    Width = 249
    Height = 49
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = 
      '3. Normalizar la suma, ya sea desplazando a la derecha increment' +
      'ando el exponente o a la izquierda decrement'#225'ndolo.'
    ParentBiDiMode = False
    TabOrder = 5
  end
  object lblOverflow: TStaticText
    Left = 112
    Top = 192
    Width = 105
    Height = 33
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = #191'Desbordamiento superior?'
    ParentBiDiMode = False
    TabOrder = 6
  end
  object lblStep4: TStaticText
    Left = 40
    Top = 248
    Width = 249
    Height = 33
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = '4. Redondear la mantisa al n'#250'mero de bits apropiado'
    ParentBiDiMode = False
    TabOrder = 7
  end
  object lblNormalized: TStaticText
    Left = 112
    Top = 288
    Width = 105
    Height = 33
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = #191'Todav'#237'a est'#225' normalizado?'
    ParentBiDiMode = False
    TabOrder = 8
  end
  object lblEnd: TStaticText
    Left = 96
    Top = 344
    Width = 137
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = 'Fin'
    ParentBiDiMode = False
    TabOrder = 9
  end
  object lblException: TStaticText
    Left = 296
    Top = 224
    Width = 65
    Height = 20
    Alignment = taCenter
    AutoSize = False
    BevelKind = bkFlat
    BevelOuter = bvRaised
    BiDiMode = bdRightToLeftNoAlign
    BorderStyle = sbsSingle
    Caption = 'Excepci'#243'n'
    ParentBiDiMode = False
    TabOrder = 10
  end
  object lblYes1: TStaticText
    Left = 288
    Top = 203
    Width = 15
    Height = 17
    Caption = 'S'#237
    TabOrder = 11
  end
  object lblNo1: TStaticText
    Left = 168
    Top = 227
    Width = 18
    Height = 17
    Caption = 'No'
    TabOrder = 12
  end
  object lblYes2: TStaticText
    Left = 168
    Top = 323
    Width = 15
    Height = 17
    Caption = 'S'#237
    TabOrder = 13
  end
  object lblNo2: TStaticText
    Left = 80
    Top = 299
    Width = 18
    Height = 17
    Caption = 'No'
    TabOrder = 14
  end
  object cbtNext: TBitBtn
    Left = 400
    Top = 328
    Width = 73
    Height = 33
    Caption = '&Siguiente'
    Default = True
    TabOrder = 3
    OnClick = cbtNextClick
  end
  object lblDesc: TStaticText
    Left = 8
    Top = 372
    Width = 584
    Height = 117
    AutoSize = False
    BorderStyle = sbsSunken
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 15
  end
  object cbtEnd: TBitBtn
    Left = 472
    Top = 328
    Width = 73
    Height = 33
    Caption = '&Finalizar'
    TabOrder = 4
    OnClick = cbtEndClick
  end
end
