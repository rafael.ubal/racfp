unit About;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Text, Buttons;

type
  TfrmAbout = class(TForm)
    Panel1: TPanel;
    Image1: TImage;
    Image2: TImage;
    lblName: TStaticText;
    lblSubtitle: TStaticText;
    lblItems: TStaticText;
    lblDesc: TStaticText;
    Image3: TImage;
    cbtOK: TBitBtn;
    lblProject: TStaticText;
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAbout: TfrmAbout;

implementation

{$R *.dfm}

procedure TfrmAbout.FormShow(Sender: TObject);
begin
  Caption             := TXT_ABOUT_CAPTION[Lng];
  lblProject.Caption  := TXT_ABOUT_TITLE[Lng];
  lblSubTitle.Caption := TXT_ABOUT_SUBTITLE[Lng];
  lblItems.Caption    := TXT_ABOUT_ITEMS[Lng];
  lblDesc.Caption     := TXT_ABOUT_DESC[Lng];
  cbtOK.Caption       := TXT_BUT_OK[Lng];
end;

end.
