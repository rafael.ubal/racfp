unit NumberInformation;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, Numbers, Text, AskNumber;

type
  TfrmNumberInfo = class(TForm)
    grpResult: TGroupBox;
    lblBin: TLabel;
    lblHex: TLabel;
    lblDec: TLabel;
    txtSign: TStaticText;
    txtExp: TStaticText;
    txtMantissa: TStaticText;
    txtHex: TStaticText;
    txtDec: TStaticText;
    cbtClose: TBitBtn;
    grpDetails: TGroupBox;
    lblPrecision: TLabel;
    lblKind: TLabel;
    lblDescription: TLabel;
    txtPrecision: TStaticText;
    txtKind: TStaticText;
    txtDesc: TStaticText;
    lblStandard: TLabel;
    txtStandard: TStaticText;
    cbtNew: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure cbtNewClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNumberInfo: TfrmNumberInfo;

implementation
{$R *.dfm}

procedure TfrmNumberInfo.FormShow(Sender: TObject);
begin
  (* Nombres seg�n idioma *)
  Caption := TXT_NUMINFO_CAPTION[Lng];
  cbtNew.Caption := TXT_NUMINFO_NEW[Lng];
  cbtClose.Caption := TXT_BUT_CLOSE[Lng];
  grpResult.Caption := TXT_NUMINFO_GRP_RESULT[Lng];
  grpDetails.Caption := TXT_NUMINFO_GRP_DETAILS[Lng];
  lblBin.Caption := TXT_NUMINFO_LBL_BINARY[Lng];
  lblHex.Caption := TXT_NUMINFO_LBL_HEXADECIMAL[Lng];
  lblDec.Caption := TXT_NUMINFO_LBL_DECIMAL[Lng];
  lblPrecision.Caption := TXT_NUMINFO_LBL_PRECISION[Lng];
  lblKind.Caption := TXT_NUMINFO_LBL_TYPE[Lng];
  lblDescription.Caption := TXT_NUMINFO_LBL_DESCRIPTION[Lng];
  lblStandard.Caption := TXT_NUMINFO_LBL_STANDARD[Lng];

  (* Abre cuadro de di�logo de insertar n�mero *)
  cbtNew.SetFocus;
  cbtNew.Click;
end;

procedure TfrmNumberInfo.cbtNewClick(Sender: TObject);
var
  FPN: TFloatingPointNumber;
begin
  FPN := AskFPNumber(True);
  if (FPN <> nil) and (FPN.ValidNumber) then begin
    (* Campos generales *)
    txtSign.Caption := FPN.Sign;
    txtExp.Caption := FPN.Exponent;
    txtMantissa.Caption := FPN.Mantissa;
    txtDec.Caption := FPN.DecValue;
    txtHex.Caption := FPN.HexValue;

    (* Est�ndar *)
    if FPN.Format = foSingle then txtStandard.Caption := TXT_NUMINFO_SINGLE[Lng]
    else if FPN.Format = foDouble then txtStandard.Caption := TXT_NUMINFO_DOUBLE[Lng]
    else txtStandard.Caption := TXT_NUMINFO_NO[Lng];

    (* Precisi�n *)
    txtPrecision.Caption := Format(TXT_NUMINFO_PRECISION[Lng],
      [FPN.ExponentBits, FPN.MantissaBits]);

    (* Tipo *)
    case FPN.Kind of
      fkNormal:     txtKind.Caption := TXT_NUMINFO_WRD_NORMALIZED[Lng];
      fkSubnormal:  txtKind.Caption := TXT_NUMINFO_WRD_NOT_NORMALIZED[Lng];
      fkInf:        txtKind.Caption := TXT_NUMINFO_WRD_INFINITE[Lng];
      fkNaN:        txtKind.Caption := TXT_NUMINFO_WRD_NAN[Lng];
      fkZero:       txtKind.Caption := TXT_NUMINFO_WRD_ZERO[Lng];
    end;

    (* Descripci�n *)
    case FPN.Kind of
      fkSubnormal:  txtDesc.Caption := TXT_NUMINFO_SUBNORMAL[Lng];
      fkInf:        txtDesc.Caption := TXT_NUMINFO_INF[Lng];
      fkNaN:        txtDesc.Caption := TXT_NUMINFO_NAN[Lng];
      fkZero:       txtDesc.Caption := TXT_NUMINFO_ZERO[Lng];
      fkNormal:     txtDesc.Caption :=
        Format(TXT_NUMINFO_NORMAL[Lng], [FPN.Bias - 1, FPN.Bias]);
    end;
  end;
  if FPN <> nil then
    FPN.Free;
end;

end.
