unit Tables;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, RafMsg, Text;

type
  TfrmTables = class(TForm)
    imgAdderOperation: TImage;
    imgAdderResultSign: TImage;
    imgMultResultSign: TImage;
    lblAdderOperation: TStaticText;
    lblAdderResultSign: TStaticText;
    lblMultResultSign: TStaticText;
    cbtAdderOperation: TBitBtn;
    cbtMultResultSign: TBitBtn;
    cbtAdderResultSign: TBitBtn;
    procedure cbtAdderOperationClick(Sender: TObject);
    procedure cbtMultResultSignClick(Sender: TObject);
    procedure cbtAdderResultSignClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmTables: TfrmTables;

implementation

{$R *.dfm}

procedure TfrmTables.cbtAdderOperationClick(Sender: TObject);
begin
  ShowInfoText(TXT_TBLS_ADDER_OPERATION[Lng]);
end;

procedure TfrmTables.cbtMultResultSignClick(Sender: TObject);
begin
  ShowInfoText(TXT_TBLS_MULT_RESULT_SIGN[Lng]);
end;

procedure TfrmTables.cbtAdderResultSignClick(Sender: TObject);
begin
  ShowInfoText(TXT_TBLS_ADDER_RESULT_SIGN[Lng]);
end;

procedure TfrmTables.FormShow(Sender: TObject);
begin
  Caption                     := TXT_TBLS_CAPTION[Lng];
  cbtAdderOperation.Caption   := TXT_TBLS_CBT_MORE[Lng];
  cbtAdderResultSign.Caption  := TXT_TBLS_CBT_MORE[Lng];
  cbtMultResultSign.Caption   := TXT_TBLS_CBT_MORE[Lng];
  lblAdderOperation.Caption   := TXT_TBLS_LBL_ADDER_OPERATION[Lng];
  lblAdderResultSign.Caption  := TXT_TBLS_LBL_ADDER_RESULT_SIGN[Lng];
  lblMultResultSign.Caption   := TXT_TBLS_LBL_MULT_RESULT_SIGN[Lng];
  cbtAdderOperation.SetFocus;
end;

end.
