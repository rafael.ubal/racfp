unit Language;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ExtCtrls, Text, RafMsg;

type
  TfrmLanguage = class(TForm)
    rdLanguage: TRadioGroup;
    cbtOK: TBitBtn;
    cbtCancel: TBitBtn;
    Image1: TImage;
    Image2: TImage;
    procedure FormShow(Sender: TObject);
    procedure cbtOKClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLanguage: TfrmLanguage;

implementation

{$R *.dfm}

procedure TfrmLanguage.FormShow(Sender: TObject);
begin
  rdLanguage.ItemIndex := Lng;
  rdLanguage.SetFocus;
  rdLanguage.Caption := TXT_LANGUAGE[Lng];
  Caption := TXT_LANGUAGE_SELECT[Lng];
  cbtOK.Caption := TXT_BUT_OK[Lng];
  cbtCancel.Caption := TXT_BUT_CANCEL[Lng];
end;

procedure TfrmLanguage.cbtOKClick(Sender: TObject);
begin
  Lng := rdLanguage.ItemIndex;
end;

end.
