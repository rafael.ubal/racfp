unit HPAdder;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Numbers, Auxiliar, AskEntry, Functions,
  Buttons, RafMsg, Text;

type
  TfrmHPAdder = class(TForm)
    pnlMain: TPanel;
    Image1: TImage;
    lbl_0_1: TStaticText;
    lbl_0_2: TStaticText;
    lbl_0_3: TStaticText;
    lbl_0_4: TStaticText;
    lbl_0_5: TStaticText;
    lbl_0_6: TStaticText;
    lbl_0_7: TStaticText;
    lbl_0_8: TStaticText;
    lbl_0_9: TStaticText;
    lbl_0_10: TStaticText;
    lbl_1_1: TStaticText;
    lbl_1_2: TStaticText;
    lbl_2_2: TStaticText;
    lbl_2_1: TStaticText;
    lbl_3_1: TStaticText;
    lbl_4_2: TStaticText;
    lbl_4_1: TStaticText;
    lbl_5_1: TStaticText;
    lbl_6_1: TStaticText;
    lbl_5_2: TStaticText;
    lbl_6_2: TStaticText;
    lbl_6_5: TStaticText;
    lbl_6_4: TStaticText;
    lbl_6_3: TStaticText;
    cbtNext: TBitBtn;
    cbtEnd: TBitBtn;
    cbtInfo: TBitBtn;
    cbtDesc: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure cbtNextClick(Sender: TObject);
    procedure cbtEndClick(Sender: TObject);
    procedure SetStep(State: Boolean);
    procedure lblMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbtInfoClick(Sender: TObject);
    procedure cbtDescClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmHPAdder: TfrmHPAdder;

implementation
{$R *.dfm}

const
  STAGE_NUM     = 7;
  STAGE_HEIGHT: array[0..STAGE_NUM - 1] of Integer = (
    152, 245, 292, 339, 386, 433, 517
  );

var
  Step: Integer;
  DescWindow: TDescriptionWindow;

  AE: TAskEntry;
  S, E, M: array[0..2] of string;
  SInt: array[0..2] of Integer;
  BigExp, SmallExp,          //N�mero del exponente mayor y menor (0 � 1)
  Op: Integer;
  Overflow: Boolean;
  NumRes: TFloatingPointNumber;

procedure TfrmHPAdder.FormShow(Sender: TObject);
begin
  //Textos
  Caption          := TXT_HPADD_CAPTION[Lng];
  lbl_0_1.Caption  := TXT_HPADD_LBL_EXP[Lng] + ' A';
  lbl_0_2.Caption  := TXT_HPADD_LBL_EXP[Lng] + ' B';
  lbl_0_3.Caption  := TXT_HPADD_LBL_SIGNIF[Lng] + ' A';
  lbl_0_4.Caption  := TXT_HPADD_LBL_SIGNIF[Lng] + ' B';
  lbl_0_5.Caption  := TXT_HPADD_LBL_FORMAT[Lng];
  lbl_0_6.Caption  := TXT_HPADD_LBL_FORMAT[Lng];
  lbl_0_7.Caption  := TXT_HPADD_LBL_EXPCOMP[Lng];
  lbl_0_8.Caption  := TXT_HPADD_LBL_MUX[Lng];
  lbl_0_9.Caption  := TXT_HPADD_LBL_SWAP[Lng];
  lbl_0_10.Caption := TXT_HPADD_LBL_SWAP[Lng];
  lbl_1_1.Caption  := TXT_HPADD_LBL_ALIGN_SHIFT[Lng];
  lbl_1_2.Caption  := TXT_HPADD_LBL_ADD_SUB[Lng];
  lbl_2_1.Caption  := TXT_HPADD_LBL_INC[Lng];
  lbl_2_2.Caption  := TXT_HPADD_LBL_RIGHT_SHIFT[Lng];
  lbl_3_1.Caption  := TXT_HPADD_LBL_PRI_ENC[Lng];
  lbl_4_1.Caption  := TXT_HPADD_LBL_DEC[Lng];
  lbl_4_2.Caption  := TXT_HPADD_LBL_LEFT_SHIFT[Lng];
  lbl_5_1.Caption  := TXT_HPADD_LBL_INC[Lng];
  lbl_5_2.Caption  := TXT_HPADD_LBL_ROUND_INC[Lng];
  lbl_6_1.Caption  := TXT_HPADD_LBL_CONST[Lng];
  lbl_6_2.Caption  := TXT_HPADD_LBL_GEN_CONST[Lng];
  lbl_6_3.Caption  := TXT_HPADD_LBL_STATUS[Lng];
  lbl_6_4.Caption  := TXT_HPADD_LBL_EXP_RES[Lng];
  lbl_6_5.Caption  := TXT_HPADD_LBL_SIGNIF_RES[Lng];
  cbtNext.Caption  := TXT_HPADD_CBT_NEXT[Lng];
  cbtEnd.Caption   := TXT_HPADD_CBT_END[Lng];
  cbtInfo.Caption  := TXT_HPADD_CBT_INFO[Lng];
  cbtDesc.Caption  := TXT_HPADD_CBT_DESC[Lng];

  //Acciones
  DescWindow := TDescriptionWindow.Create(TXT_ELEMENT_DESCRIPTION[Lng]);
  Step := 0;
  SetStep(True);
end;

procedure TfrmHPAdder.lblMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  DescWindow.SetDescription(TStaticText(Sender).Hint);
end;

procedure TfrmHPAdder.cbtInfoClick(Sender: TObject);
begin
  DescWindow.HideAndSave;
  ShowInfoText(TXT_HPADD_INFO[Lng]);
  DescWindow.Restore;
end;

procedure TfrmHPAdder.cbtDescClick(Sender: TObject);
begin
  DescWindow.ToggleShowing;
end;

procedure TfrmHPAdder.cbtNextClick(Sender: TObject);
begin
  SetStep(False);
  inc(Step);
  SetStep(True);
end;

procedure TfrmHPAdder.cbtEndClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmHPAdder.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  SetStep(False);
  DescWindow.Free;
end;

procedure TfrmHPAdder.SetStep;
label
  StepAction;
var
  str, res: string;
  i: Integer;
begin

  StepAction:

  (* Acciones a realizar s�lo si se activa un estado *)
  if State then case Step of

    (* Pregunta entradas. Si no son correctas, vuelve a 0
     * Primera etapa *)
    1: begin
      if not AskUserEntry(AE, 0) then begin
        Step := 0;
        goto StepAction;
      end;

      ExpandOperandsInfo(AE.Num, S, E, M, SInt);
      lbl_0_1.Hint := E[0];
      lbl_0_2.Hint := E[1];
      lbl_0_3.Hint := M[0];
      lbl_0_4.Hint := M[1];

      M[0] := M[0] + '00';
      M[1] := M[1] + '00';
      AddImplicitBit(E[0], M[0]);
      AddImplicitBit(E[1], M[1]);
      lbl_0_5.Hint := Format(TXT_HPADD_HINT_FORMAT_SIGNIF[Lng], ['A', M[0]]);
      lbl_0_6.Hint := Format(TXT_HPADD_HINT_FORMAT_SIGNIF[Lng], ['B', M[1]]);

      BigExp := BinCompare(E[0], E[1]);
      if BigExp < 0 then BigExp := 0;
      SmallExp := (BigExp + 1) mod 2;
      lbl_0_7.Hint := Format(TXT_HPADD_HINT_EXPCOMP[Lng], [chr(BigExp + 65), E[BigExp]]);
      lbl_0_8.Hint := Format(TXT_HPADD_HINT_MUX[Lng], [chr(BigExp + 65), E[BigExp]]);
      lbl_0_9.Hint := Format(TXT_HPADD_HINT_SWAP_A[Lng], [M[SmallExp]]);
      lbl_0_10.Hint := Format(TXT_HPADD_HINT_SWAP_B[Lng], [M[BigExp]]);
    end;

    (* Segunda etapa *)
    2: begin
      str := M[SmallExp];
      CompareAndSetExponents(E[0], M[0], E[1], M[1]);
      lbl_1_1.Hint := Format(TXT_HPADD_HINT_ALIGN_SHIFT[Lng], [str, M[SmallExp]]);

      Op := GetAdderOperation(SInt[0], SInt[1], AE.OpUser);
      AddSignificands(S[2], M[2], '0', M[0], '0', M[1], Op = 0);
      E[2] := E[1];
      SInt[2] := BinToInt(S[2]);
      lbl_1_2.Hint := Format(TXT_HPADD_HINT_ADD_SUB[Lng], [M[0], TXT_ADDITION_SYMBOL[Op],
        M[1], S[2], M[2]]);
    end;

    (* Tercera etapa *)
    3: if M[2, 1] = '1' then begin

      AddPositive(E[2], '1', res);
      Overflow := (Pos('0', res) = 0);
      lbl_2_1.Hint := Format(TXT_HPADD_HINT_EXP_INC_OV[Lng], [E[2], res]);
      E[2] := res;

      str := BinShift(M[2], 1, '0');
      lbl_2_2.Hint := Format(TXT_HPADD_HINT_RIGHT_SHIFT_OV[Lng], [M[2], str]);
      M[2] := str;

    end else begin

      lbl_2_1.Hint := Format(TXT_HPADD_HINT_EXP_INC[Lng], [E[2]]);
      lbl_2_2.Hint := lbl_2_1.Hint;

    end;


    (* Cuarta etapa *)
    4: begin
      i := Pos('1', M[2]) - 2;
      if i < 0 then
        i := 0;
      str := IntToBin(i, 6);
      lbl_3_1.Hint := Format(TXT_HPADD_HINT_PRIO_ENC[Lng], [str]);
    end;

    (* Quinta etapa *)
    5: begin
      E[0] := E[2];
      M[0] := M[2];
      Normalize(E[2], M[2], 2);
      lbl_4_1.Hint := Format(TXT_HPADD_HINT_EXP_DEC[Lng], [E[0], E[2]]);
      lbl_4_2.Hint := Format(TXT_HPADD_HINT_LEFT_SHIFT[Lng], [M[0], M[2]]);
    end;

    (* Sexta etapa *)
    6: begin
      str := M[2];
      RoundSignificand(S[2], M[2], AE.RoundMethod);
      if M[2, 1] = '1' then begin
        AddPositive(E[2], '1', res);
        Overflow := Overflow or (Pos('0', res) = 0);
        lbl_5_1.Hint := Format(TXT_HPADD_HINT_EXP_INC2_YES[Lng], [E[2], res]);
        E[2] := res;
        M[2] := BinShift(M[2], 1, '0');
      end else begin
        lbl_5_1.Hint := Format(TXT_HPADD_HINT_EXP_INC2_NO[Lng], [E[2]]);
      end;
      lbl_5_2.Hint := Format(TXT_HPADD_HINT_ROUND_INC[Lng],
        [TXT_ROUND[Lng, Integer(AE.RoundMethod)], str, M[2]]);
    end;

    (* S�ptima etapa *)
    7: begin
      Delete(M[2], 1, 2);

      NumRes := TFloatingPointNumber.Create(
        fiBin, AE.num[0].Format, 0, 0, S[2] + E[2] + M[2]);
      lbl_6_3.Hint := Format(TXT_HPADD_HINT_SUCCESS[Lng], [AE.Num[0].Sign,
        AE.Num[0].Exponent, AE.Num[0].Mantissa, AE.Num[0].DecValue,
        TXT_ADDITION_SYMBOL[AE.OpUser], AE.Num[1].Sign, AE.Num[1].Exponent,
        AE.Num[1].Mantissa, AE.Num[1].DecValue, S[2], E[2], M[2],
        NumRes.DecValue]);
      NumRes.Free;

      if Overflow then begin
        BinZeros(M[2]);
        BinOnes(E[2]);
        lbl_6_3.Hint := TXT_OVERFLOW[Lng];
      end;
      lbl_6_1.Hint := Format(TXT_HPADD_HINT_CONST[Lng], [E[2]]);
      lbl_6_2.Hint := Format(TXT_HPADD_HINT_GEN_CONST[Lng], [M[2]]);
      lbl_6_4.Hint := E[2];
      lbl_6_5.Hint := M[2];
    end;

  end;

  (* Acciones a realizar al activar y desactivar *)
  SetStage(Self, Step - 1, State, STAGE_HEIGHT);
  DescWindow.SetPosition(Step > 4);
  cbtNext.Enabled := Step < STAGE_NUM;
end;

end.
