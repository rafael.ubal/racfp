(*
-Caso en que ocurre la desnormalizaci�n al redondear
  00000000000000000000000000000011
  00000001011111111111111111111111

-Caso en el que hay desbordamiento superior
  01111111011111111111111111111111
  01111111011111111111111111111111
*)
unit AdderAlgorithm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, Buttons, Numbers, RafMsg,
  Auxiliar, Functions, AskEntry, Text;

type
  TfrmAdderAlgorithm = class(TForm)
    lblInit: TStaticText;
    lblStep1: TStaticText;
    lblStep2: TStaticText;
    lblStep3: TStaticText;
    lblOverflow: TStaticText;
    lblStep4: TStaticText;
    lblNormalized: TStaticText;
    lblEnd: TStaticText;
    lblException: TStaticText;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    lblYes1: TStaticText;
    lblNo1: TStaticText;
    lblYes2: TStaticText;
    Shape4: TShape;
    Shape5: TShape;
    Shape6: TShape;
    lblNo2: TStaticText;
    cbtNext: TBitBtn;
    cbtEnd: TBitBtn;
    lblDesc: TStaticText;
    Image1: TImage;
    procedure cbtNextClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure SetStep(aStep: Integer; State: boolean);
    procedure cbtEndClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmAdderAlgorithm: TfrmAdderAlgorithm;

implementation
{$R *.dfm}

const
  MAX_STEP = 12;

var
  Step: Integer;

  AE: TAskEntry;
  S, M, E: array[0..2] of string; //Para cada array: elem 0 = 1er operando. elem 1 = 2� operando. elem 2 = resultado
  SInt: array[0..2] of Integer;
  Op: Integer;               //Operaci�n que realizar� el sum/rest de mantisas
  OverFlow, YetNormalized: Boolean;


(***  EVENTOS DE LA VENTANA  ***)

procedure TfrmAdderAlgorithm.FormShow(Sender: TObject);
begin
  //Nombres de elementos seg�n idioma
  Caption               := TXT_ALGADD_CAPTION[Lng];
  lblInit.Caption       := TXT_ALGADD_LBL_INIT[Lng];
  lblStep1.Caption      := TXT_ALGADD_LBL_STEP1[Lng];
  lblStep2.Caption      := TXT_ALGADD_LBL_STEP2[Lng];
  lblStep3.Caption      := TXT_ALGADD_LBL_STEP3[Lng];
  lblOverflow.Caption   := TXT_ALGADD_LBL_OVERFLOW[Lng];
  lblException.Caption  := TXT_ALGADD_LBL_EXCEPTION[Lng];
  lblStep4.Caption      := TXT_ALGADD_LBL_STEP4[Lng];
  lblNormalized.Caption := TXT_ALGADD_LBL_NORMALIZED[Lng];
  lblEnd.Caption        := TXT_ALGADD_LBL_END[Lng];
  lblYes1.Caption       := TXT_YES[Lng];
  lblYes2.Caption       := TXT_YES[Lng];
  lblNo1.Caption        := TXT_NO[Lng];
  lblNo2.Caption        := TXT_NO[Lng];
  cbtNext.Caption       := TXT_BUT_NEXT[Lng];
  cbtEnd.Caption        := TXT_BUT_END[Lng];

  //Activa primer estado
  SetStep(0, True);
  AE.Num[0] := nil;
  AE.Num[1] := nil;
  cbtNext.SetFocus;
end;

procedure TfrmAdderAlgorithm.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  i: Integer;
begin
  SetStep(Step, False);
  for i := 0 to 1 do if AE.Num[i] <> nil then begin
    AE.Num[i].Free;
    AE.Num[i] := nil;
  end;
end;

procedure TfrmAdderAlgorithm.cbtNextClick(Sender: TObject);
begin
  SetStep(Step, False);
  inc(Step);
  SetStep(Step, True);
end;

procedure TfrmAdderAlgorithm.cbtEndClick(Sender: TObject);
begin
  Close;
end;


(***  FUNCI�N DE TRANSICI�N DE ESTADOS  ***
 Tiene 3 partes:
  -Acciones al activar un estado por primera vez (s�lo si state es true)
  -Acciones al activar/desactivar estado
  -Mostrar informaci�n del estado (s�lo si state es true)
 *)
procedure TfrmAdderAlgorithm.SetStep;
label
  StepAction;
var
  i: Integer;
  str, Desc: string;
  NumRes: TFloatingPointNumber;
begin
  (* Acciones al activar un estado por primera vez *)
  StepAction:
  if State then case aStep of

    //Inico
    0: begin
      Desc := TXT_ALGADD_STEP[Lng, 0];
    end;

    //Lee la entrada. Si es err�nea, vuelve a 0
    1: begin
      if not AskUserEntry(AE, 0) then begin
        aStep := 0;
        goto StepAction;
      end;

      ExpandOperandsInfo(AE.Num, S, E, M, SInt);
      for i := 0 to 1 do M[i] := M[i] + '00';

      Desc := Format(TXT_ALGADD_STEP[Lng, 1], [AE.Num[0].Sign, AE.Num[0].Exponent,
        AE.Num[0].Mantissa, AE.Num[0].DecValue, AE.Num[1].Sign, AE.Num[1].Exponent,
        AE.Num[1].Mantissa, AE.Num[1].DecValue]);
    end;

    //A�ade bits impl�citos y dos '0' al final
    2: begin
      for i := 0 to 1 do AddImplicitBit(E[i], M[i]);
      Desc := Format(TXT_ALGADD_STEP[Lng, 2], [M[0], M[1]]);
    end;

    //Desplaza mantisa menor e iguala exponentes
    3: begin
      CompareAndSetExponents(E[0], M[0], E[1], M[1]);
      Desc := Format(TXT_ALGADD_STEP[Lng, 3], [E[0], M[0], E[1], M[1]]);
    end;

    //Calcula operaci�n a realizar
    4: begin
      Op := GetAdderOperation(SInt[0], SInt[1], AE.OpUser);
      Desc := Format(TXT_ALGADD_STEP[Lng, 4], [SInt[0], SInt[1], AE.OpUser, Op,
        TXT_ADDITION_NAME[Lng, Op]]);
    end;

    //Opera con mantisas
    5: begin
      AddSignificands(S[2], M[2], '0', M[0], '0', M[1], Op = 0);
      E[2] := E[1];
      SInt[2] := BinToInt(S[2]);
      Desc := Format(TXT_ALGADD_STEP[Lng, 5], [M[0],
        TXT_ADDITION_SYMBOL[Op], M[1], S[2], M[2]]);
    end;

    //Normaliza
    6: begin
      E[0] := E[2];
      M[0] := M[2];
      Normalize(E[2], M[2], 2);
      Desc := Format(TXT_ALGADD_STEP[Lng, 6], [E[0], M[0], E[2], M[2]]);
    end;

    //�Ocurre excepci�n?
    7: begin
      OverFlow := Pos('0', E[2]) = 0;
      if OverFlow then str := TXT_AFFIRMATION[Lng] else str := TXT_NEGATION[Lng];
      Desc := Format(TXT_ALGADD_STEP[Lng, 7], [str]);
    end;

    //Excepci�n
    8: begin
      if not OverFlow then begin
        inc(aStep);
        goto StepAction;
      end;
      Desc := TXT_ALGADD_STEP[Lng, 8];
    end;

    //Redondeo
    9: begin
      M[0] := M[2];
      RoundSignificand(S[2], M[2], AE.RoundMethod);
      Desc := Format(TXT_ALGADD_STEP[Lng, 9],
        [TXT_ROUND[Lng, Integer(AE.RoundMethod)], M[0], M[2]]);
    end;

    //Normalizado a�n?
    10: begin
      YetNormalized := M[2][1] = '0';
      if YetNormalized then str := TXT_NEGATION[Lng] else str := TXT_AFFIRMATION[Lng];
      Desc := Format(TXT_ALGADD_STEP[Lng, 10], [str]);
    end;

    //Fin
    11: if YetNormalized then begin
      Delete(M[2], 1, 2);
      S[2] := IntToStr(GetAdderResultSign(SInt[0], SInt[1], SInt[2], AE.OpUser));
      NumRes := TFloatingPointNumber.Create(
        fiBin, AE.num[0].Format, 0, 0, S[2] + E[2] + M[2]);
      Desc := Format(TXT_ALGADD_STEP[Lng, 11], [AE.Num[0].Sign, AE.Num[0].Exponent,
        AE.Num[0].Mantissa, AE.Num[0].DecValue, TXT_ADDITION_SYMBOL[AE.OpUser],
        AE.Num[1].Sign, AE.Num[1].Exponent,
        AE.Num[1].Mantissa, AE.Num[1].DecValue, S[2], E[2], M[2],
        NumRes.DecValue]);
      NumRes.Free;
    end else begin
      M[2] := M[2] + '00';
      aStep := 6;
      goto StepAction;
    end;

  end;


  (* Acciones al activar/desactivar estado *)
  case aStep of
    1, 2: SetLabelState(lblInit, State);
    3: SetLabelState(lblStep1, State);
    4, 5: SetLabelState(lblStep2, State);
    6: SetLabelState(lblStep3, State);
    7: SetLabelState(lblOverflow, State);
    8: SetLabelState(lblException, State);
    9: SetLabelState(lblStep4, State);
    10: SetLabelState(lblNormalized, State);
    11: SetLabelState(lblEnd, State);
  end;


  (* Muestra descripci�n del estado *)
  if State then begin
    lblDesc.Caption := Desc;
    cbtNext.Enabled := not (aStep in [MAX_STEP - 1, 8]);
    Step := aStep;
  end;
end;

end.
