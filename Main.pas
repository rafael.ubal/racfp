unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, RafMsg, Functions, Numbers,
  NumberInformation, Buttons, Text,
  AdderAlgorithm, MultiplierAlgorithm, Tables, HPAdder, AskEntry, HPMultiplier,
  Language, About, Functions2;


type
  TfrmMain = class(TForm)
    cbtConversion: TBitBtn;
    grpAlgorithms: TGroupBox;
    cbtGenericAdder: TBitBtn;
    grpCircuits: TGroupBox;
    cbtTables: TBitBtn;
    cbtAbout: TBitBtn;
    cbtLanguage: TBitBtn;
    cbtHPMultiplier: TBitBtn;
    cbtClose: TBitBtn;
    cbtGenericMultiplier: TBitBtn;
    cbtHPAdder: TBitBtn;
    procedure cbtConversionClick(Sender: TObject);
    procedure cbtGenericAdderClick(Sender: TObject);
    procedure cbtGenericMultiplierClick(Sender: TObject);
    procedure cbtTablesClick(Sender: TObject);
    procedure cbtAboutClick(Sender: TObject);
    procedure cbtHPAdderClick(Sender: TObject);
    procedure cbtHPMultiplierClick(Sender: TObject);
    procedure cbtCloseClick(Sender: TObject);
    procedure cbtLanguageClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  frmMain: TfrmMain;

implementation

uses AskNumber;
{$R *.dfm}

procedure TfrmMain.cbtConversionClick(Sender: TObject);
begin
  frmNumberInfo.ShowModal;
end;

procedure TfrmMain.cbtGenericAdderClick(Sender: TObject);
begin
  frmAdderAlgorithm.ShowModal;
end;

procedure TfrmMain.cbtGenericMultiplierClick(Sender: TObject);
begin
  frmMultiplierAlgorithm.ShowModal;
end;

procedure TfrmMain.cbtTablesClick(Sender: TObject);
begin
  frmTables.ShowModal;
end;

procedure TfrmMain.cbtAboutClick(Sender: TObject);
begin
  frmAbout.ShowModal;
end;

procedure TfrmMain.cbtHPAdderClick(Sender: TObject);
begin
  frmHPAdder.ShowModal;
end;

procedure TfrmMain.cbtHPMultiplierClick(Sender: TObject);
begin
  frmHPMultiplier.ShowModal;
end;

procedure TfrmMain.cbtCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.cbtLanguageClick(Sender: TObject);
begin
  frmLanguage.ShowModal;
  FormShow(Sender);
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //Selecciona idioma para mensajes emergentes
  case Lng of
    0: SetMessageBoxLanguageSpanish;
    1: SetMessageBoxLanguageEnglish;
  end;

  //Actualiza los textos de la ventana
  Caption                     := TXT_MAIN_CAPTION[Lng];
  Application.Title           := TXT_MAIN_CAPTION[Lng];
  cbtConversion.Caption       := TXT_MAIN_CONVERSION[Lng];
  cbtTables.Caption           := TXT_MAIN_TABLES[Lng];
  cbtAbout.Caption            := TXT_MAIN_ABOUT[Lng];
  cbtClose.Caption            := TXT_MAIN_EXIT[Lng];
  cbtLanguage.Caption         := TXT_MAIN_LANGUAGE[Lng];
  cbtGenericAdder.Caption     := TXT_MAIN_ADDSUB[Lng];
  cbtGenericMultiplier.Caption:= TXT_MAIN_MULTDIV[Lng];
  cbtHPAdder.Caption          := TXT_MAIN_ADDSUB[Lng];
  cbtHPMultiplier.Caption     := TXT_MAIN_MULTDIV[Lng];
  grpAlgorithms.Caption       := TXT_MAIN_ALGORITHMS[Lng];
  grpCircuits.Caption         := TXT_MAIN_CIRCUITS[Lng];
end;

end.
