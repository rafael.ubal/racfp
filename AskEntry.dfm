object frmAskEntry: TfrmAskEntry
  Left = 248
  Top = 143
  BorderStyle = bsDialog
  Caption = 'Entrada de la operaci'#243'n...'
  ClientHeight = 162
  ClientWidth = 353
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object chkAction: TRadioGroup
    Left = 5
    Top = 96
    Width = 169
    Height = 65
    Caption = 'Acci'#243'n a realizar'
    TabOrder = 0
  end
  object grpInput: TGroupBox
    Left = 181
    Top = 8
    Width = 169
    Height = 81
    Caption = 'Valores de entrada'
    TabOrder = 1
    object fldNum1: TEdit
      Left = 8
      Top = 20
      Width = 121
      Height = 21
      TabOrder = 0
      Text = '5'
    end
    object fldNum2: TEdit
      Left = 8
      Top = 48
      Width = 121
      Height = 21
      TabOrder = 2
      Text = '-2'
    end
    object cbtChoose1: TButton
      Left = 136
      Top = 23
      Width = 17
      Height = 17
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 1
      OnClick = cbtChooseClick
    end
    object cbtChoose2: TButton
      Left = 136
      Top = 51
      Width = 17
      Height = 17
      Caption = '...'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'MS Sans Serif'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 3
      OnClick = cbtChooseClick
    end
  end
  object chkRound: TRadioGroup
    Left = 5
    Top = 8
    Width = 169
    Height = 81
    Caption = 'Ajuste del resultado'
    ItemIndex = 3
    Items.Strings = (
      'A +Infinito'
      'A - Infinito'
      'A cero'
      'Al par m'#225's pr'#243'ximo')
    TabOrder = 2
  end
  object cbtOk: TBitBtn
    Left = 200
    Top = 104
    Width = 129
    Height = 24
    Caption = '&Aceptar'
    Default = True
    TabOrder = 3
    OnClick = cbtOkClick
    Glyph.Data = {
      DE010000424DDE01000000000000760000002800000024000000120000000100
      0400000000006801000000000000000000001000000000000000000000000000
      80000080000000808000800000008000800080800000C0C0C000808080000000
      FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
      3333333333333333333333330000333333333333333333333333F33333333333
      00003333344333333333333333388F3333333333000033334224333333333333
      338338F3333333330000333422224333333333333833338F3333333300003342
      222224333333333383333338F3333333000034222A22224333333338F338F333
      8F33333300003222A3A2224333333338F3838F338F33333300003A2A333A2224
      33333338F83338F338F33333000033A33333A222433333338333338F338F3333
      0000333333333A222433333333333338F338F33300003333333333A222433333
      333333338F338F33000033333333333A222433333333333338F338F300003333
      33333333A222433333333333338F338F00003333333333333A22433333333333
      3338F38F000033333333333333A223333333333333338F830000333333333333
      333A333333333333333338330000333333333333333333333333333333333333
      0000}
    NumGlyphs = 2
  end
  object cbtCancel: TBitBtn
    Left = 200
    Top = 135
    Width = 129
    Height = 26
    Caption = '&Cancelar'
    TabOrder = 4
    Kind = bkCancel
  end
end
