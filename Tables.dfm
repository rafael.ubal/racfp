object frmTables: TfrmTables
  Left = 173
  Top = 135
  BorderStyle = bsDialog
  Caption = 'Tablas'
  ClientHeight = 361
  ClientWidth = 434
  Color = clInfoBk
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object imgAdderOperation: TImage
    Left = 32
    Top = 24
    Width = 161
    Height = 170
    Picture.Data = {
      07544269746D6170860D0000424D860D0000000000003E000000280000009D00
      0000AA0000000100010000000000480D00000000000000000000020000000000
      000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFF8000000000000FFFF
      FFFFFFFFFFFFFFFFFFFFFFF9FFFFFFFFFFF0FFFFFFFFFFFFFFFFFFFFFFFFFFF9
      00000000001000000000000000000000000000010000000000107FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFE
      07FFDFFF81FFF7FFFC0FFFF97FFF81FFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF90000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF9
      FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FFFFFFFFF9000000000010000000000000
      00000000000000010000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFF7FFFF97FFFEFFFFF907FFE07FFDFFF81FFF7FFFF7FFFF9
      7FFFEFFFFF907FFFFFFFDFFFFFFFF7FFFC1FFFF97FFF83FFFF907FFFFFFFDFFF
      FFFFF7FFFF7FFFF97FFFEFFFFF907FFFFFFFDFFFFFFFF7FFFF7FFFF97FFFEFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      0000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF9FFFFFFFFFFF07FFFFFFFDFFF
      FFFFF7FFFFFFFFF9000000000010000000000000000000000000000100000000
      00107FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFEFFFF7FFFFFFFFF9
      7FFFEFFFFF907FFE07FFDFFFEFFFF7FFFC0FFFF97FFFEFFFFF907FFFFFFFDFFF
      83FFF7FFFFFFFFF97FFF83FFFF907FFFFFFFDFFFEFFFF7FFFFFFFFF97FFFEFFF
      FF907FFFFFFFDFFFEFFFF7FFFFFFFFF97FFFEFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF90000000000107FFFFFFFDFFF
      FFFFF7FFFFFFFFF9FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FFFFFFFFF900000000
      001000000000000000000000000000010000000000107FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFEFFFF7FFFF7FFFF97FFFFFFFFF907FFE07FFDFFF
      EFFFF7FFFF7FFFF97FFF81FFFF907FFFFFFFDFFF83FFF7FFFC1FFFF97FFFFFFF
      FF907FFFFFFFDFFFEFFFF7FFFF7FFFF97FFFFFFFFF907FFFFFFFDFFFEFFFF7FF
      FF7FFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF90000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF9FFFFFFFF
      FFF07FFFFFFFDFFFFFFFF7FFFFFFFFF900000000001000000000000000000000
      000000010000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFBFFFDFFF
      FFFFF7FFFFFFFFF97FFFEFFFFF907FFFBFFFDFFF81FFF7FFFC0FFFF97FFFEFFF
      FF907FFE0FFFDFFFFFFFF7FFFFFFFFF97FFF83FFFF907FFFBFFFDFFFFFFFF7FF
      FFFFFFF97FFFEFFFFF907FFFBFFFDFFFFFFFF7FFFFFFFFF97FFFEFFFFF907FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF900000000
      00107FFFFFFFDFFFFFFFF7FFFFFFFFF9FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FF
      FFFFFFF900000000001000000000000000000000000000010000000000107FFF
      FFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFBFFFDFFFFFFFF7FFFF7FFFF97FFFFFFF
      FF907FFFBFFFDFFF81FFF7FFFF7FFFF97FFF81FFFF907FFE0FFFDFFFFFFFF7FF
      FC1FFFF97FFFFFFFFF907FFFBFFFDFFFFFFFF7FFFF7FFFF97FFFFFFFFF907FFF
      BFFFDFFFFFFFF7FFFF7FFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF90000000000107FFFFFFFDFFFFFFFF7FF
      FFFFFFF9FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FFFFFFFFF90000000000100000
      0000000000000000000000010000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFBFFFDFFFEFFFF7FFFFFFFFF97FFFFFFFFF907FFFBFFFDFFFEFFFF7FF
      FC0FFFF97FFF81FFFF907FFE0FFFDFFF83FFF7FFFFFFFFF97FFFFFFFFF907FFF
      BFFFDFFFEFFFF7FFFFFFFFF97FFFFFFFFF907FFFBFFFDFFFEFFFF7FFFFFFFFF9
      7FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF90000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF9FFFFFFFFFFF07FFF
      FFFFDFFFFFFFF7FFFFFFFFF90000000000100000000000000000000000000001
      0000000000107FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFBFFFDFFFEFFFF7FF
      FF7FFFF97FFFEFFFFF907FFFBFFFDFFFEFFFF7FFFF7FFFF97FFFEFFFFF907FFE
      0FFFDFFF83FFF7FFFC1FFFF97FFF83FFFF907FFFBFFFDFFFEFFFF7FFFF7FFFF9
      7FFFEFFFFF907FFFBFFFDFFFEFFFF7FFFF7FFFF97FFFEFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF90000000000107FFF
      FFFFDFFFFFFFF7FFFFFFFFF9FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FFFFFFFFF9
      00000000001000000000000000000000000000010000000000107FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFF1FFFF97FFF8FFF
      FF907FFFFFFFDFFFFFFFF7FFFFBFFFF97FFFDFFFFF907FFFFFFFDFFFFFFFF7FF
      FFBFFFF97FFFDFFFFF907FFFFFFFDFFFFFFFF7FFE38FFFF97FF1C7FFFF907FFE
      23FFDFFF0FFFF7FFDDB7FFF97FEEDBFFFF907FFF77FFDFFFB7FFF7FFDDB7FFF9
      7FEEDBFFFF907FFF87FFDFFFB7FFF7FFDDB7FFF97FEEDBFFFF907FFFAFFFDFFF
      8FFFF7FFE30FFFF97FF187FFFF907FFFAFFFDFFFB7FFF7FFFFFFFFF97FFFFFFF
      FF907FFFAFFFDFFFB7FFF7FFFFFFFFF97FFFFFFFFF907FFFDFFFDFFF0FFFF7FF
      FFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFF
      0FFFDFFF87FFF7FFFFFFFFF97FFFFFFFFF907FFF77FFDFFFBBFFF7FFFFFFFFF9
      7FFFFFFFFF907FFF67FFDFFFB3FFF7FC4188FFF941023181B8907F08189FDF84
      0C4FF7FB9D7DFFF96DB5EFBB37907F6D7DBFDFB6BEDFF7FBD90DFFF96FB42FBB
      50907FED0DBFDFF686DFF7FBD76CFFF961B5ADBB56907F9D6CBFDFCEB65FF7FB
      D1997FF96F02710249907F79813FDFBCC09FF7FBDFFFFFF96DB7FFBFFF907F4F
      FFFFDFA7FFFFF7F18FFFFFF941B7FFFBFF907FADFFFFDFD6FFFFF7FFFFFFFFF9
      7FC9FFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFF
      FFFFF7FFFFFFFFF97FFFFFFFFF907FFFFFFFDFFFFFFFF7FFFFFFFFF97FFFFFFF
      FF907FFFFFFFDFFFFFFFF7FFFFFFFFF90000000000107FFFFFFFDFFFFFFFF7FF
      FFFFFFF9FFFFFFFFFFF07FFFFFFFDFFFFFFFF7FFFFFFFFF80000000000000000
      000000000000000000000000000000000000}
    Transparent = True
  end
  object imgAdderResultSign: TImage
    Left = 232
    Top = 24
    Width = 185
    Height = 306
    Picture.Data = {
      07544269746D6170EE1C0000424DEE1C0000000000003E00000028000000B500
      0000320100000100010000000000B01C00000000000000000000020000000000
      000000000000FFFFFF00FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE00000000
      0000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE7FFFFFFFF000FFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFE4000000010000000000000000000000000000000
      000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFBFFF
      90007FFE07FFDFFF81FFF7FFE07FFDFFFF03FFFE5FFFBFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFE0FFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFBFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFBFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE40000000100000000000000000000000000000000000000040000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF90007FFE07FFDFFF
      81FFF7FFE07FFDFFFFDFFFFE5FFE07FF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FF07FFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10000000000000000000000000000000000000004000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFE07FFDFFF81FFF7FFFBFFFDFF
      FF03FFFE5FFE07FF90007FFFFFFFDFFFFFFFF7FFE0FFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFF
      F0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE400000001000000000000000
      0000000000000000000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFBFFFDFF
      FFDFFFFE5FFFFFFF90007FFE07FFDFFF81FFF7FFFBFFFDFFFFDFFFFE5FFE07FF
      90007FFFFFFFDFFFFFFFF7FFE0FFFDFFFF07FFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFBFFFDFFFFDFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFBFFFDFF
      FFDFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010000000000000000000000000000000
      000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFEFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFE07FFDFFFEFFFF7FFE07FFDFFFF03FFFE5FFE07FF90007FFFFFFFDFFF
      83FFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFEFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFEFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE40000000100000000000000000000000000000000000000040000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF90007FFE07FFDFFF
      EFFFF7FFE07FFDFFFFDFFFFE5FFFBFFF90007FFFFFFFDFFF83FFF7FFFFFFFDFF
      FF07FFFE5FFE0FFF90007FFFFFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF
      90007FFFFFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10000000000000000000000000000000000000004000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      EFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFE07FFDFFFEFFFF7FFFBFFFDFF
      FF03FFFE5FFE07FF90007FFFFFFFDFFF83FFF7FFE0FFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFEFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      EFFFF7FFFBFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFF
      F0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE400000001000000000000000
      0000000000000000000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFEFFFF7FFFBFFFDFF
      FFDFFFFE5FFFFFFF90007FFE07FFDFFFEFFFF7FFFBFFFDFFFFDFFFFE5FFE07FF
      90007FFFFFFFDFFF83FFF7FFE0FFFDFFFF07FFFE5FFFFFFF90007FFFFFFFDFFF
      EFFFF7FFFBFFFDFFFFDFFFFE5FFFFFFF90007FFFFFFFDFFFEFFFF7FFFBFFFDFF
      FFDFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010000000000000000000000000000000
      000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFBFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFBFFF
      90007FFFBFFFDFFF81FFF7FFE07FFDFFFF03FFFE5FFFBFFF90007FFE0FFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFE0FFF90007FFFBFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFBFFF90007FFFBFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFBFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE40000000100000000000000000000000000000000000000040000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFBFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF90007FFFBFFFDFFF
      81FFF7FFE07FFDFFFFDFFFFE5FFE07FF90007FFE0FFFDFFFFFFFF7FFFFFFFDFF
      FF07FFFE5FFFFFFF90007FFFBFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF
      90007FFFBFFFDFFFFFFFF7FFFFFFFDFFFFDFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10000000000000000000000000000000000000004000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFBFFFDFFF
      FFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFBFFFDFFF81FFF7FFFBFFFDFF
      FF03FFFE5FFFBFFF90007FFE0FFFDFFFFFFFF7FFE0FFFDFFFFFFFFFE5FFE0FFF
      90007FFFBFFFDFFFFFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFBFFFDFFF
      FFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFF
      F0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE400000001000000000000000
      0000000000000000000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFBFFFDFFFFFFFF7FFFBFFFDFF
      FFDFFFFE5FFFBFFF90007FFFBFFFDFFF81FFF7FFFBFFFDFFFFDFFFFE5FFFBFFF
      90007FFE0FFFDFFFFFFFF7FFE0FFFDFFFF07FFFE5FFE0FFF90007FFFBFFFDFFF
      FFFFF7FFFBFFFDFFFFDFFFFE5FFFBFFF90007FFFBFFFDFFFFFFFF7FFFBFFFDFF
      FFDFFFFE5FFFBFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010000000000000000000000000000000
      000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFBFFFDFFFEFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFBFFFDFFFEFFFF7FFE07FFDFFFF03FFFE5FFE07FF90007FFE0FFFDFFF
      83FFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFBFFFDFFFEFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFBFFFDFFFEFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE40000000100000000000000000000000000000000000000040000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFBFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF90007FFFBFFFDFFF
      EFFFF7FFE07FFDFFFFDFFFFE5FFFBFFF90007FFE0FFFDFFF83FFF7FFFFFFFDFF
      FF07FFFE5FFE0FFF90007FFFBFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF
      90007FFFBFFFDFFFEFFFF7FFFFFFFDFFFFDFFFFE5FFFBFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10000000000000000000000000000000000000004000000010007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFBFFFDFFF
      EFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFBFFFDFFFEFFFF7FFFBFFFDFF
      FF03FFFE5FFFBFFF90007FFE0FFFDFFF83FFF7FFE0FFFDFFFFFFFFFE5FFE0FFF
      90007FFFBFFFDFFFEFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFBFFFDFFF
      EFFFF7FFFBFFFDFFFFFFFFFE5FFFBFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFF
      F0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE400000001000000000000000
      0000000000000000000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFBFFFDFFFEFFFF7FFFBFFFDFF
      FFDFFFFE5FFFBFFF90007FFFBFFFDFFFEFFFF7FFFBFFFDFFFFDFFFFE5FFFBFFF
      90007FFE0FFFDFFF83FFF7FFE0FFFDFFFF07FFFE5FFE0FFF90007FFFBFFFDFFF
      EFFFF7FFFBFFFDFFFFDFFFFE5FFFBFFF90007FFFBFFFDFFFEFFFF7FFFBFFFDFF
      FFDFFFFE5FFFBFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE40000000
      10007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE7FFFFFFFF0007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010000000000000000000000000000000
      000000004000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFF0FFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE5FFF77FF90007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFE3FFFE5FFF67FF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFF7FFFE5F88189F
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFF7FFFE5FED7DBF90007FFE23FFDFFF
      0FFFF7FE311FFDFFFC71FFFE5FCD0DBF90007FFF77FFDFFFB7FFF7FF6FDFFDFF
      FBB6FFFE5FBD6CBF90007FFF87FFDFFFB7FFF7FF619FFDFFFBB6FFFE5F89813F
      90007FFFAFFFDFFF8FFFF7FF2D7FFDFFFBB6FFFE5FFFFFFF90007FFFAFFFDFFF
      B7FFF7FE531FFDFFFC61FFFE5FFDFFFF90007FFFAFFFDFFFB7FFF7FFFFFFFDFF
      FFFFFFFE5FFFFFFF90007FFFDFFFDFFF0FFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFF0FFFDFFF
      87FFF7FFF0FFFDFFFFFFFFFE5FFFFFFF90007FFF77FFDFFFBBFFF7FFF77FFDFF
      FFFFFFFE5C70200F90007FFF67FFDFFFB3FFF7FFF67FFDFFFFFFFFFE5EFB6ADF
      90007F08189FDF840C4FF7F08189FDFF88311FFE5EFB6CDF90007F6D7DBFDFB6
      BEDFF7F6D7DBFDFF73AFBFFE5E1B2EDF90007FED0DBFDFF686DFF7FED0DBFDFF
      7B21BFFE5EF248DF90007F9D6CBFDFCEB65FF7F9D6CBFDFF7AED9FFE5EFFFFDF
      90007F79813FDFBCC09FF7F79813FDFF7A332FFE5C1BFFDF90007F4FFFFFDFA7
      FFFFF7F4FFFFFDFF7BFFFFFE5FFFFF9F90007FADFFFFDFD6FFFFF7FADFFFFDFE
      31FFFFFE5FFFFFFF90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF
      90007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE5FFFFFFF90007FFFFFFFDFFF
      FFFFF7FFFFFFFDFFFFFFFFFE4000000010007FFFFFFFDFFFFFFFF7FFFFFFFDFF
      FFFFFFFE7FFFFFFFF0007FFFFFFFDFFFFFFFF7FFFFFFFDFFFFFFFFFE00000000
      0000000000000000000000000000000000000000000000000000}
    Transparent = True
  end
  object imgMultResultSign: TImage
    Left = 54
    Top = 232
    Width = 121
    Height = 102
    Picture.Data = {
      07544269746D61709E060000424D9E060000000000003E000000280000007100
      0000660000000100010000000000600600000000000000000000020000000000
      000000000000FFFFFF00FFFFFFFFFFFFFFFFE000000000000000FFFFFFFFFFFF
      FFFFE7FFFFFFFFFF0000FFFFFFFFFFFFFFFFE400000000010000000000000000
      000004000000000100007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFBFFFF900007FFE07FFDFFF
      C0FFE5FFFFBFFFF900007FFFFFFFDFFFFFFFE5FFFE0FFFF900007FFFFFFFDFFF
      FFFFE5FFFFBFFFF900007FFFFFFFDFFFFFFFE5FFFFBFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE4000000000100007FFFFFFFDFFFFFFFE7FFFFFFFFFF00007FFFFFFFDFFF
      FFFFE400000000010000000000000000000004000000000100007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      F7FFE5FFFFFFFFF900007FFE07FFDFFFF7FFE5FFFE07FFF900007FFFFFFFDFFF
      C1FFE5FFFFFFFFF900007FFFFFFFDFFFF7FFE5FFFFFFFFF900007FFFFFFFDFFF
      F7FFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE4000000000100007FFFFFFFDFFF
      FFFFE7FFFFFFFFFF00007FFFFFFFDFFFFFFFE400000000010000000000000000
      000004000000000100007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFBFFFDFFFFFFFE5FFFFFFFFF900007FFFBFFFDFFF
      C0FFE5FFFE07FFF900007FFE0FFFDFFFFFFFE5FFFFFFFFF900007FFFBFFFDFFF
      FFFFE5FFFFFFFFF900007FFFBFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE4000000000100007FFFFFFFDFFFFFFFE7FFFFFFFFFF00007FFFFFFFDFFF
      FFFFE400000000010000000000000000000004000000000100007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFBFFFDFFF
      F7FFE5FFFFBFFFF900007FFFBFFFDFFFF7FFE5FFFFBFFFF900007FFE0FFFDFFF
      C1FFE5FFFE0FFFF900007FFFBFFFDFFFF7FFE5FFFFBFFFF900007FFFBFFFDFFF
      F7FFE5FFFFBFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE4000000000100007FFFFFFFDFFF
      FFFFE7FFFFFFFFFF00007FFFFFFFDFFFFFFFE400000000010000000000000000
      000004000000000100007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFC47FFDFFF0FFFE5FFFFFFFFF900007FFEEFFFDFFF
      B7FFE5FFF188FFF900007FFF0FFFDFFFB7FFE5FFFB7EFFF900007FFF5FFFDFFF
      8FFFE5FFFB0CFFF900007FFF5FFFDFFFB7FFE5FFF96BFFF900007FFF5FFFDFFF
      B7FFE5FFF298FFF900007FFFBFFFDFFF0FFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFE1FFFDFFF87FFE5FFFFFFFFF900007FFEEFFFDFFF
      BBFFE5FFFF87FFF900007FFECFFFDFFFB3FFE5FFFFBBFFF900007E10313FDF84
      0C4FE5FFFFB3FFF900007EDAFB7FDFB6BEDFE5FF840C4FF900007FDA1B7FDFF6
      86DFE5FFB6BEDFF900007F3AD97FDFCEB65FE5FFF686DFF900007EF3027FDFBC
      C09FE5FFCEB65FF900007E9FFFFFDFA7FFFFE5FFBCC09FF900007F5BFFFFDFD6
      FFFFE5FFA7FFFFF900007FFFFFFFDFFFFFFFE5FFD6FFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE5FFFFFFFFF900007FFFFFFFDFFF
      FFFFE5FFFFFFFFF900007FFFFFFFDFFFFFFFE4000000000100007FFFFFFFDFFF
      FFFFE7FFFFFFFFFF00007FFFFFFFDFFFFFFFE000000000000000000000000000
      00000000000000000000}
    Transparent = True
  end
  object lblAdderOperation: TStaticText
    Left = 32
    Top = 7
    Width = 158
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'Operaci'#243'n efectiva suma/resta'
    Color = clSilver
    ParentColor = False
    TabOrder = 0
  end
  object lblAdderResultSign: TStaticText
    Left = 232
    Top = 7
    Width = 180
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'Signo del resultado suma/resta'
    Color = clSilver
    ParentColor = False
    TabOrder = 1
  end
  object lblMultResultSign: TStaticText
    Left = 32
    Top = 215
    Width = 158
    Height = 17
    Alignment = taCenter
    AutoSize = False
    BorderStyle = sbsSunken
    Caption = 'Signo resultado mult/div'
    Color = clSilver
    ParentColor = False
    TabOrder = 2
  end
  object cbtAdderOperation: TBitBtn
    Left = 139
    Top = 194
    Width = 52
    Height = 18
    Caption = 'M'#225's...'
    TabOrder = 3
    OnClick = cbtAdderOperationClick
  end
  object cbtMultResultSign: TBitBtn
    Left = 119
    Top = 334
    Width = 52
    Height = 18
    Caption = 'M'#225's...'
    TabOrder = 4
    OnClick = cbtMultResultSignClick
  end
  object cbtAdderResultSign: TBitBtn
    Left = 369
    Top = 330
    Width = 52
    Height = 18
    Caption = 'M'#225's...'
    TabOrder = 5
    OnClick = cbtAdderResultSignClick
  end
end
