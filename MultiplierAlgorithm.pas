(*
  Casos especiales:

  N�mero no normalizado
  00000000000000000000000000000010
  dividido por 2

  N�mero m�s peque�o normalizado
  00000000100000000000000000000000
  dividido por 2

  N�mero m�s grande
  01111111011111111111111111111111
*)
unit MultiplierAlgorithm;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, Buttons, StdCtrls, Numbers, Functions, Text,
  RafMsg, AskEntry, Auxiliar;

type
  TfrmMultiplierAlgorithm = class(TForm)
    lblInit: TStaticText;
    Shape1: TShape;
    lblStep1: TStaticText;
    lblStep2: TStaticText;
    lblStep3: TStaticText;
    lblOverflow: TStaticText;
    lblStep4: TStaticText;
    lblNormalized: TStaticText;
    Shape6: TShape;
    Shape5: TShape;
    Shape4: TShape;
    lblYes2: TStaticText;
    lblEnd: TStaticText;
    lblNo2: TStaticText;
    lblException: TStaticText;
    Shape2: TShape;
    Shape3: TShape;
    lblYes1: TStaticText;
    cbtEnd: TBitBtn;
    cbtNext: TBitBtn;
    lblDesc: TStaticText;
    Image1: TImage;
    lblSign: TStaticText;
    Shape7: TShape;
    lblNo1: TStaticText;
    procedure cbtNextClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure cbtEndClick(Sender: TObject);
    procedure SetStep(aStep: Integer; State: boolean);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMultiplierAlgorithm: TfrmMultiplierAlgorithm;

implementation
{$R *.dfm}

const
  MAX_STEP = 11;

var
  Step: Integer;

  AE: TAskEntry;
  NumRes: TFloatingPointNumber;
  S, M, E: array[0..2] of string; //Para cada array: elem 0 = 1er operando. elem 1 = 2� operando. elem 2 = resultado
  SInt: array[0..2] of Integer;
  ExpOverflow, ExpUnderflow,
  NormOverflow,
  YetNormalized: Boolean;



(***  EVENTOS DE LA VENTANA  ***)

procedure TfrmMultiplierAlgorithm.FormShow(Sender: TObject);
begin
  //Textos
  Caption               := TXT_ALGMULT_CAPTION[Lng];
  lblInit.Caption       := TXT_ALGMULT_LBL_INIT[Lng];
  lblStep1.Caption      := TXT_ALGMULT_LBL_STEP1[Lng];
  lblStep2.Caption      := TXT_ALGMULT_LBL_STEP2[Lng];
  lblStep3.Caption      := TXT_ALGMULT_LBL_STEP3[Lng];
  lblOverflow.Caption   := TXT_ALGMULT_LBL_OVERFLOW[Lng];
  lblException.Caption  := TXT_ALGMULT_LBL_EXCEPTION[Lng];
  lblStep4.Caption      := TXT_ALGMULT_LBL_STEP4[Lng];
  lblNormalized.Caption := TXT_ALGMULT_LBL_NORMALIZED[Lng];
  lblSign.Caption       := TXT_ALGMULT_LBL_SIGN[Lng];
  lblEnd.Caption        := TXT_ALGMULT_LBL_END[Lng];
  lblYes1.Caption       := TXT_YES[Lng];
  lblYes2.Caption       := TXT_YES[Lng];
  lblNo1.Caption        := TXT_NO[Lng];
  lblNo2.Caption        := TXT_NO[Lng];
  cbtNext.Caption       := TXT_BUT_NEXT[Lng];
  cbtEnd.Caption        := TXT_BUT_END[Lng];

  //Inicializaci�n de elementos
  SetStep(0, True);
  AE.Num[0] := nil;
  AE.Num[1] := nil;
end;

procedure TfrmMultiplierAlgorithm.FormClose(Sender: TObject;
  var Action: TCloseAction);
var
  i: Integer;
begin
  SetStep(Step, False);
  for i := 0 to 1 do if AE.Num[i] <> nil then begin
    AE.Num[i].Free;
    AE.Num[i] := nil;
  end;
end;

procedure TfrmMultiplierAlgorithm.cbtNextClick(Sender: TObject);
begin
  //Paso siguiente
  SetStep(Step, False);
  inc(Step);
  SetStep(Step, True);
end;

procedure TfrmMultiplierAlgorithm.cbtEndClick(Sender: TObject);
begin
  Close;
end;



(***  FUNCI�N DE TRANSICI�N DE ESTADOS  ***)

procedure TfrmMultiplierAlgorithm.SetStep;
label
  StepAction;
var
  i, aux: Integer;
  str, Desc: string;
begin
  (* Acciones al activar un estado por primera vez *)
  StepAction:
  if State then case aStep of

    //Inico
    0: begin
      Desc := TXT_ALGMULT_STEP[Lng, 0];
    end;

    //Lee la entrada. Si es err�nea, vuelve a 0
    1: begin
      if not AskUserEntry(AE, 1) then begin
        aStep := 0;
        goto StepAction;
      end;

      ExpandOperandsInfo(AE.Num, S, E, M, SInt);

      //Divisi�n por 0
      if (AE.OpUser = 1) and (AE.Num[1].Value = 0) then begin
        ShowError(TXT_DIVISION_BY_ZERO[Lng]);
        aStep := 0;
        goto StepAction;
      end;

      Desc := Format(TXT_ALGMULT_STEP[Lng, 1], [S[0], E[0], M[0],
        AE.Num[0].DecValue, S[1], E[1], M[1], AE.Num[1].DecValue]);
    end;

    //A�ade bits impl�citos y suma exponentes
    2: begin
      for i := 0 to 1 do AddImplicitBit(E[i], M[i]);
      E[2] := AddExponents(E[0], E[1], AE.OpUser, ExpOverflow, ExpUnderflow);
      Desc := Format(TXT_ALGMULT_STEP[Lng, 2], [S[0], E[0], M[0], S[1], E[1],
        M[1], E[2]]);
    end;

    //Multiplica/divide mantisas
    3: begin
      M[2] := MultiplierSignifOp(M[0], M[1], AE.OpUser);
      SolveMultUnderflow(ExpUnderflow, E[2], M[2]);

      Desc := Format(TXT_ALGMULT_STEP[Lng, 3], [M[0], M[1], M[2]]);
    end;

    //Normaliza
    4: begin
      E[0] := E[2];
      M[0] := M[2];
      NormOverflow := Normalize(E[2], M[2], 2);
      TruncSignifAfterNormalize(M[2]);

      Desc := Format(TXT_ALGMULT_STEP[Lng, 4], [E[0], M[0], E[2], M[2]]);
    end;

    //�Desbordamiento?
    5: begin
      if NormOverflow or ExpOverflow then str := TXT_AFFIRMATION[Lng]
      else str := TXT_NEGATION[Lng];
      Desc := Format(TXT_ALGMULT_STEP[Lng, 5], [str]);
    end;

    //Excepci�n
    6: begin
      if not (NormOverflow or ExpOverflow) then begin
        inc(aStep);
        goto StepAction;
      end;
      Desc := TXT_ALGMULT_STEP[Lng, 6];
    end;

    //Redondeo
    7: begin
      M[0] := M[2];
      RoundSignificand(S[2], M[2], AE.RoundMethod);
      Desc := Format(TXT_ALGMULT_STEP[Lng, 7], [TXT_ROUND[Lng, Integer(
        AE.RoundMethod)], M[0], M[2]]);
    end;

    //Normalizado a�n?
    8: begin
      YetNormalized := M[2][1] = '0';
      if YetNormalized then str := TXT_NEGATION[Lng] else str := TXT_AFFIRMATION[Lng];
      Desc := Format(TXT_ALGMULT_STEP[Lng, 8], [str]);
    end;

    //Signo del resultado
    9: if YetNormalized then begin
      S[2] := GetMultResultSign(S[0], S[1]);
      Desc := Format(TXT_ALGMULT_STEP[Lng, 9], [S[0], S[1], S[2]]);
    end else begin
      aux := Length(M[2]) - 1;
      for i := 1 to aux do M[2] := M[2] + '0';
      aStep := 4;
      goto StepAction;
    end;

    //Fin
    10: begin
      Delete(M[2], 1, 2);
      NumRes := TFloatingPointNumber.Create(
        fiBin, AE.num[0].Format, 0, 0, S[2] + E[2] + M[2]);
      Desc := Format(TXT_ALGMULT_STEP[Lng, 10], [AE.Num[0].Sign,
        AE.Num[0].Exponent, AE.Num[0].Mantissa, AE.Num[0].DecValue,
        TXT_MULTIPLICATION_SYMBOL[AE.OpUser], AE.Num[1].Sign,
        AE.Num[1].Exponent, AE.Num[1].Mantissa, AE.Num[1].DecValue, S[2],
        E[2], M[2], NumRes.DecValue]);
      NumRes.Free;
    end;
  end;



  (* Acciones al activar/desactivar estado *)
  case aStep of
    1: SetLabelState(lblInit, State);
    2: SetLabelState(lblStep1, State);
    3: SetLabelState(lblStep2, State);
    4: SetLabelState(lblStep3, State);
    5: SetLabelState(lblOverflow, State);
    6: SetLabelState(lblException, State);
    7: SetLabelState(lblStep4, State);
    8: SetLabelState(lblNormalized, State);
    9: SetLabelState(lblSign, State);
    10: SetLabelState(lblEnd, State);
  end;


  (* Muestra descripci�n del estado *)
  if State then begin
    lblDesc.Caption := Desc;
    cbtNext.Enabled := not (aStep in [MAX_STEP - 1, 6]);
    Step := aStep;
  end;
end;


end.
